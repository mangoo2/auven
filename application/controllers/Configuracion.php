<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Configuracion extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelEquipo');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid,6); // 2 es el id del submenu
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/configuracion/form');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/configuracion/formjs');
  }
}