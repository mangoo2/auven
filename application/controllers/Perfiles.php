<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid_tz');
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// 2 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }

        }
    }
	public function index(){
            //$data['menu'] = $this->ModeloGeneral->getselect('menu_sub');
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('administrador/perfiles/perfil',$data);
            $this->load->view('templates/footer');
            $this->load->view('administrador/perfiles/jsperfil');
	}
    public function guardar(){
        $datos = $this->input->post();
        $id=$datos['perfilId'];
        unset($datos['perfilId']);
         if ($id>=1) {
             $result = $this->ModeloGeneral->updateCatalogo($datos,'perfilId',$id,'perfiles');
          }else{
             $result = $this->ModeloGeneral->tabla_inserta('perfiles',$datos);
          }   
        echo $result;
    }
    public function getListado()
    {
       $getdatos = $this->ModeloGeneral->getselectwhere('perfiles','estatus',1);
       $json_data = array("data" => $getdatos);
       echo json_encode($json_data);
    }

    public function guardar_perfil_detalles(){
        $datos = $this->input->post('data');

        
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['perfilId']= $DATA[$i]->perfilId;
            $data['MenusubId']= $DATA[$i]->MenusubId;
            $this->ModeloCatalogos->delete_perfiles($data['perfilId']); 
           /*
            $result = $this->ModeloCatalogos->perfil_detalles($data['perfilId']);
            var_dump($result['Perfil_detalleId']);
           */  
        }
        for ($y=0;$y<count($DATA);$y++) { 
            $data['perfilId']= $DATA[$y]->perfilId;
            $data['MenusubId']= $DATA[$y]->MenusubId;
            $this->ModeloGeneral->tabla_inserta('perfiles_detalles',$data);   
        }
    }
   
    function visualizar_permisos(){
       $id = $this->input->get('id');
       $getdatos = $this->ModeloGeneral->getselectwhere('perfiles_detalles','perfilId',$id);
       echo json_encode($getdatos);
    }
    function list_permisos(){
      $id = $this->input->post('id'); 
      $result = $this->ModeloCatalogos->getlistpermisos($id); 
      $html = '';
      foreach ($result as $item) {  
      $html .= '<div class="col-9">
             <div class="form-group">
               <label for="drop-remove">'.$item->Nombre.'</label>
             </div>
           </div>
           <div class="col-1">
             <table id="table_perfil">
               <tbody>
                 <tr> 
                   <td>
                    <img src="public/img/checked.png" style="height: 25px; width: 25px">
                   </td> 
                 </tr>
               </tbody>
             </table> 
           </div>';
      }
      echo $html;
    }
    function eliminar(){
        $id = $this->input->post('id');
        $data = array('estatus' => 0);
        $this->ModeloGeneral->updateCatalogo($data,'perfilId',$id,'perfiles');
        $this->ModeloCatalogos->delete_perfiles($id); 
    }

}    
