var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
});

function reload_registro(){
    tabla.destroy();
    loadtable();
}
function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Equipo/getlistado",
            type: "post",
            "data": {
                'tipo_equipo':$('#tipo_equipo option:selected').val()},
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    if(row.tipo==1){
                        html+='Equipo MT';
                    }else if(row.tipo==2){
                        html+='Equipo UT';
                    }else{
                        html+='';
                    }
                return html;
                }
            },
            {"data":"marca"},
            {"data":"modelo"},
            {"data":"serie"},
            {"data":"no_certificado"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    if(row.fecha_cal!='00/00/0000'){
                        html=row.fecha_cal;  
                    }else{
                        html='';   
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    if(row.vigencia_cal!='00/00/0000'){
                        html=row.vigencia_cal;  
                    }else{
                        html='';   
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Equipo/registrar/'+row.id+'" class="btn btn-outline-dark mb-1"><i class="icon-xl far fa-edit"></i></a>\
                        <a data-alias="'+row.marca+'" onclick="delete_data('+row.id+')" class="btn btn-outline-dark mb-1 id_reg_'+row.id+'"><i class="icon-xl far fa-trash-alt"></i></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}

function delete_data(id){
    var nombre=$('.id_reg_'+id).data('alias');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar a '+nombre+'?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipo/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}