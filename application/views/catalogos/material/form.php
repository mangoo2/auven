<div class="row">
	<div class="col-12">
		<div class="content-header"><?php echo $tittle ?> material</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body table-responsive">

					<form method="post" id="form_data">
						<input type="hidden" name="id" id="id_reg" value="<?php echo $id ?>">

						<div class="row">
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Nominal Composition" name="nominal_composition" value="<?php echo $nominal_composition ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Product Form" name="product_form" value="<?php echo $product_form ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Spec. No" name="spec_no" value="<?php echo $spec_no ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Type/Grande" name="type_grande" value="<?php echo $type_grande ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Alloy Desig./ UNS No." name="alloy_desig" value="<?php echo $alloy_desig ?>">
							</div>
                            <div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Class/ Condition/ Temper" name="class_condition" value="<?php echo $class_condition ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Size/Thickess, in." name="size_thickess" value="<?php echo $size_thickess ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="P-No." name="p_no" value="<?php echo $p_no ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Group No." name="group_no" value="<?php echo $group_no ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Ksi" name="ksi" value="<?php echo $ksi ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Ksi" name="ksi2" value="<?php echo $ksi2 ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="I" name="l" value="<?php echo $l ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="III" name="lll" value="<?php echo $lll ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="VIII-1    XII" name="vlll_xll" value="<?php echo $vlll_xll ?>">
							</div>
							<div class="col-3 form-group">
								<input class="form-control" type="text" placeholder="Chart No." name="chart_no" value="<?php echo $chart_no ?>">
							</div>
						</div>
						<br>
						<div class="form-group">
							<h4>Esfuerzo permisible Mpa / °C</h4>
							<div class="">
								<div class="d-flex justify-content-around">
									<label class="text-center mx-1">40<input type="number" class="text-center form-control" name="c_40" value="<?php echo $c_40 ?>"></label>
									<label class="text-center mx-1">65<input type="number" class="text-center form-control" name="c_65" value="<?php echo $c_65 ?>"></label>
									<label class="text-center mx-1">100<input type="number" class="text-center form-control" name="c_100" value="<?php echo $c_100 ?>"></label>
									<label class="text-center mx-1">125<input type="number" class="text-center form-control" name="c_125" value="<?php echo $c_125 ?>"></label>
									<label class="text-center mx-1">150<input type="number" class="text-center form-control" name="c_150" value="<?php echo $c_150 ?>"></label>
									<label class="text-center mx-1">200<input type="number" class="text-center form-control" name="c_200" value="<?php echo $c_200 ?>"></label>
									<label class="text-center mx-1">250<input type="number" class="text-center form-control" name="c_250" value="<?php echo $c_250 ?>"></label>
									<label class="text-center mx-1">300<input type="number" class="text-center form-control" name="c_300" value="<?php echo $c_300 ?>"></label>
									<label class="text-center mx-1">325<input type="number" class="text-center form-control" name="c_325" value="<?php echo $c_325 ?>"></label>
									<label class="text-center mx-1">350<input type="number" class="text-center form-control" name="c_350" value="<?php echo $c_350 ?>"></label>
									<label class="text-center mx-1">375<input type="number" class="text-center form-control" name="c_375" value="<?php echo $c_375 ?>"></label>
								</div>
								<div class="d-flex justify-content-around">
									
									<label class="text-center mx-1">400<input type="number" class="text-center form-control" name="c_400" value="<?php echo $c_400 ?>"></label>
									<label class="text-center mx-1">425<input type="number" class="text-center form-control" name="c_425" value="<?php echo $c_425 ?>"></label>
									<label class="text-center mx-1">450<input type="number" class="text-center form-control" name="c_450" value="<?php echo $c_450 ?>"></label>
									<label class="text-center mx-1">475<input type="number" class="text-center form-control" name="c_475" value="<?php echo $c_475 ?>"></label>
									<label class="text-center mx-1">500<input type="number" class="text-center form-control" name="c_500" value="<?php echo $c_500 ?>"></label>
									<label class="text-center mx-1">525<input type="number" class="text-center form-control" name="c_525" value="<?php echo $c_525 ?>"></label>
									<label class="text-center mx-1">550<input type="number" class="text-center form-control" name="c_550" value="<?php echo $c_550 ?>"></label>
									<label class="text-center mx-1">575<input type="number" class="text-center form-control" name="c_575" value="<?php echo $c_575 ?>"></label>
									<label class="text-center mx-1">600<input type="number" class="text-center form-control" name="c_600" value="<?php echo $c_600 ?>"></label>
									<label class="text-center mx-1">625<input type="number" class="text-center form-control" name="c_625" value="<?php echo $c_625 ?>"></label>
									<label class="text-center mx-1">650<input type="number" class="text-center form-control" name="c_650" value="<?php echo $c_650 ?>"></label>
								</div>
								<div class="d-flex justify-content-around">
									<label class="text-center mx-1">675<input type="number" class="text-center form-control" name="c_675" value="<?php echo $c_675 ?>"></label>
									<label class="text-center mx-1">700<input type="number" class="text-center form-control" name="c_700" value="<?php echo $c_700 ?>"></label>
									<label class="text-center mx-1">725<input type="number" class="text-center form-control" name="c_725" value="<?php echo $c_725 ?>"></label>
									<label class="text-center mx-1">750<input type="number" class="text-center form-control" name="c_750" value="<?php echo $c_750 ?>"></label>
									<label class="text-center mx-1">775<input type="number" class="text-center form-control" name="c_775" value="<?php echo $c_775 ?>"></label>
									<label class="text-center mx-1">800<input type="number" class="text-center form-control" name="c_800" value="<?php echo $c_800 ?>"></label>
									<label class="text-center mx-1">825<input type="number" class="text-center form-control" name="c_825" value="<?php echo $c_825 ?>"></label>
									<label class="text-center mx-1">850<input type="number" class="text-center form-control" name="c_850" value="<?php echo $c_850 ?>"></label>
									<label class="text-center mx-1">875<input type="number" class="text-center form-control" name="c_875" value="<?php echo $c_875 ?>"></label>
									<label class="text-center mx-1">900<input type="number" class="text-center form-control" name="c_900" value="<?php echo $c_900 ?>"></label>
									<label class="text-center mx-1">950<input type="number" class="text-center form-control" name="c_950" value="<?php echo $c_950 ?>"></label>
								</div>
								<div class="d-flex justify-content-around">
									<label class="text-center mx-1">1000<input type="number" class="text-center form-control" name="c_1000" value="<?php echo $c_1000 ?>"></label>
									<label class="text-center mx-1">1050<input type="number" class="text-center form-control" name="c_1050" value="<?php echo $c_1050 ?>"></label>
									<label class="text-center mx-1">1100<input type="number" class="text-center form-control" name="c_1100" value="<?php echo $c_1100 ?>"></label>
									<label class="text-center mx-1">1150<input type="number" class="text-center form-control" name="c_1150" value="<?php echo $c_1150 ?>"></label>
									<label class="text-center mx-1">1200<input type="number" class="text-center form-control" name="c_1200" value="<?php echo $c_1200 ?>"></label>
									<label class="text-center mx-1">1250<input type="number" class="text-center form-control" name="c_1250" value="<?php echo $c_1250 ?>"></label>
									<label class="text-center mx-1">1300<input type="number" class="text-center form-control" name="c_1300" value="<?php echo $c_1300 ?>"></label>
									<label class="text-center mx-1">1350<input type="number" class="text-center form-control" name="c_1350" value="<?php echo $c_1350 ?>"></label>
									<label class="text-center mx-1">1400<input type="number" class="text-center form-control" name="c_1400" value="<?php echo $c_1400 ?>"></label>
									<label class="text-center mx-1">1450<input type="number" class="text-center form-control" name="c_1450" value="<?php echo $c_1450 ?>"></label>
									<label class="text-center mx-1">1500<input type="number" class="text-center form-control" name="c_1500" value="<?php echo $c_1500 ?>"></label>
								</div>
                                <div class="d-flex">
									<label class="text-center mx-1" style="width: 8%;">1550<input type="number" class="text-center form-control" name="c_1550" value="<?php echo $c_1550 ?>"></label>
									<label class="text-center mx-1" style="width: 8%;">1600<input type="number" class="text-center form-control" name="c_1600" value="<?php echo $c_1600 ?>"></label>
								</div>	
							</div>
						</div>
						<br>
						<div class="row" style="display:none;">
							<div class="col-7 form-group">
								<table class="table text-center m-0" id="table_data_detalles">
			                        <thead>
			                            <tr>
			                                <th></th>
			                                <th>#</th>
			                                <th>Valor</th>
			                                <th></th>
			                            </tr>
			                        </thead>
			                        <tbody id="t_body">

			                        </tbody>
			                    </table>
							</div>
						</div>	
					</form>
					<hr>
					<div class="row">
                        <div class="col-lg-12">
                            <button type="button" onclick="add_form()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Material" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
				</div>

			</div>
		</div>
	</div>
</div>
</div>