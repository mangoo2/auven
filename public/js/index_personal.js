var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
});

function reload_registro(){
    tabla.destroy();
    loadtable();
}
function loadtable(){
	tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Personal/getlistado",
            type: "post",
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            {"data":"personalId"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
               
                    if(row.foto!='' && row.foto!=null){
                        html+='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/personal/'+row.foto+'">';  
                    }else{
                        html+='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'public/img/avatar.png">';
                    }
                return html;
                }
            },
            {"data":"nombre"},
            {"data":"correo"},
            {"data":"celular"},
            {"data":"Usuario"},
            {"data":"perfil"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Personal/registrar/'+row.personalId+'" class="btn btn-outline-dark mb-1"><i class="icon-xl far fa-edit"></i></a>\
                        <a data-empleado="'+row.nombre+'" onclick="delete_data('+row.personalId+')" class="btn btn-outline-dark mb-1 id_reg_'+row.personalId+'"><i class="icon-xl far fa-trash-alt"></i></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}

function delete_data(id){
    var nombre=$('.id_reg_'+id).data('empleado');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar a '+nombre+'?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Personal/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}