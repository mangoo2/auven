<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual = date('Y-m-d');
        $this->hora = date('G:i:s');
    }

    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,perf.nombre AS perfil_nombre
                FROM usuarios AS usu
                INNER JOIN perfiles AS perf ON perf.perfilId = usu.perfilId
                INNER JOIN personal AS per ON per.personalId = usu.personalId
                WHERE usu.estatus = 1 AND perf.estatus = 1 AND usu.Usuario = '$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);

        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;  
            $perfil_nombre = $row->perfil_nombre;

            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'perfil_nombre'=>$perfil_nombre,
                    );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 

        echo $count;
    }

    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.orden ASC";
        $query = $this->db->query($strq);
        return $query;

    }
    
    public function submenus($perfil,$menu,$tipo){

        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='$perfil' AND menus.tipo='$tipo' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;

    }
    function equiposvencidos(){
        //$strq="SELECT * FROM equipo WHERE activo=1 AND vigencia_cal<DATE_SUB(NOW(),INTERVAL '2' MONTH) ORDER BY vigencia_cal DESC";
        $strq="SELECT * FROM equipo WHERE activo=1 AND vigencia_cal<DATE_ADD(NOW(),INTERVAL '2' MONTH) ORDER BY vigencia_cal DESC";
        $query = $this->db->query($strq);
        return $query;
    }
    function diferenciafecha($fecha){
        $fechatexto='';
        if($fecha!='0000-00-00'){
            $date1 = new DateTime($this->fechaactual);
            $date2 = new DateTime($fecha);
            $df = $date1->diff($date2);
            //===========================================
            $str = 'Hace ';
            if($fecha<$this->fechaactual){
                $str = 'Hace ';
            }else{
                $str = 'En ';    
            }
            
            $str .= ($df->invert == 1) ? ' - ' : '';
            if ($df->y > 0) {
                // years
                $str .= ($df->y > 1) ? $df->y . ' Años ' : $df->y . ' Año ';
            } if ($df->m > 0) {
                // month
                $str .= ($df->m > 1) ? $df->m . ' Meses ' : $df->m . ' Mes ';
            } if ($df->d > 0) {
                // days
                $str .= ($df->d > 1) ? $df->d . ' Dias ' : $df->d . ' dia ';
            } if ($df->h > 0) {
                // hours
                $str .= ($df->h > 1) ? $df->h . ' Hours ' : $df->h . ' Hour ';
            } if ($df->i > 0) {
                // minutes
                $str .= ($df->i > 1) ? $df->i . ' Minutes ' : $df->i . ' Minute ';
            } if ($df->s > 0) {
                // seconds
                $str .= ($df->s > 1) ? $df->s . ' Seconds ' : $df->s . ' Second ';
            }

            $fechatexto=$str;
        }
        return $fechatexto;
    }

   
}
