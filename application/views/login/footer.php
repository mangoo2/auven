<!-- Jquery Core Js -->
<script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>
<!--
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
-->
<script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

</body>

</html>