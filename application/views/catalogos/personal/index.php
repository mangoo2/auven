<div class="row">
    <div class="col-12">
        <div class="content-header">Listado de usuarios</div>
    </div>
      
</div>
<div class="row"> 
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body table-responsive">
                    <a href="<?php echo base_url() ?>Personal/registrar" class="btn btn-sistema mr-sm-2 mb-1"><i class="fa fa-user"></i>  Nuevo usuario</a>
                    <table class="table text-center m-0" id="table_data" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Foto</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Celular</th>
                                <th>Usuario</th>
                                <th>Perfil</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>