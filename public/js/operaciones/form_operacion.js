var base_url = $('#base_url').val();
$(document).ready(function($) {
    wizard=$(".icons-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        enableAllSteps: true,
        labels: {
          next: '<button type="button" class="btn btn-sistema">Siguiente <i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
          previous: '<button type="button" class="btn btn-sistema"><i class="fa fa-arrow-left" aria-hidden="true"></i> Anterior</button>',
          finish: '<button id="savef" type="button" onclick="finalizarProceso()" class="btn btn-sistema"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
          
                return true; 
            
        },
        onFinished: function (event, currentIndex,newIndex) {
          return true;
        }
    });

    $('#id_cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione un cliente',
        ajax: {
            url: base_url+'Operaciones/searchClientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.alias,
                        direc: element.direccion
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        $("#direccion").html(data.direc);
    });
});

function finalizarProceso(){
    saveOperacion();    
}

function saveOperacion(){
	var form_register = $('#form_p1');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_cliente: {
                required: true
            },
            direccion: {
                required: true
            },
            num_expediente:{
              required: true
            },
            fecha:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_p1").valid();

    if ($valid) {
        ////////////////////
        $('#savef').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            	swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Operaciones/recipientes';
                }, 1500);
            }
        });           
    }
}