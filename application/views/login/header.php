<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>AUVEN</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>public/img/FAV.png" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
    <!-- Custom Css -->
    <link href="<?php echo base_url();?>plugins/style.css" rel="stylesheet">

</head>
<style type="text/css">
    body {
      background-image: url("<?php echo base_url(); ?>public/img/fondo.png");
    }

    .input_login{
        background: #ff000000;
        border: 3px solid white !important;
        color: white !important;
        border-radius: 18px !important;
    }

    @media screen and (max-width: 480px) {
        .margen_m{
            padding: 38px;
        }
        .img_logo_f{
            display: none !important;
        }
        .img_logo{
            display: block !important;
        }
        body {
            height: 700px;
            background-image: url("<?php echo base_url(); ?>public/img/fondo2.png");
            background-repeat:no-repeat;
            background-position-x:center;
            background-position-y:center;
            background-size: 10rem;
        }
    }
</style>