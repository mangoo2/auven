<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelOperaciones extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.id_cliente',
            2=>'o.direccion',
            3=>'o.num_expediente',
            4=>'o.fecha',
            5=>'e.alias'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('operaciones o');
        $this->db->join('empresa e','e.id=o.id_cliente');
        $this->db->where("estatus",1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_result($params){
        $columns = array( 
            0=>'o.id',
            1=>'o.id_cliente',
            2=>'o.direccion',
            3=>'o.num_expediente',
            4=>'o.fecha',
            5=>'e.alias'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('operaciones o');
        $this->db->join('empresa e','e.id=o.id_cliente');
        $this->db->where("estatus",1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getOperacion($id){
        $this->db->select("o.*, e.alias");
        $this->db->from("operaciones o");
        $this->db->join("empresa e","e.id=o.id_cliente");
        $this->db->where("o.id",$id);
        $query=$this->db->get();
        return $query->row();
    }

    public function getMaxEspesor($name,$id){
        $this->db->select("MAX(".$name.") as max_espesor");
        $this->db->from("espesores_paso5");
        $this->db->where("id_operacion",$id);
        $this->db->limit("1");
        $query=$this->db->get();
        return $query->row();
    }

    public function getTempDiseno($name,$id){
        $this->db->select(" ".$name." as temp_diseno");
        $this->db->from("material");
        $this->db->where("id",$id);
        $query=$this->db->get();
        return $query->row();
    }
}