            <div class="row">
                <div class="col-12">
                    <div class="content-header">Listado de operaciones</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="<?php echo base_url(); ?>Operaciones/nueva/" class="btn btn-sistema mr-sm-2 mb-3"><i class="fa fa-gears"></i> &nbsp; Nueva operación</a>
                                    </div>
                                    <!--<div class="col-3">
                                        <select id="tipo_equipo" class="form-control" onchange="reload_registro()">
                                            <option value="0" selected="">Selecciona tipo de equipo </option>
                                            <option value="0">Todos</option>
                                            <option value="1">Equipo MT</option>
                                            <option value="2">Equipo UT</option>
                                        </select>
                                    </div>-->
                                </div>
                                <table class="table text-center m-0" id="table_data">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Cliente</th>
                                            <th>Expediente</th>
                                            <th>Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_resumen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title_modaltxt" id="myModalLabel1">Resumen Cronológico</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input class="form-control" type="hidden" id="id_opera">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tipo_resumen">Tipo</label>
                            <select id="tipo_resumen" class="form-control">
                                <option value="1">REVISIONES Y MANTENIMIENTOS EFECTUADOS, DE ACUERDO CON EL PROGRAMA ELABORADO PARA TAL EFECTO, DEBIDAMENTE REGISTRADOS Y DOCUMENTADOS</option>
                                <option value="2">PRUEBAS DE PRESIÓN O EXÁMENES NO DESTRUCTIVOS</option>
                                <option value="3">MODIFICACIONES Y ALTERACIONES EFECTUADOS AL EQUIPO, DEBIDAMENTE REGISTRADAS Y DOCUMENTADAS</option>
                                <option value="4">REPARACIONES QUE IMPLICARON SOLDADURA, DEBIDAMENTE REGISTRADAS Y DOCUMENTADAS</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group d-flex">
                        <div class="col-md-1 align-self-center">
                            <label>Fecha</label>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" type="date" id="fecha" >
                        </div>
                        <div class="col-md-1 align-self-center">
                            <label>Descripción</label>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="descrip" >
                        </div>
                        <div class="col-md-1 align-self-center">
                            <label>Documento</label>
                        </div>
                        <div class="col-md-3">
                            <input class="form-control" type="text" id="documento" >
                        </div>
                    </div>
                    <div class="col-md-12 form-group d-flex">
                        <div class="col-md-1 align-self-center">
                            <label>Realizado por</label>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="realiza" >
                        </div>
                        <div class="col-md-1 align-self-center">
                            <label>Responsable</label>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="responsable" >
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-social-icon btn-sistema round" id="add_resumen"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-bordered" id="table_resumen">
                            <thead>
                                <tr>
                                    <th width="10%">Fecha</th>
                                    <th width="35%">Descripción</th>
                                    <th width="15%">Documento</th>
                                    <th width="15%">Realizó</th>
                                    <th width="15%">Responsable Empresa</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody id="body_resumen">

                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="save_re" class="btn btn-outline-primary btn-success">Guardar</button>
            </div>
        </div>
    </div>
</div>