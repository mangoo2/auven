<?php
defined('BASEPATH') or exit('No direct script access allowed');
require 'vendors/autoload.php';//php 7.1 como minimo
use PhpOffice\PhpSpreadsheet\IOFactory;

class Material extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelMaterial');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid, 2); // 2 es el id del submenu
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/material/index');

    $this->load->view('templates/footer');
    $this->load->view('catalogos/material/indexjs');$this->load->view('catalogos/material/modals');
  }

  public function registrar($id=0)
  {
    if($id==0){
      $data['tittle']='Nuevo';
      $data['id']=0;
      $data['nominal_composition']='';
      $data['product_form']='';
      $data['spec_no']='';
      $data['type_grande']='';
      $data['alloy_desig']='';
      $data['class_condition']='';
      $data['size_thickess']='';
      $data['p_no']='';
      $data['group_no']='';
      $data['ksi']='';
      $data['ksi2']='';
      $data['l']='';
      $data['lll']='';
      $data['vlll_xll']='';
      $data['chart_no']='';
      $data['c_40']=0;
      $data['c_65']=0;
      $data['c_100']=0;
      $data['c_125']=0;
      $data['c_150']=0;
      $data['c_200']=0;
      $data['c_250']=0;
      $data['c_300']=0;
      $data['c_325']=0;
      $data['c_350']=0;
      $data['c_375']=0;
      $data['c_400']=0;
      $data['c_425']=0;
      $data['c_450']=0;
      $data['c_475']=0;
      $data['c_500']=0;
      $data['c_525']=0;
      $data['c_550']=0;
      $data['c_575']=0;
      $data['c_600']=0;
      $data['c_625']=0;
      $data['c_650']=0;
      $data['c_675']=0;
      $data['c_700']=0;
      $data['c_725']=0;
      $data['c_750']=0;
      $data['c_775']=0;
      $data['c_800']=0;
      $data['c_825']=0;
      $data['c_850']=0;
      $data['c_875']=0;
      $data['c_900']=0;
      $data['c_950']=0;
      $data['c_1000']=0;
      $data['c_1050']=0;
      $data['c_1100']=0;
      $data['c_1150']=0;
      $data['c_1200']=0;
      $data['c_1250']=0;
      $data['c_1300']=0;
      $data['c_1350']=0;
      $data['c_1400']=0;
      $data['c_1450']=0;
      $data['c_1500']=0;
      $data['c_1550']=0;
      $data['c_1600']=0;

    }else{
      $data['tittle']='Edición'; 
      $resul=$this->ModeloGeneral->getselectwhere('material','id',$id);
      foreach ($resul as $x) {
        $data['tittle']='Nueva';
        $data['id']=$x->id;
        $data['nominal_composition']=$x->nominal_composition;
        $data['product_form']=$x->product_form;
        $data['spec_no']=$x->spec_no;
        $data['type_grande']=$x->type_grande;
        $data['alloy_desig']=$x->alloy_desig;
        $data['class_condition']=$x->class_condition;
        $data['size_thickess']=$x->size_thickess;
        $data['p_no']=$x->p_no;
        $data['group_no']=$x->group_no;
        $data['ksi']=$x->ksi;
        $data['ksi2']=$x->ksi2;
        $data['l']=$x->l;
        $data['lll']=$x->lll;
        $data['vlll_xll']=$x->vlll_xll;
        $data['chart_no']=$x->chart_no;
        $data['c_40']=$x->c_40;
        $data['c_65']=$x->c_65;
        $data['c_100']=$x->c_100;
        $data['c_125']=$x->c_125;
        $data['c_150']=$x->c_150;
        $data['c_200']=$x->c_200;
        $data['c_250']=$x->c_250;
        $data['c_300']=$x->c_300;
        $data['c_325']=$x->c_325;
        $data['c_350']=$x->c_350;
        $data['c_375']=$x->c_375;
        $data['c_400']=$x->c_400;
        $data['c_425']=$x->c_425;
        $data['c_450']=$x->c_450;
        $data['c_475']=$x->c_475;
        $data['c_500']=$x->c_500;
        $data['c_525']=$x->c_525;
        $data['c_550']=$x->c_550;
        $data['c_575']=$x->c_575;
        $data['c_600']=$x->c_600;
        $data['c_625']=$x->c_625;
        $data['c_650']=$x->c_650;
        $data['c_675']=$x->c_675;
        $data['c_700']=$x->c_700;
        $data['c_725']=$x->c_725;
        $data['c_750']=$x->c_750;
        $data['c_775']=$x->c_775;
        $data['c_800']=$x->c_800;
        $data['c_825']=$x->c_825;
        $data['c_850']=$x->c_850;
        $data['c_875']=$x->c_875;
        $data['c_900']=$x->c_900;
        $data['c_950']=$x->c_950;
        $data['c_1000']=$x->c_1000;
        $data['c_1050']=$x->c_1050;
        $data['c_1100']=$x->c_1100;
        $data['c_1150']=$x->c_1150;
        $data['c_1200']=$x->c_1200;
        $data['c_1250']=$x->c_1250;
        $data['c_1300']=$x->c_1300;
        $data['c_1350']=$x->c_1350;
        $data['c_1400']=$x->c_1400;
        $data['c_1450']=$x->c_1450;
        $data['c_1500']=$x->c_1500;
        $data['c_1550']=$x->c_1550;
        $data['c_1600']=$x->c_1600;
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/material/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/material/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'material');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('material',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelMaterial->get_result($params);
    $totaldata= $this->ModelMaterial->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'material');
      echo $resul;
  }
  
  function registro_detalles(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) {   
        $data['idmaterial']=$DATA[$i]->idmaterial;        
        $data['numero']=$DATA[$i]->numero;
        $data['valor']=$DATA[$i]->valor;    
        if($DATA[$i]->id==0){
            $this->ModeloGeneral->tabla_inserta('material_detalles',$data);
        }else{
            $this->ModeloGeneral->updateCatalogo($data,'id',$DATA[$i]->id,'material_detalles');
        }
    }
  }

  function get_tabla_detalles()
  {
      $id = $this->input->post('id');
      $results=$this->ModeloGeneral->getselectwhere_n_consulta('material_detalles',array('activo'=>1,'idmaterial'=>$id));
      echo json_encode($results);
  }

  public function delete_detalles(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'material_detalles');
      echo $resul;
  }
  function inserupdate(){
        $DIR_SUC=FCPATH.'file_materiales';
        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'xlsx';
        $config['max_size']             = 5000;
        $file_names=date('YmdGis');
        $config['file_name']=$file_names;       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('documento')){
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data));                    
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            $this->procesardocumento($file_name);

            $data = array('upload_data' => $this->upload->data());
            //$output = [];
            //log_message('error', json_encode($data));
        }
        echo json_encode($output);
    }
    function procesardocumento($file_name){
        //log_message('error', $file_name);
        $dataeqarrayinsert=array();


        $file=FCPATH.'file_materiales/'.$file_name;
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){

                $data['nominal_composition']=$hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $data['product_form']=$hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $data['spec_no']=$hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $data['type_grande']=$hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $data['alloy_desig']=$hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $data['class_condition']=$hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                $data['size_thickess']=$hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                $data['p_no']=$hojaActual->GetCellByColumnAndRow('8',$indiceFila);
                $data['group_no']=$hojaActual->GetCellByColumnAndRow('9',$indiceFila);
                $data['ksi']=$hojaActual->GetCellByColumnAndRow('10',$indiceFila);
                $data['ksi2']=$hojaActual->GetCellByColumnAndRow('11',$indiceFila);
                $data['l']=$hojaActual->GetCellByColumnAndRow('12',$indiceFila);
                $data['lll']=$hojaActual->GetCellByColumnAndRow('13',$indiceFila);
                $data['vlll_xll']=$hojaActual->GetCellByColumnAndRow('14',$indiceFila);
                $data['chart_no']=$hojaActual->GetCellByColumnAndRow('15',$indiceFila);
                $data['c_40']=$hojaActual->GetCellByColumnAndRow('17',$indiceFila);
                $data['c_65']=$hojaActual->GetCellByColumnAndRow('18',$indiceFila);
                $data['c_100']=$hojaActual->GetCellByColumnAndRow('19',$indiceFila);
                $data['c_125']=$hojaActual->GetCellByColumnAndRow('20',$indiceFila);
                $data['c_150']=$hojaActual->GetCellByColumnAndRow('21',$indiceFila);
                $data['c_200']=$hojaActual->GetCellByColumnAndRow('22',$indiceFila);
                $data['c_250']=$hojaActual->GetCellByColumnAndRow('23',$indiceFila);
                $data['c_300']=$hojaActual->GetCellByColumnAndRow('24',$indiceFila);
                $data['c_325']=$hojaActual->GetCellByColumnAndRow('25',$indiceFila);
                $data['c_350']=$hojaActual->GetCellByColumnAndRow('26',$indiceFila);
                $data['c_375']=$hojaActual->GetCellByColumnAndRow('27',$indiceFila);
                $data['c_400']=$hojaActual->GetCellByColumnAndRow('28',$indiceFila);
                $data['c_425']=$hojaActual->GetCellByColumnAndRow('29',$indiceFila);
                $data['c_450']=$hojaActual->GetCellByColumnAndRow('30',$indiceFila);
                $data['c_475']=$hojaActual->GetCellByColumnAndRow('31',$indiceFila);
                $data['c_500']=$hojaActual->GetCellByColumnAndRow('32',$indiceFila);
                $data['c_525']=$hojaActual->GetCellByColumnAndRow('33',$indiceFila);
                $data['c_550']=$hojaActual->GetCellByColumnAndRow('34',$indiceFila);
                $data['c_575']=$hojaActual->GetCellByColumnAndRow('35',$indiceFila);
                $data['c_600']=$hojaActual->GetCellByColumnAndRow('36',$indiceFila);
                $data['c_625']=$hojaActual->GetCellByColumnAndRow('37',$indiceFila);
                $data['c_650']=$hojaActual->GetCellByColumnAndRow('38',$indiceFila);
                $data['c_675']=$hojaActual->GetCellByColumnAndRow('39',$indiceFila);
                $data['c_700']=$hojaActual->GetCellByColumnAndRow('40',$indiceFila);
                $data['c_725']=$hojaActual->GetCellByColumnAndRow('41',$indiceFila);
                $data['c_750']=$hojaActual->GetCellByColumnAndRow('42',$indiceFila);
                $data['c_775']=$hojaActual->GetCellByColumnAndRow('43',$indiceFila);
                $data['c_800']=$hojaActual->GetCellByColumnAndRow('44',$indiceFila);
                $data['c_825']=$hojaActual->GetCellByColumnAndRow('45',$indiceFila);
                $data['c_850']=$hojaActual->GetCellByColumnAndRow('46',$indiceFila);
                $data['c_875']=$hojaActual->GetCellByColumnAndRow('47',$indiceFila);
                $data['c_900']=$hojaActual->GetCellByColumnAndRow('48',$indiceFila);
                $data['c_950']=$hojaActual->GetCellByColumnAndRow('49',$indiceFila);
                $data['c_1000']=$hojaActual->GetCellByColumnAndRow('50',$indiceFila);
                $data['c_1050']=$hojaActual->GetCellByColumnAndRow('51',$indiceFila);
                $data['c_1100']=$hojaActual->GetCellByColumnAndRow('52',$indiceFila);
                $data['c_1150']=$hojaActual->GetCellByColumnAndRow('53',$indiceFila);
                $data['c_1200']=$hojaActual->GetCellByColumnAndRow('54',$indiceFila);
                $data['c_1250']=$hojaActual->GetCellByColumnAndRow('55',$indiceFila);
                $data['c_1300']=$hojaActual->GetCellByColumnAndRow('56',$indiceFila);
                $data['c_1350']=$hojaActual->GetCellByColumnAndRow('57',$indiceFila);
                $data['c_1400']=$hojaActual->GetCellByColumnAndRow('58',$indiceFila);
                $data['c_1450']=$hojaActual->GetCellByColumnAndRow('59',$indiceFila);
                $data['c_1500']=$hojaActual->GetCellByColumnAndRow('60',$indiceFila);
                $data['c_1550']=$hojaActual->GetCellByColumnAndRow('61',$indiceFila);
                $data['c_1600']=$hojaActual->GetCellByColumnAndRow('62',$indiceFila);
                
                $this->ModeloGeneral->tabla_inserta('material',$data);


                //$dataeqarrayinsert[]=$arraydata;

                
              }
              /*
                if(count($dataeqarrayinsert)>0){
                    $this->ModeloCatalogos->insert_batch('config_vuelos',$dataeqarrayinsert);
                }
              */
              
        //}
    }

}
