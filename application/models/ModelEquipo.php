<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelEquipo extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'e.id',
            1=>'e.tipo',
            2=>'e.marca',
            3=>'e.modelo',
            4=>'e.serie',
            5=>'e.no_certificado',
            6=>'DATE_FORMAT(e.fecha_cal,  "%d/%m/%Y" ) AS fecha_cal',
            7=>'DATE_FORMAT(e.vigencia_cal,  "%d/%m/%Y" ) AS vigencia_cal',
        );
        $columnsx = array( 
            0=>'e.id',
            1=>'e.tipo',
            2=>'e.marca',
            3=>'e.modelo',
            4=>'e.serie',
            5=>'e.no_certificado',
            6=>'e.fecha_cal',
            7=>'e.vigencia_cal',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('equipo e');
        $where = array('e.activo'=>1);
        $this->db->where($where);
        if($params['tipo_equipo']==1){
            $this->db->where(array('e.tipo'=>1));
        }else if($params['tipo_equipo']==2){
            $this->db->where(array('e.tipo'=>2));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsx[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_result($params){
        $columns = array( 
            0=>'e.id',
            1=>'e.tipo',
            2=>'e.marca',
            3=>'e.modelo',
            4=>'e.serie',
            5=>'e.no_certificado',
            6=>'e.fecha_cal',
            7=>'e.vigencia_cal',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('equipo e');
        $where = array('e.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}