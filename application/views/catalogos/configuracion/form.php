<!-- Account Settings starts -->
<div class="row">
    <div class="col-md-3 mt-3">
        <!-- Nav tabs -->
        <ul class="nav flex-column nav-pills" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">
                    <i class="fa fa-wrench"></i>
                    <span class="align-middle">Generales</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="formula-tab" data-toggle="tab" href="#formula" role="tab" aria-controls="formula" aria-selected="false">
                    <i class="fa fa-flask"></i>
                    <span class="align-middle">Formula</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="dimenciones-tab" data-toggle="tab" href="#dimenciones" role="tab" aria-controls="dimenciones" aria-selected="false">
                    <i class="fa fa-cube"></i>
                    <span class="align-middle">Dimensiones</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-9">
        <!-- Tab panes -->
        <h3>Nueva configuración de tapa</h3>
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="tab-content">
                        <!-- general Tab -->
                        <div class="tab-pane active" id="general" role="tabpanel" aria-labelledby="general-tab">
                            <div class="form-group row align-items-center">
                                <div class="col-lg-3" align="right">
                                    <label>Tipo o nombre de Tapa</label>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-3" align="right">
                                    <label>Última actualización</label>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-3" align="right">
                                    <label>Usuario</label>
                                </div>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" >
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12 d-flex flex-sm-row flex-column">
                                    <button type="button" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                        <!-- formula Tab -->
                        <div class="tab-pane" id="formula" role="tabpanel" aria-labelledby="formula-tab">
                            <div class="row">
                                <div class="col-2 form-group" align="right">
                                </div>
                                <div class="col-8 form-group">
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr style="font: oblique 100% cursive; border-bottom: 2px solid;">
                                                <td rowspan="2">t=</td>
                                                <td align="center">PD</td>
                                                <td rowspan="2">&nbsp;&nbsp;&nbsp; or </td>
                                                <td rowspan="2">p=</td>
                                                <td align="center">2SEt cos a</td>
                                            </tr>
                                            <tr style="font: oblique 100% cursive;">
                                                <td align="center">2 cos a(SE-0.6P)</td>
                                                <td align="center">D + 1.2t cos a</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-3 form-group" align="right">
                                    <h3 style="font: oblique 140% cursive;">t</h3>
                                </div>
                                <div class="col-3 form-group">
                                    <input class="form-control" type="text">
                                </div>
                                <div class="col-1 form-group" align="right">
                                    <h3 style="font: oblique 140% cursive;">p</h3>
                                </div>
                                <div class="col-3 form-group">
                                    <input class="form-control" type="text">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12 d-flex flex-sm-row flex-column">
                                    <button type="button" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                        <!-- general Tab -->
                        <div class="tab-pane" id="dimenciones" role="tabpanel" aria-labelledby="dimenciones-tab">
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Diámetro exterior</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(D<span style="font-size: 10px;">o</span>)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Diámetro interior</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(D<span style="font-size: 10px;">int</span>) ó (L)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Espesor de Diseño</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(t<span style="font-size: 10px;">d</span>)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <!--
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Diámetro T.</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(D)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>-->
                            <!--
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Diámetro H.</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(D)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>-->
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Espesor Actual</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(t<span style="font-size: 10px;">s</span>)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Espesor por Corrosión</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(Corr)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <div class="col-lg-4" align="right">
                                    <label>Falta Recta o L</label>
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" >
                                </div>
                                <div class="col-lg-2" align="center">
                                    <b>(F<span style="font-size: 10px;">r</span>)</b>
                                </div>
                                <div class="col-lg-2">
                                    <b>mm</b>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-12 d-flex flex-sm-row flex-column">
                                    <button type="button" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Account Settings ends -->