<div class="row">
    <div class="col-12">
        <h4 class="card-title"><?php echo $tittle ?> Cliente</h4>
        <div class="card">
            
            <div class="card-content">
                <div class="card-body">
                    <form method="post" id="form_data">
                        <input type="hidden" name="id" id="id_reg" value="<?php echo $id ?>">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="media-body" align="center">
                                    <span class="img_foto" align="center">
                                    <?php if($foto!=''){ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>uploads/empresa/<?php echo $foto; ?>" class="rounded mr-3" height="80" width="80">
                                    <?php }else{ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>public/img/empresa.png" height="80" width="80">
                                    <?php } ?>
                                    </span>
                                    <div class="col-12 ">
                                        <br>
                                        <label class="btn btn-sm bg-light-primary mb-sm-0" for="foto_avatar">Subir foto</label>
                                        <input type="file" id="foto_avatar" hidden>
                                        <a class="btn btn-sm bg-light-secondary ml-sm-2" onclick="reset_img()">Reiniciar</a>
                                    </div>
                                    <p class="text-muted mb-0 mt-1 mt-sm-0">
                                        <small>Permitido JPG, GIF or PNG. Tamaño máximo de 800kB</small>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group row align-items-center" style="display:none;">
                                    <div class="col-lg-3" align="right">
                                        <label>Tipo de empresa</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="tipo">
                                            <option disabled="" selected="">Seleccione opción</option>
                                            <option value="1" <?php if($tipo==1) echo 'selected' ?>>Consultoría</option>
                                            <option value="2" <?php if($tipo==2) echo 'selected' ?>>Cliente</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Nombre</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Alias</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="alias" value="<?php echo $alias ?>">
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Abreviación</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="abreviacion" value="<?php echo $abreviacion ?>">
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Dirección</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <textarea rows="4" class="form-control" name="direccion"><?php echo $direccion ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-2">
                                        <label>Seleccionar color</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="color" class="form-control" name="color" value="<?php echo $color ?>">
                                    </div>
                                </div>
                            </div>    
                            <?php 
                                $check_checked='';
                                $check_display='';
                                if($check_datosfiscales==1){
                                    $check_checked='checked';
                                    $check_display='style="display:block"';
                                }else{
                                    $check_checked='';
                                    $check_display='style="display:none"';
                                }
                            ?>
                            <div class="col-12">
                                <label>Agregar datos fiscales</label>
                                <div class="custom-switch custom-control-inline mb-1 mb-xl-0">
                                    <input type="checkbox" class="custom-control-input" id="check_datosfiscales" name="check_datosfiscales" onclick="datosfiscales()" <?php echo $check_checked ?>>
                                    <label class="custom-control-label mr-1" for="check_datosfiscales">
                                        <span style="color: #ff000000;">.</span>
                                    </label>
                                </div>
                            </div>    
                            <div class="col-12">
                                <div class="card row_datosfiscales" <?php echo $check_display ?>>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">RFC</label>
                                                        <input type="text" class="form-control" name="rfc" value="<?php echo $rfc ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Razón social</label>
                                                        <input type="text" class="form-control" name="razon_social" value="<?php echo $razon_social ?>">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Código postal</label>
                                                        <input type="text" class="form-control" oninput="cambiaCP()" id="codigo_postal" name="codigo_postal" value="<?php echo $codigo_postal ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-8">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Calle y número</label>
                                                        <input type="text" class="form-control" name="calle_numero" value="<?php echo $calle_numero ?>">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Colonia</label>
                                                        <select class="form-control colonia" name="colonia" id="colonia">
                                                        <?php if($colonia!=''){ ?>    
                                                            <option value="<?php echo $colonia ?>"><?php echo $colonia ?></option>  
                                                        <?php } ?>    
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Ciudad</label>
                                                        <input type="text" class="form-control" name="ciudad" id="ciudad" value="<?php echo $ciudad ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Estado</label>
                                                        <input type="text" class="form-control" name="estado" id="estado" value="<?php echo $estado ?>">
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Uso de CFDI</label>
                                                        <select class="form-control" name="UsoCFDI">
                                                            <option disabled="" selected="">Seleccione opción</option>
                                                            <?php foreach ($get_cfdi as $x) { ?>
                                                                <option value="<?php echo $x->uso_cfdi ?>" <?php if($x->uso_cfdi==$UsoCFDI) echo 'selected' ?>><?php echo $x->uso_cfdi_text ?></option>  
                                                            <?php } ?>    
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Método de pago</label>
                                                        <select class="form-control" name="metodopago">
                                                            <option disabled="" selected="">Seleccione opción</option>
                                                            <?php foreach ($get_metodopago as $x) { ?>
                                                                <option value="<?php echo $x->id ?>" <?php if($x->id==$metodopago) echo 'selected' ?>><?php echo $x->metodopago.' '.$x->metodopago_text ?></option>  
                                                            <?php } ?>    
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Forma de pago</label>
                                                        <select class="form-control" name="forma_pago">
                                                            <option disabled="" selected="">Seleccione opción</option>
                                                            <?php foreach ($get_formapago as $x) { ?>
                                                                <option value="<?php echo $x->id ?>" <?php if($x->id==$forma_pago) echo 'selected' ?>><?php echo $x->formapago_text ?></option>  
                                                            <?php } ?>    
                                                        </select>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Condiciones de pago</label>
                                                        <input type="text" class="form-control" name="condiciones_pago" value="<?php echo $condiciones_pago ?>">
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-8">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Régimen fiscal</label>
                                                        <select class="form-control" name="regimenfiscalreceptor">
                                                            <option disabled="" selected="">Seleccione opción</option>
                                                            <?php foreach ($get_regimenfiscal as $x) { ?>
                                                                <option value="<?php echo $x->id ?>" <?php if($x->id==$regimenfiscalreceptor) echo 'selected' ?>><?php echo $x->concepto ?></option>  
                                                            <?php } ?>    
                                                        </select>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-8">
                                                    <fieldset class="form-group">
                                                        <label for="basicInput">Correo electrónico</label>
                                                        <input type="email" class="form-control" name="correo" value="<?php echo $correo ?>">
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>        
                            </div>
                        </div>  
                        <hr>
                    </form>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" onclick="add_form()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Empresa" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>