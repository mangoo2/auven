UPDATE `menu` SET `Nombre` = 'Operaciones\r\n' WHERE `menu`.`MenuId` = 3;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `tipo`) VALUES (NULL, '3', 'Recipientes', 'Operaciones/recipientes', 'fa fa-database', '1');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '7');
UPDATE `menu` SET `Icon` = 'fa fa-cube' WHERE `menu`.`MenuId` = 3;
ALTER TABLE `personal` ADD `cedula_prof` VARCHAR(100) NOT NULL AFTER `correo`;

ALTER TABLE `equipo` ADD `tipo_palpa` VARCHAR(35) NOT NULL AFTER `serie`, ADD `size` VARCHAR(35) NOT NULL AFTER `tipo_palpa`, ADD `frecuencia` VARCHAR(35) NOT NULL AFTER `size`;
ALTER TABLE `equipo` ADD `yugo` VARCHAR(100) NOT NULL AFTER `frecuencia`, ADD `part_magne` VARCHAR(35) NOT NULL AFTER `yugo`, ADD `tipo_part` VARCHAR(75) NOT NULL AFTER `part_magne`, ADD `color` VARCHAR(35) NOT NULL AFTER `tipo_part`, ADD `contraste` VARCHAR(55) NOT NULL AFTER `color`;

/* ****************05-06-2023************************* */
ALTER TABLE `levantamiento` ADD `direccion` TEXT NOT NULL AFTER `consultor`, ADD `color` VARCHAR(10) NOT NULL AFTER `direccion`, ADD `logo` TEXT NOT NULL AFTER `color`;
ALTER TABLE `levantamiento_paso5` ADD `niveles` FLOAT NOT NULL AFTER `id_equipo`, ADD `lecturas_nivel` FLOAT NOT NULL AFTER `niveles`;
ALTER TABLE `levantamiento_paso6` ADD `id_equipo_bloque` INT NOT NULL AFTER `id_operacion`;

/* **********************10-08-2023**************************** */
ALTER TABLE `tapas_paso2` ADD `alt_concava` DECIMAL(6,2) NULL AFTER `num_puntos`;
ALTER TABLE `tapas_paso2` ADD `radio_sup` FLOAT NOT NULL COMMENT 'para tapas conicas' AFTER `alt_concava`, ADD `radio_inf` FLOAT NOT NULL COMMENT 'para tapas conicas' AFTER `radio_sup`, ADD `falda_recta` FLOAT NOT NULL COMMENT 'para tapas conicas' AFTER `radio_inf`;
ALTER TABLE `levantamiento_paso4` ADD `justif_tecnica` TEXT NOT NULL AFTER `tipo_disp`;
ALTER TABLE `fluidos` ADD `peligroso` VARCHAR(1) NOT NULL AFTER `nombre`;
ALTER TABLE `levantamiento_paso6` ADD `num_stps` VARCHAR(75) NOT NULL AFTER `categoria_rsp`;
ALTER TABLE `tapas_paso2` ADD `altura_esferica` DECIMAL(6,2) NOT NULL AFTER `falda_recta`, ADD `radio_esferica` DECIMAL(6,2) NOT NULL AFTER `altura_esferica`, ADD `radio_interior` DECIMAL(6,2) NOT NULL AFTER `radio_esferica`, ADD `radio_interno` DECIMAL(6,2) NOT NULL AFTER `radio_interior`;
ALTER TABLE `tapas_paso2` ADD `parte_recta` DECIMAL(6,2) NOT NULL AFTER `radio_interno`;
ALTER TABLE `levantamiento_paso2` CHANGE `capacidad` `capacidad` DECIMAL(8,2) NOT NULL;

/* ********************20-10-23****************************/
ALTER TABLE `levantamiento_paso3` CHANGE `presion_hidro` `presion_hidro` VARCHAR(8) NOT NULL;

/************************1-11-2023**********************************/
ALTER TABLE `levantamiento_paso2` ADD `factorc` DECIMAL(6,2) NOT NULL AFTER `espesor_act`;

ALTER TABLE `levantamiento_paso3` CHANGE `presion_hidro` `presion_hidro` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
/* **********************05-12-23********************************/

ALTER TABLE `levantamiento_paso3` ADD `corrosion` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no aplica, 1= aplica a formula de corrosion' AFTER `factor_seguridad`;