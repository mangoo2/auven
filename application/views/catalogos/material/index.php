<div class="row">
    <div class="col-12">
        <div class="content-header">Listado de materiales</div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body table-responsive">
                    <a href="<?php echo base_url(); ?>Material/registrar/" class="btn btn-sistema mr-sm-2 mb-3"><i class="fa fa-flask"></i> &nbsp; Nuevo material</a>
                    <div class="row">
                        <div class="col-md-12" style="text-align: end;">
                            <a type="button" class="btn btn-success mr-1 mb-1" onclick="cargarmateriales()">Cargar Materiales</a>
                        </div>
                    </div>
                    <table class="table text-center m-0" id="table_data">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nominal Composition</th>
                                <th>Product Form</th>
                                <th>Spec. No</th>
                                <th>Type/Grande</th>
                                <th>Alloy Desig./ UNS No.</th>
                                <th>Class/ Condition/ Temper</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



