var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    $("#documento").fileinput({
            
            showUpload: true,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["xlsx"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Material/inserupdate',
            inputGroupClass: "input-group-sm",
            maxFilePreviewSize: 5000,
            preferIconicPreview: true,
            previewFileIconSettings: { // configure your icon file extensions
                'doc': '<i class="fas fa-file-word text-primary"></i>',
                'xls': '<i class="fas fa-file-excel text-success"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fas fa-file-powerpoint text-danger"></i>',
                'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                'zip': '<i class="fas fa-file-archive text-muted"></i>',
                'htm': '<i class="fas fa-file-code text-info"></i>',
                'txt': '<i class="fas fa-file-alt text-info"></i>',
                'mov': '<i class="fas fa-file-video text-warning"></i>',
                'mp3': '<i class="fas fa-file-audio text-warning"></i>',
                // note for these file types below no extension determination logic 
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fas fa-file-image text-danger"></i>', 
                'gif': '<i class="fas fa-file-image text-muted"></i>', 
                'png': '<i class="fas fa-file-image text-primary"></i>'    
            },
            uploadExtraData: function (previewId, index) {
                var info = {
                            rid:0

                        };
                return info;
            }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
            setTimeout(function(){ 
                reload_registro();
            }, 2000);
          //location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    }); 
});

function reload_registro(){
    tabla.destroy();
    loadtable();
}
function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Material/getlistado",
            type: "post",
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nominal_composition"},
            {"data":"product_form"},
            {"data":"spec_no"},
            {"data":"type_grande"},
            {"data":"alloy_desig"},
            {"data":"class_condition"}, 
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                        <a href="'+base_url+'Material/registrar/'+row.id+'" class="btn btn-outline-dark mb-1"><i class="icon-xl far fa-edit"></i></a>\
                        <a data-alias="'+row.nominal_composition+'" onclick="delete_data('+row.id+')" class="btn btn-outline-dark mb-1 id_reg_'+row.id+'"><i class="icon-xl far fa-trash-alt"></i></a>';
                    html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}

function delete_data(id){
    var nombre=$('.id_reg_'+id).data('alias');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar a '+nombre+'?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Material/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function cargarmateriales(){
    $('#documento').fileinput('clear');
    $('#modalcarga').modal({backdrop: 'static', keyboard: false});
}