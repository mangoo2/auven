<!DOCTYPE html>
<html lang="en" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Mangoo">
    <title>AUVEN</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/img/FAV.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/img/FAV.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/switchery.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/layout-dark.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/plugins/switchery.css">
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css-rtl/pages/dashboard1.css">-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
    <!-- END APEX CSS-->
    <link href="<?php echo base_url();?>plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/datatables/datatables.bundle.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/alert/sweetalert.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/pages/form-wizard.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/fileinput/fileinput.css">

    <link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->
        <input type="hidden" name="ssessius" id="ssessius" value="<?php echo $_SESSION['perfilid_tz'];?>" readonly>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" readonly>
    </head>
    <style type="text/css">
        @font-face {
            font-family: "Helvetica";
            src: url("<?php echo base_url(); ?>public/Helvetica.ttf");
        }
    </style>
<body class="vertical-layout vertical-menu 2-columns  navbar-sticky" data-menu="vertical-menu" data-col="2-columns">