<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operaciones extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelOperaciones');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid,7);
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/recipientes/index');
    $this->load->view('templates/footer');
    $this->load->view('operaciones/recipientes/indexjs');
  }

  public function recipientes()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/recipientes/index');
    $this->load->view('templates/footer');
    $this->load->view('operaciones/recipientes/indexjs');
  }

  public function nueva($id=0)
  {
    $data=[];
    if($id>0){
      $data["r"]=$this->ModelOperaciones->getOperacion($id);
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/recipientes/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/recipientes/formjs');
  }

  public function searchClientes(){
    $cli = $this->input->get('search');
    $results=$this->ModeloGeneral->getselect_like("empresa","alias",$cli,array("activo"=>1));
    echo json_encode($results);
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'operaciones');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('operaciones',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelOperaciones->get_result($params);
    $totaldata= $this->ModelOperaciones->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'operaciones');
    echo $resul;
  }

  /* ************LEVANTAMIENTO******************** */
  public function levantamiento($id)
  {
    $data["id_operacion"]=$id;
    $data["fluido"]=$this->ModeloGeneral->getselectwhere_n_consulta('fluidos',array('activo'=>1));
    $data["equi"]=$this->ModeloGeneral->getselectwhere_n_consulta('equipo',array('activo'=>1));
    $data["mat"]=$this->ModeloGeneral->getselectwhere_n_consulta('material',array('activo'=>1));
    $data["eq_blo"]=$this->ModeloGeneral->getselectwhere_n_consulta('equipo_bloque',array('estatus'=>1));
    if($id>0){
      $data["l"]=$this->ModeloGeneral->getselectwhererow2("levantamiento",array('id_operacion'=>$id));
      $data["l2"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso2",array('id_operacion'=>$id));
      $data["l3"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso3",array('id_operacion'=>$id));
      $data["l4"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso4",array('id_operacion'=>$id));
      $data["l5"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso5",array('id_operacion'=>$id));
      $data["l6"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso6",array('id_operacion'=>$id));
    }
    $data["tec"]=$this->ModeloGeneral->getselectwhere_n_consulta('personal',array('estatus'=>1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('operaciones/recipientes/levantamiento',$data);
    $this->load->view('templates/footer');
    $this->load->view('operaciones/recipientes/levantamientojs');
  }

  public function getDetallesMaterial(){
    $id = $this->input->post('id');
    $temp_dise = $this->input->post('temp_dise');
    //log_message('error', "temp_dise: ".$temp_dise); 
    $results=$this->ModeloGeneral->getselectwhere_n_consulta("material",array('id'=>$id));
    $ksi=$results[0]->ksi;
    if($temp_dise==40 || $temp_dise==65 || $temp_dise==100 || $temp_dise==125 || $temp_dise==150 || $temp_dise==200 || $temp_dise==250 || $temp_dise==300 || $temp_dise==325 || $temp_dise==350 || $temp_dise==375 || $temp_dise==400 || $temp_dise==425 || $temp_dise==450 || $temp_dise==475 || $temp_dise==500 || $temp_dise==525 || $temp_dise==550 || $temp_dise==575 || $temp_dise==600 || $temp_dise==625 || $temp_dise==650 || $temp_dise==675 || $temp_dise==700 || $temp_dise==725 || $temp_dise==750 || $temp_dise==775 || $temp_dise==800 || $temp_dise==825 || $temp_dise==850 || $temp_dise==875 || $temp_dise==900 || $temp_dise==925 || $temp_dise==950 || $temp_dise==1000 || $temp_dise==1050 || $temp_dise==1100 || $temp_dise==1150 || $temp_dise==1200 || $temp_dise==1250 || $temp_dise==1300 || $temp_dise==1350 || $temp_dise==1400 || $temp_dise==1450 || $temp_dise==1500 || $temp_dise==1550 || $temp_dise==1600){
      $get_td=$this->ModelOperaciones->getTempDiseno("c_".$temp_dise,$id);
    }
    if(isset($get_td)){
      //log_message('error', "Existe temperatura: ".$get_td->temp_diseno);
      $y=$get_td->temp_diseno; 
    }else{
      $array_tmps=array(40,65,100,125,150,200,250,300,325,350,375,400,425,450,475,500,525,550,575,600,625,650,675,700,725,750,775,800,825,850,875,900,925,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1550,1600);
      //log_message('error', "array_tmps: ".var_dump($array_tmps)); 

      $menor = 0;
      $cercanomay = 0;
      for($i=0; $i<count($array_tmps);$i++){
        if($array_tmps[$i]<$temp_dise){
          $menor=$array_tmps[$i];
          //break;
          //log_message('error', "cercano menor: ".$menor);
        }else if( ($array_tmps[$i] - $temp_dise) >= 0 ){
          $cercanomay = $array_tmps[$i];
          //log_message('error', "cercanomay: ".$cercanomay); 
          break;
        }
      }
      $x1=$results[0]->{"c_".$menor}; $x2=$results[0]->{"c_".$cercanomay};
      //log_message('error', "x1: ".$x1);
      //log_message('error', "x2: ".$x2);

      //ksi2=menor, ksi=mayor y1=menor, y2=mayor
      $y1=($results[0]->ksi2*1000)/14.22; $y2=($results[0]->ksi*1000)/14.22;
      $y= $y1 + ($y2 - $y1)/($x2 - $x1)-($temp_dise - $x1);
    }
    //log_message('error', "y: ".$y);
    echo json_encode(array("results"=>$results,"ksi"=>$ksi,"y"=>round($y,2)));
  }

  public function getMateriales(){
    $id_mat = $this->input->post("id_mat");
    $html="";
    $results=$this->ModeloGeneral->getselectwhere_n_consulta("material",array('activo'=>1));
    foreach($results as $m){
      if($m->id==$id_mat) $sel="selected"; else $sel="";
      $html.="<option ".$sel." value='".$m->id."'>".$m->nominal_composition." - ".$m->type_grande."</option>";
    }
    echo $html;
  }

  public function calcular(){
    $id_operacion = $this->input->post("id_operacion");
    $emr=0; $pimp=0; $volumen=0; $name_tapa=""; $html=""; $vol_total=0; $array_pmtp=array();
    $gp3=$this->ModeloGeneral->getselectwhererow2('levantamiento_paso3',array('estatus'=>1,'id_operacion'=>$id_operacion));
    $tap=$this->ModeloGeneral->getselectwhere_n_consulta('tapas_paso2',array('estatus'=>1,'id_operacion'=>$id_operacion));

    $l2=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso2",array('id_operacion'=>$id_operacion));
    $l4=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso4",array('id_operacion'=>$id_operacion));
    $l5=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso5",array('id_operacion'=>$id_operacion));
    $l6=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso6",array('id_operacion'=>$id_operacion));

    foreach($tap as $t){
      /*$volumen=(((3.1416/24)*pow($t->d_int,3))+(3.1416*pow($t->d_int/2,2)*$t->longitud))/pow(10,6);
      $vol_total=$vol_total+$volumen;
      $emr=($gp3->presion_diseno*$t->d_int)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
      $pimp=(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->d_int+0.2*$t->espesor);*/

      if($t->tipo_tapa=="1"){ //elipsoidal -- ok
        $volumen=(((3.1416/24)*pow($t->d_int,3))+3.1416*pow($t->d_int/2,2)*$t->longitud)/pow(10,6);
        $vol_total=$vol_total+$volumen;
        $emr=($gp3->presion_diseno*$t->d_int)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
        $pimp=(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->d_int+0.2*$t->espesor);
      }else if($t->tipo_tapa=="2"){ //hemisferica -- ok
        //$volumen=round( (2/3)*3.1416* pow(($t->d_int/2),3) / pow(10,6),2); //TAPIA
        $volumen=round( (4/3)*3.1416* pow(($t->d_int/2),3) / pow(10,6),2); //NUEVO EJEMPLO
        $vol_total=$vol_total+$volumen;
        //log_message('error', "(4/3): ".(4/3));
        //log_message('error', "pow((t->d_int/2),2): ".pow(($t->d_int/2),2));
        $emr=($gp3->presion_diseno*$t->longitud)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
        $pimp=round(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor/($t->longitud+(0.2*$t->espesor)),2);
        /*log_message('error', "esfuerzo_diseno:  ".$gp3->esfuerzo_diseno);
        log_message('error', "eficiencia_solda:  ".$gp3->eficiencia_solda);
        log_message('error', "espesor:  ".$t->espesor);
        log_message('error', "longitud:  ".$t->longitud);*/
      }else if($t->tipo_tapa=="3"){ //toriesfericas 100/6 --ok
        //$volumen=round((3.1416/37)*pow($t->d_int,3) + (3.1416*pow(($t->d_int/2),2)*$t->longitud) / pow(10,6),2);
        $vol_abomb=round(3.1416 * pow($t->alt_concava,2)*($t->longitud-($t->alt_concava/3)) / pow(10,6),2);
        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
        $vol_total=$vol_total+$vol_abomb+$vol_esferi+$vol_cilin;
        $volumen=$vol_abomb+$vol_esferi+$vol_cilin;

        /*log_message('error', "esfuerzo_diseno: ".$gp3->esfuerzo_diseno);
        log_message('error', "eficiencia_solda: ".$gp3->eficiencia_solda);
        log_message('error', "espesor: ".$t->espesor);
        log_message('error', "0.885: ");
        log_message('error', "longitud: ".$t->longitud);
        log_message('error', "0.1: ");
        log_message('error', "espesor: ".$t->espesor);*/

        //$emr=round((0.885*$gp3->presion_diseno*($t->d_int/2))/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.1*$gp3->presion_diseno),2);
        $emr=round(0.885*$gp3->presion_diseno*$t->longitud/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda)-(0.1*$gp3->presion_diseno),2);
        $pimp=round($gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor/(0.885*$t->longitud)+(0.1*$t->espesor),2);
      }else if($t->tipo_tapa=="4"){ //toriesfericas --ok
        $vol_abomb=round(3.1416 * pow($t->alt_concava,2)*($t->longitud-($t->alt_concava/3)) / pow(10,6),2);
        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
        $vol_total=$vol_total+$vol_abomb+$vol_esferi+$vol_cilin;
        $volumen=$vol_abomb+$vol_esferi+$vol_cilin;

        $emr=round(($gp3->presion_diseno*$t->longitud*$l4->peso_molecular)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda)-(0.2*$gp3->presion_diseno),2);
        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->longitud*$l4->factor)+(0.2*$t->espesor),2);
      }else if($t->tipo_tapa=="5"){ //Plana -- ok
        $volumen=0;
        $vol_total=$vol_total+$volumen;
        $emr=round($t->d_int * sqrt(($l2->factorc*$gp3->presion_diseno)/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda)),2);
        $pimp=round(pow($t->espesor,2)*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda/($l2->factorc*pow($t->d_int,2)) ,2);
        /*log_message('error', "d_int : ".$t->d_int);
        log_message('error', "factorc : ".$l2->factorc);
        log_message('error', "presion_diseno : ".$gp3->presion_diseno);
        log_message('error', "esfuerzo_diseno : ".$gp3->esfuerzo_diseno);
        log_message('error', "eficiencia_solda : ".$gp3->eficiencia_solda);*/
        
        /*log_message('error', "espesor 2: ".pow($t->espesor,2));
        log_message('error', "esfuerzo_diseno : ".$gp3->esfuerzo_diseno);
        log_message('error', "eficiencia_solda : ".$gp3->eficiencia_solda);
        log_message('error', "factorc : ".$l2->factorc);
        log_message('error', "d_int 2: ".pow($t->d_int,2));
        log_message('error', "volumen: ".$volumen);
        log_message('error', "vol_total: ".$vol_total);
        log_message('error', "emr: ".$emr);
        log_message('error', "pimp: ".$pimp);*/
      }else if($t->tipo_tapa=="6"){ //Plana circular apernada 
        $volumen=round((3.1416/37)*pow($t->d_int,3) + (3.1416*pow(($t->d_int/2),2)*$t->longitud) / pow(10,6),2);
        $vol_total=$vol_total+$volumen;
        $emr=round(($t->d_int * sqrt($l4->factor_compre*$gp3->presion_diseno))/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda),2);
        $pimp=round(pow($t->espesor,2)*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda/$l4->factor_compre*pow($t->d_int,2),2);
      }else if($t->tipo_tapa=="7"){ //abombada -- volumen formula ajustada con excel de formulas 
        $volumen=round( 3.1416 * pow($t->alt_concava,2) * ($t->d_int - $t->alt_concava/3) / pow(10,6),2);
        $vol_total=$vol_total+$volumen;
        $emr=round((5*$gp3->presion_diseno*$t->d_int)/(6*$gp3->esfuerzo_diseno),2); //pendientes por realizar- no existen en excel
        $pimp=round((6*$gp3->esfuerzo_diseno*$t->espesor)/(5*$t->d_int),2); //pendientes por realizar- no existen en excel

        //log_message('error', "alt_concava : ".$t->alt_concava);
        //log_message('error', "d_int : ".$t->d_int);
        /*log_message('error', "presion_diseno : ".$gp3->presion_diseno);
        log_message('error', "d_int : ".$t->d_int);
        log_message('error', "esfuerzo_diseno : ".$gp3->esfuerzo_diseno);
        log_message('error', "volumen: ".$volumen);
        log_message('error', "vol_total: ".$vol_total);
        log_message('error', "emr: ".$emr);
        log_message('error', "pimp: ".$pimp);*/
      }
      else if($t->tipo_tapa=="8"){ //volumen formula ajustada con excel de formulas 
        $volumen=round(1/3 * 3.1416 * $t->falda_recta*( pow($t->radio_sup,2) + pow($t->radio_inf,2) + ($t->radio_sup * $t->radio_inf)) / pow(10,6),2);
        $vol_total=$vol_total+$volumen;
        $c1=$t->falda_recta;
        $c2=$t->radio_sup/2;
        $c1 = pow($c1,2);
        $c2 = pow($c2,2);
        $h=$c1+$c2;
        $h=round(sqrt($h),2);
        //log_message('error', "h: ".$h);
        //log_message('error', "falda_recta: ".$t->falda_recta);
        $angulo = $t->falda_recta/$h;
        $ang = asin($angulo);
        $angulo2 = round(acos($angulo)/pi()*180,2);
        /*$angulo = round($t->falda_recta/$t->radio_sup,2);  
        $angulo2 = round(atan($angulo),2); 
        $angulo2 = $angulo2/2;*/
        //log_message('error', "angulo2: ".$angulo2);
        if(cos($angulo2)<0){
          $cos = cos($angulo2)* -1; 
        }else{
          $cos = cos($angulo2); 
        }
        $emr=round($gp3->presion_diseno*$t->d_int / ((2 * $cos) *(($gp3->esfuerzo_diseno*$gp3->eficiencia_solda - 0.6 * $gp3->presion_diseno))),2); //
        /*log_message('error', "presion_diseno: ".$gp3->presion_diseno);
        log_message('error', "d_int: ".$t->d_int); 
        log_message('error', "cos: ".$cos); 
        log_message('error', "esfuerzo_diseno: ".$gp3->esfuerzo_diseno); 
        log_message('error', "eficiencia_solda: ".$gp3->eficiencia_solda); */
        $pimp=round(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor * $cos/($t->d_int+1.2*$t->espesor * $cos),2); //pendientes por valida
      }
      else if($t->tipo_tapa=="9"){ //toriconicas, faltan formulas de espesor y presion
        $vol_coni=round(1/3 * 3.1416 * $t->falda_recta*( pow($t->radio_sup,2) + pow($t->radio_inf,2) + ($t->radio_sup * $t->radio_inf)) / pow(10,6),2);
        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
        $vol_total=$vol_total+$vol_coni+$vol_esferi+$vol_cilin;
        $volumen=$vol_coni+$vol_esferi+$vol_cilin;

        $c1=$t->falda_recta;
        $c2=$t->radio_sup/2;
        $c1 = pow($c1,2);
        $c2 = pow($c2,2);
        $h=$c1+$c2;
        $h=round(sqrt($h),2);
        $angulo = $t->falda_recta/$h;
        //log_message('error', "angulo: ".$angulo);
        $ang = asin($angulo);
        $angulo2 = round(acos($angulo)/pi()*180,2);
        if(cos($angulo2)<0){
          $cos = cos($angulo2)* -1; 
        }else{
          $cos = cos($angulo2); 
        }
        $emr=round(($gp3->presion_diseno*$t->d_int) / (2 * $cos *(($gp3->esfuerzo_diseno*$gp3->eficiencia_solda - 0.6 * $gp3->presion_diseno))),2); //pendientes por validar
        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor * $cos)/($t->d_int+1.2*$t->espesor * $cos),2); 
      }
      
      if($t->tipo_tapa=="1"){ $name_tapa="Elipsoidal"; }
      if($t->tipo_tapa=="2"){ $name_tapa="Hemisferica"; }
      if($t->tipo_tapa=="3"){ $name_tapa="Toriesférica 100/6"; }
      if($t->tipo_tapa=="4"){ $name_tapa="Toriesférica"; }
      if($t->tipo_tapa=="5"){ $name_tapa="Plana"; }
      if($t->tipo_tapa=="6"){ $name_tapa="Plana circular apernada"; }
      if($t->tipo_tapa=="7"){ $name_tapa="Abombada"; }
      if($t->tipo_tapa=="8"){ $name_tapa="Conicas"; }
      if($t->tipo_tapa=="9"){ $name_tapa="Toriconicas"; }
      $html.= "<tr>
          <td>".$name_tapa."</td>
          <td>".round($emr,2)."</td>
          <td>".round($pimp,2)."</td>
          <td>".round($volumen,2)."</td>
        </tr>";
      array_push($array_pmtp,$pimp);
    }

    $volumen_env=0; $emr_env=0; $pimp_env=0;
    $l=$this->ModeloGeneral->getselectwhererow2("levantamiento",array('id_operacion'=>$id_operacion));
    $gp2=$this->ModeloGeneral->getselectwhere_n_consulta2('levantamiento_paso2',array('estatus'=>1,'id_operacion'=>$id_operacion));
    if($gp2->num_rows()>0){
      $gp2=$gp2->row();
      /*$volumen_env=(3.1416*pow($gp2->d_int/2,2)*$gp2->cuerpo)/pow(10,6);
      $vol_total=$vol_total+$volumen_env;
      $emr_env=($gp3->presion_diseno*($gp2->d_int/2))/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.6*$gp3->presion_diseno);
      $pimp_env=($gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$gp2->espesor_act)/($gp2->d_int/2+0.6*$gp2->espesor_act);*/
      if($l->tipo_env=="1"){ //cilindrico
        $volumen_env=(3.1416*pow($gp2->d_int/2,2)*$gp2->cuerpo)/pow(10,6); //--ok
        $emr_env=$gp3->presion_diseno*($gp2->d_int/2) / ($gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.6*$gp3->presion_diseno);
        $pimp_env=$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$gp2->espesor_act/(($gp2->d_int/2)+0.6*$gp2->espesor_act);
      }else{  //esferico
        $volumen_env=(3.1416*pow($gp2->d_int/2,2)*$gp2->cuerpo)/pow(10,6);
        $emr_env=$gp3->presion_diseno*($gp2->d_int/2) / (2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
        $pimp_env=(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$gp2->espesor_act)/(($gp2->d_int/2)+0.2*$gp2->espesor_act);
      }
      $vol_total=$vol_total+$volumen_env;
      $this->ModeloGeneral->updateCatalogo(array("capacidad"=>$vol_total),'id_operacion',$id_operacion,"levantamiento_paso2");
    }
    $html.= "<tr>
      <td>Cuerpo</td>
      <td>".round($emr_env,2)."</td>
      <td>".round($pimp_env,2)."</td>
      <td>".round($volumen_env,2)."</td>
    </tr>";
    array_push($array_pmtp,$pimp_env);
    //log_message('error', "array_pmtp: ".var_dump($array_pmtp)); 
    echo json_encode(array("html"=>$html,"vol_total"=>$vol_total,"pmtp"=>max($array_pmtp)));
  }

  public function guardarLevantamiento(){
    $datos = $this->input->post();
    $tabla = $this->input->post("tabla");
    $id=$datos['id'];
    unset($datos['id']);
    unset($datos['tabla']);
    /*if($tabla=="levantamiento_paso5"){
      $espesor_max=$datos['espesor_max'];
      $num_puntos=$datos['num_puntos'];
      unset($datos['espesor_max']);
      unset($datos['num_puntos']);
      $this->ModeloGeneral->updateCatalogo(array("espesor_max"=>$espesor_max,"num_puntos"=>$num_puntos),'id_operacion',$datos["id_operacion"],"levantamiento_paso2");
    }*/
    if($tabla=="levantamiento_paso4"){
      $vol_total=$datos["vol_total"];
      unset($datos["vol_total"]);
    } 
    $id_reg=0;
    if($tabla=="levantamiento_paso6"){
      unset($datos["tipo_equi"]);
    }
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,$tabla);
      $id_reg=$id; 
    }else{
      if($tabla=="levantamiento"){
        $datos['fecha_reg']=$this->fechahoy;
      }
      $id_reg=$this->ModeloGeneral->tabla_inserta($tabla,$datos);
    } 
    if($tabla=="levantamiento_paso4"){
      $this->ModeloGeneral->updateCatalogo(array("capacidad"=>$vol_total),'id_operacion',$datos["id_operacion"],"levantamiento_paso2");
    } 
    echo $id_reg;
  }

  public function delete_tapa(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'tapas_paso2');
    echo $resul;
  }

  public function guarda_tapas(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) {   
      $data['id_operacion']=$DATA[$i]->id_operacion;        
      $data['tipo_tapa']=$DATA[$i]->tipo_tapa;
      $data['id_material']=$DATA[$i]->id_mat_tapa;
      $data['d_ext']=$DATA[$i]->d_ext;
      $data['d_int']=$DATA[$i]->d_int;
      $data['longitud']=$DATA[$i]->longitud;
      $data['espesor']=$DATA[$i]->espesor;
      //$data['falda']=$DATA[$i]->falda;
      if($DATA[$i]->tipo_tapa==3 || $DATA[$i]->tipo_tapa==4){
        $data['alt_concava']=$DATA[$i]->alt_concava;
        $data['altura_esferica']=$DATA[$i]->altura_esferica;
        $data['radio_esferica']=$DATA[$i]->radio_esferica;
        $data['radio_interior']=$DATA[$i]->radio_interior;
        $data['radio_interno']=$DATA[$i]->radio_interno;
        $data['parte_recta']=$DATA[$i]->parte_recta;
      } 
      if($DATA[$i]->tipo_tapa==7){
        $data['alt_concava']=$DATA[$i]->alt_concava;
      }  
      if($DATA[$i]->tipo_tapa==8){
        $data['radio_sup']=$DATA[$i]->radio_sup;
        $data['radio_inf']=$DATA[$i]->radio_inf;
        $data['falda_recta']=$DATA[$i]->falda_recta;
      }
      if($DATA[$i]->tipo_tapa==9){
        $data['falda_recta']=$DATA[$i]->falda_recta_tori;
        $data['radio_sup']=$DATA[$i]->radio_sup_tori;
        $data['radio_inf']=$DATA[$i]->radio_inf_tori;
        $data['altura_esferica']=$DATA[$i]->altura_esferica_tori;
        $data['radio_esferica']=$DATA[$i]->radio_esferica_tori;
        $data['radio_interior']=$DATA[$i]->radio_interior_tori;
        $data['radio_interno']=$DATA[$i]->radio_interno_tori;
        $data['parte_recta']=$DATA[$i]->parte_recta_tori;
      }   
      if($DATA[$i]->id==0){
        $this->ModeloGeneral->tabla_inserta('tapas_paso2',$data);
      }else{
        $this->ModeloGeneral->updateCatalogo($data,'id',$DATA[$i]->id,'tapas_paso2');
      }
    }
  }

  public function guarda_detalle_tapas(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) {           
      $data['espesor_max']=$DATA[$i]->espesor_max;
      $data['num_puntos']=$DATA[$i]->num_puntos; 
      
      $this->ModeloGeneral->updateCatalogo($data,'id',$DATA[$i]->id,'tapas_paso2');
    }
  }

  public function guarda_espesor_tapas(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) {
      $data['id_operacion']=$DATA[$i]->id_operacion;
      $data['esp_tapa1']=$DATA[$i]->esp_tapa1;        
      $data['esp_tapa2']=$DATA[$i]->esp_tapa2;
      $data['esp_cuerpo']=$DATA[$i]->esp_cuerpo; 
      
      if($DATA[$i]->id==0){
        $this->ModeloGeneral->tabla_inserta('espesores_paso5',$data);
      }else{
        $this->ModeloGeneral->updateCatalogo($data,'id',$DATA[$i]->id,'espesores_paso5');
      }
    }
  }

  public function delete_tapa_esp(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'espesores_paso5');
    echo $resul;
  }

  public function get_tabla_tapas()
  {
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $results=$this->ModeloGeneral->getselectwhere_n_consulta($tabla,array('estatus'=>1,'id_operacion'=>$id));
    echo json_encode($results);
  }

  public function cargaimagen(){
    $id_operacion=$_POST['id_operacion'];
    $input_name=$_POST['input_name'];

    if($input_name=="file"){
      $DIR_SUC=FCPATH.'uploads/croquis';
      $col="img_croquis";
    }else if($input_name=="file_croq2"){
      $DIR_SUC=FCPATH.'uploads/croquis';
      $col="img_croquis_mt";
    }else if($input_name=="file_placa"){
      $DIR_SUC=FCPATH.'uploads/placas';
      $col="file_placa";
    }else if($input_name=="files"){
      $DIR_SUC=FCPATH.'uploads/evidencias';
      $col="img";
    }else if($input_name=="img_equipo"){
      $DIR_SUC=FCPATH.'uploads/equipos_cliente';
      $col="img_equipo";
    }
    /*log_message('error', "DIR_SUC: ".$DIR_SUC); 
    log_message('error', "col: ".$col); 
    log_message('error', "input_name: ".$input_name); */

    $config['upload_path']          = $DIR_SUC;
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
    $config['max_size']             = 8000;
    $file_names=date('YmdGis');
    $config['file_name']=$file_names;       
    $output = [];

    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload($input_name)){
      $data = array('error' => $this->upload->display_errors());
      //log_message('error', json_encode($data));                    
    }else{
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension=$upload_data['file_ext'];    // uploded file extension
      if($input_name=="file" || $input_name=="file_placa" || $input_name=="file_croq2" || $input_name=="img_equipo"){
        $this->ModeloGeneral->updateCatalogo_value(array($col=>$file_name),array('id_operacion'=>$id_operacion),'levantamiento_paso5');
      }else{
        $this->ModeloGeneral->tabla_inserta('inspeccion_superficial',array('id_operacion'=>$id_operacion,"img"=>$file_name));
      }
      $data = array('upload_data' => $this->upload->data());
      //$output = [];
      //log_message('error', json_encode($data));
    }
    echo json_encode($output);
  }

  public function deleteFile($id){
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'inspeccion_superficial');
    echo $resul;
  }

  public function deleteImgP5($id,$tipo){
    if($tipo==1){
      $data = array('img_croquis' => "");
    }else if($tipo==2){
      $data = array('file_placa' => "");
    }else if($tipo==3){
      $data = array('img_croquis_mt' => "");
    }else if($tipo==4){
      $data = array('img_equipo' => "");
    }
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'levantamiento_paso5');
    echo $resul;
  }

  public function resumen_crono()
  {
    $id = $this->input->post('id');
    $tipo = $this->input->post('tipo');
    $html="";
    $results=$this->ModeloGeneral->getselectwhere_n_consulta("resumen_cronologico",array('estatus'=>1,'id_operacion'=>$id,"tipo"=>$tipo));
    foreach($results as $r){
      $html.="<tr id='row_for_".$r->id."'>
              <td><input type='hidden' id='idt' value='".$r->id."'><input class='form-control' type='date' id='fechat' value='".$r->fecha."'></td>
              <td><input class='form-control' type='text' id='descript' value='".$r->descrip."'></td>
              <td><input class='form-control' type='text' id='documentot' value='".$r->documento."'></td>
              <td><input class='form-control' type='text' id='realizat' value='".$r->realiza."'></td>
              <td><input class='form-control' type='text' id='responsablet' value='".$r->responsable."'></td>
              <td><a onclick='deleteCrono(1,".$r->id.")' class='btn btn-outline-dark mb-1'><i class='icon-xl far fa-trash-alt'></i></td>
          </tr>";
    }
    echo $html;
  }

  public function delete_crono(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'resumen_cronologico');
    echo $resul;
  }

  public function guarda_resumen(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    for ($i=0;$i<count($DATA);$i++) {   
      $data['id_operacion']=$DATA[$i]->id_operacion;        
      $data['tipo']=$DATA[$i]->tipo;
      $data['fecha']=$DATA[$i]->fecha;
      $data['descrip']=$DATA[$i]->descrip;
      $data['documento']=$DATA[$i]->documento;
      $data['realiza']=$DATA[$i]->realiza;
      $data['responsable']=$DATA[$i]->responsable;
 
      if($DATA[$i]->id==0){
        $this->ModeloGeneral->tabla_inserta('resumen_cronologico',$data);
      }else{
        $this->ModeloGeneral->updateCatalogo($data,'id',$DATA[$i]->id,'resumen_cronologico');
      }
    }
  }

  public function getImages(){
    $id_opera = $this->input->post("id_opera");
    $i=0; 
    $getdat=$this->ModeloGeneral->getselectwhere_n_consulta('inspeccion_superficial',array('estatus'=>1,'id_operacion'=>$id_opera));
    foreach ($getdat as $k) {
      $img=""; $imgdet=""; $imgp="";
      $i++;
      $img = "<img src='".base_url()."uploads/croquis/".$k->img."' width='50px' alt='Sin Dato'>";
      $imgdet = "{type: 'image', url: '".base_url()."Operaciones/deleteFile/".$k->id."', caption: '".$k->img."', key:".$i."}";
      $imgp = "<img src='".base_url()."uploads/evidencias/".$k->img."' class='kv-preview-data file-preview-image' width='80px'>"; 
      $typePDF = "false";  
      echo '
          <div class="row_'.$k->id.'">
            <div class="col-md-5">
                <label class="col-md-12"> Imagen: '.$k->img.'</label>
                <input type="file" name="inputFile" id="inputFile'.$i.'" class="form-control">
                <hr style="background-color: red;">
            </div>
        </div>
          <script> 
          $("#inputFile'.$i.'").fileinput({
              overwriteInitial: false,
              showClose: false,
              showCaption: false,
              showUploadedThumbs: false,
              showBrowse: false,
              removeTitle: "Cancel or reset changes",
              elErrorContainer: "#kv-avatar-errors-1",
              msgErrorClass: "alert alert-block alert-danger",
              defaultPreviewContent: "'.$img.'",
              layoutTemplates: {main2: "{preview} {remove}"},
              allowedFileExtensions: ["jpg", "png", "jpeg","webp"],
              initialPreview: [
              "'.$imgp.'"
              ],
              initialPreviewAsData: '.$typePDF.',
              initialPreviewFileType: "image",
              initialPreviewConfig: [
                  '.$imgdet.'
              ]
          });
          </script>';
    }     
  }

  /* ****************************************************** */
  function memoria($id){
    $data["o"]=$this->ModeloGeneral->getselectwhererow2("operaciones",array('id'=>$id));
    $data["l"]=$this->ModeloGeneral->getselectwhererow2("levantamiento",array('id_operacion'=>$id));
    $data["l2"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso2",array('id_operacion'=>$id));
    $data["tap"]=$this->ModeloGeneral->getselectwhere_n_consulta('tapas_paso2',array('estatus'=>1,'id_operacion'=>$id));
    $data["l3"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso3",array('id_operacion'=>$id));
    $data["l4"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso4",array('id_operacion'=>$id));
    $data["l5"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso5",array('id_operacion'=>$id));
    $data["fecha_inspeccion"]= date('d/m/Y',strtotime($data["l"]->fecha_inspeccion));
    $data["num_expediente"]=$data["o"]->num_expediente;
    $cli=$this->ModeloGeneral->getselectwhererow2("empresa",array('id'=>$data["o"]->id_cliente));
    $data["foto"]=$cli->foto;
    $data["logo_cli"]=$cli->foto;
    $data["nombre"]=$cli->nombre;
    $data["direccion"]=$cli->direccion;
    $data["color"]=$cli->color;
    $data["consultor"]=$data["l"]->consultor;
    if($data["l"]->consultor=="1"){
      $data["foto"]=$data["l"]->logo;
      $data["direccionc"]=$data["l"]->direccion;
      $data["color"]=$data["l"]->color;
    }
    $get_per=$this->ModeloGeneral->getselectwhererow2("personal",array('personalId'=>$data["l"]->id_tecnico));
    $data["tecnico"]=$get_per->nombre;
    $data["cedula"]=$get_per->cedula_prof;
    $this->load->view('reportes/memoria_calculo',$data);
  }

  function expediente($id){
    $data["o"]=$this->ModeloGeneral->getselectwhererow2("operaciones",array('id'=>$id));
    $data["l"]=$this->ModeloGeneral->getselectwhererow2("levantamiento",array('id_operacion'=>$id));
    $data["l2"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso2",array('id_operacion'=>$id));
    
    $data["l3"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso3",array('id_operacion'=>$id));
    $data["l4"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso4",array('id_operacion'=>$id));
    $data["l5"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso5",array('id_operacion'=>$id));
    $data["l6"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso6",array('id_operacion'=>$id));
    $data["fecha_inspeccion"]= date('d/m/Y',strtotime($data["l"]->fecha_inspeccion));
    $data["num_expediente"]=$data["o"]->num_expediente;
    $cli=$this->ModeloGeneral->getselectwhererow2("empresa",array('id'=>$data["o"]->id_cliente));
    $flu=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$data["l"]->id_fluido));
    $data["foto"]=$cli->foto;
    $data["logo_cli"]=$cli->foto;
    $data["nombre"]=$cli->nombre;
    $data["direccion"]=$cli->direccion;
    $data["color"]=$cli->color;
    $data["consultor"]=$data["l"]->consultor;
    if($data["l"]->consultor=="1"){
      $data["foto"]=$data["l"]->logo;
      $data["direccionc"]=$data["l"]->direccion;
      $data["color"]=$data["l"]->color;
    }
    
    $data["resu"]=$this->ModeloGeneral->getselectwhere_n_consulta('resumen_cronologico',array('estatus'=>1,'id_operacion'=>$id));
    $this->load->view('reportes/expediente',$data);
  }

  function inspeccion($id){
    $data["o"]=$this->ModeloGeneral->getselectwhererow2("operaciones",array('id'=>$id));
    $data["l"]=$this->ModeloGeneral->getselectwhererow2("levantamiento",array('id_operacion'=>$id));
    $data["l2"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso2",array('id_operacion'=>$id));
    $data["tap"]=$this->ModeloGeneral->getselectwhere_n_consulta('tapas_paso2',array('estatus'=>1,'id_operacion'=>$id));
    $data["l3"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso3",array('id_operacion'=>$id));
    $data["l4"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso4",array('id_operacion'=>$id));
    $data["l5"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso5",array('id_operacion'=>$id));
    $data["l6"]=$this->ModeloGeneral->getselectwhererow2("levantamiento_paso6",array('id_operacion'=>$id));
    $data["fecha_inspeccion"]= date('d/m/Y',strtotime($data["l"]->fecha_inspeccion));
    $data["num_expediente"]=$data["o"]->num_expediente;
    $cli=$this->ModeloGeneral->getselectwhererow2("empresa",array('id'=>$data["o"]->id_cliente));
    $flu=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$data["l"]->id_fluido));
    $data["foto"]=$cli->foto;
    $data["logo_cli"]=$cli->foto;
    $data["nombre"]=$cli->nombre;
    $data["direccion"]=$cli->direccion;
    $data["color"]=$cli->color;
    $data["consultor"]=$data["l"]->consultor;
    if($data["l"]->consultor=="1"){
      $data["foto"]=$data["l"]->logo;
      $data["direccionc"]=$data["l"]->direccion;
      $data["color"]=$data["l"]->color;
    }
    $get_per=$this->ModeloGeneral->getselectwhererow2("personal",array('personalId'=>$data["l"]->id_tecnico));
    $data["tecnico"]=$get_per->nombre;
    $data["cedula"]=$get_per->cedula_prof;

    $data["get_esp1"]=$this->ModelOperaciones->getMaxEspesor("esp_tapa1",$id);
    $data["get_esp2"]=$this->ModelOperaciones->getMaxEspesor("esp_tapa2",$id);
    $data["get_cpo"]=$this->ModelOperaciones->getMaxEspesor("esp_cuerpo",$id);

    $data["get_cont1"]=$this->ModeloGeneral->getselectwhere_n_consulta2("espesores_paso5",array('id_operacion'=>$id,"esp_tapa1 !="=>"","estatus"=>1));
    $data["get_cont2"]=$this->ModeloGeneral->getselectwhere_n_consulta2("espesores_paso5",array('id_operacion'=>$id,"esp_tapa2 !="=>"","estatus"=>1));
    $data["get_cont3"]=$this->ModeloGeneral->getselectwhere_n_consulta2("espesores_paso5",array('id_operacion'=>$id,"esp_cuerpo !="=>"","estatus"=>1));
    $data["eb"]=$this->ModeloGeneral->getselectwhererow2("equipo_bloque",array('id'=>$data["l6"]->id_equipo_bloque));
    $data["espes"]=$this->ModeloGeneral->getselectwhere_n_consulta("espesores_paso5",array('id_operacion'=>$id,"estatus"=>1));
    $data["evide"]=$this->ModeloGeneral->getselectwhere_n_consulta("inspeccion_superficial",array('id_operacion'=>$id,"estatus"=>1));
    $i=0;
    foreach($data["evide"] as $e){
      ${'th_imgs_'.$i}= '<th height="170px"><img height="240px" width="220px" src="'.FCPATH.'uploads/evidencias/'.$e->img.'"></th>';
      $i++;
    }
    $html=""; $b=0; 
    for($c=0; $c<=count($data["evide"]); $c++){
      //if($b%2==0) { $html.= "<tr>"; }
      if($b<count($data["evide"])){
        $html.= "<tr>";
        $html.=${'th_imgs_'.$b}; 
      }
      $b2=$b++;
      if($b2<count($data["evide"])){
        $html.=${'th_imgs_'.$b};
        if(${'th_imgs_'.$b}=="" || !isset(${'th_imgs_'.$b})){
          $html.="<th></th>";  
        }
        $html.= "</tr>";
      }
      //if($c%2==0 && $c<count($data["evide"]) || $c==count($data["evide"])) { $html.="</tr>";}
      $b++;
      $html = str_replace("<tr><th></th></tr>", "", $html);
    }
    if($i==0){
      $html.='<tr><th colspan="2" height="700px"></th></tr>';
    }
    //log_message('error', "html: ".$html); 
    $data["html_tab"]=$html;
    $this->load->view('reportes/ensayos_no_destruc',$data);
  }

  public function tablaFotos($id){
    $data["evide"]=$this->ModeloGeneral->getselectwhere_n_consulta("inspeccion_superficial",array('id_operacion'=>$id,"estatus"=>1));
    $this->load->view('reportes/tabla_fotos',$data);
  }

  function cargafiles(){
    $id=$this->input->post('id');
    $upload_folder ='uploads/consultor';
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $nombre_archivo = $this->eliminar_acentos($nombre_archivo);
    $newfile=$fecha.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      log_message('error', "ocurrió un error al subir el archivo. No pudo guardarse");
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('logo'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'levantamiento');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  /* ***************************************************/

  public function eliminar_acentos($cadena){
    //Reemplazamos la A y a
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ç', 'ç'),
    array('C', 'c'),
    $cadena
    );

    return $cadena;
  }

}
