<div class="row">
    <div class="col-12">
        <div class="content-header">Listado de Clientes</div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body table-responsive">
                    <div class="row">
                        <div class="col-3">
                            <a href="<?php echo base_url(); ?>Empresa/registrar/" class="btn btn-sistema mr-sm-2 mb-3"><i class="fa fa-users"></i> &nbsp; Nuevo Cliente</a>
                        </div>
                        <div class="col-6">
                            <select id="tipo_registro" class="form-control" onchange="reload_registro()">
                                <option value="1">Activos</option>
                                <option value="0">Eliminados</option>
                            </select>
                        </div>
                    </div>
                    <table class="table text-center m-0" id="table_data">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Logo</th>
                                <th>Cliente</th>
                                <th>Razón social</th>
                                <th>Tipo empresa</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>