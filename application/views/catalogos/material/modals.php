<div class="modal fade text-left" id="modalcarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Carga Vuelos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url();?>file_materiales/malla.xlsx" class="btn btn-social-icon btn-outline-linkedin mr-2" title="Plantilla de ejemplo"><i class="fa fa-file-excel-o"></i> </a>
                    </div>
                    <div class="col-md-12">
                        <input type="file" name="documento" id="documento" >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>