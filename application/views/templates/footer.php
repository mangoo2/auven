                </div>
            </div>
   <!-- BEGIN : Footer-->
            <footer class="footer undefined undefined">
                <p class="clearfix text-muted m-0"><span>Copyright &copy; <?php echo date('Y');?> &nbsp;</span><a href="http://www.mangoo.mx" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2" style="color: orange !important;"> <img style="width: 110px;" src="<?php echo base_url() ?>public/img/mangoo.png"> </a><span class="d-none d-sm-inline-block">, All rights reserved.</span></p>
            </footer>
            <!-- End : Footer-->
            <!-- Scroll to top button -->
            <button class="btn btn-primary scroll-top" type="button"><i class="ft-arrow-up"></i></button>

        </div>
    </div>
    <!-- END Notification Sidebar-->
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/switchery.min.js"></script>
    <script src="<?php echo base_url();?>plugins/scripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/alert/sweetalert.js"></script>
    <script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/customizer.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/scroll-top.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>public/datatables/datatables.bundle.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/jquery.steps.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>public/fileinput/fileinput.js?v=<?php echo date("Ymd Gis"); ?>" type="text/javascript"></script>

    <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#6993FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <!-- BEGIN: Custom CSS-->
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    <script type="text/javascript">
        var cont = 0;
        $('#sidebarToggle').click(function(event) {
            if(cont==0){
                cont=1;
                $('.img_logo').html('<img style="width: 35px;" src="<?php echo base_url(); ?>public/img/FAV.png" alt="Logo" />');
                $('.logo_tam').css('height','64px');
            }else{
                cont=0;
                $('.img_logo').html('<img src="<?php echo base_url(); ?>public/img/logo_inicio.png" alt="Logo" />');
                $('.logo_tam').css('height','114px');
            }
        });
    </script>
    <!-- END: Custom CSS-->
</body>
<!-- END : Body-->

</html>