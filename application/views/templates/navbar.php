<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
if (!isset($_SESSION['perfilid_tz'])) {
$perfil=0;
}else{
$perfil=$_SESSION['perfilid_tz'];
}
if(!isset($_SESSION['perfilid_tz'])){
?>
<script>
document.location="<?php echo base_url(); ?>index.php/Login"
</script>
<?php
}
$menu=$this->ModeloSession->menus($perfil);
$req=$this->ModeloSession->equiposvencidos();
?>
<nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-fixed">
    <div class="container-fluid navbar-wrapper">
        <div class="navbar-header d-flex">
            <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
            <ul class="navbar-nav">
                <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i class="ft-maximize font-medium-3"></i></a></li>
            </ul>
        </div>
        <div class="navbar-container">
            <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="dropdown nav-item"><a class="nav-link dropdown-toggle dropdown-notification p-0 mt-2" id="dropdownBasic1" href="javascript:;" data-toggle="dropdown" aria-expanded="false"><i class="ft-bell font-medium-3"></i><span class="notification badge badge-pill <?php if($req->num_rows()>0){ echo 'badge-danger';}else{ echo 'badge-grey';}?> "><?php echo $req->num_rows();?></span></a>
                    <ul class="notification-dropdown dropdown-menu dropdown-menu-media dropdown-menu-right m-0 overflow-hidden">
                        <li class="dropdown-menu-header">
                            <div class="dropdown-header d-flex justify-content-between m-0 px-3 py-2 white bg-primary">
                                <div class="d-flex"><i class="ft-bell font-medium-3 d-flex align-items-center mr-2"></i><span class="noti-title"><?php echo $req->num_rows();?> New Notification</span></div>
                            </div>
                        </li>
                        <li class="scrollable-container ps">
                            <?php foreach ($req->result() as $iteme) { ?>
                                <div class="media d-flex align-items-center">
                                    <div class="media-left">
                                        <div class="mr-3">
                                            <div class="avatar-content font-medium-2"><i class="ft-align-left text-info"></i></div>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="m-0"><span><?php if($iteme->tipo==1){ echo 'Equipo MT';} if($iteme->tipo==2){ echo 'Equipo UT';}?></span><small class="grey lighten-1 font-italic float-right"><?php echo $this->ModeloSession->diferenciafecha($iteme->vigencia_cal)?></small></h6>
                                        <small class="noti-text"><?php echo 'Marca: '.$iteme->marca.' Modelo: '.$iteme->modelo.' Serie: '.$iteme->serie;?></small>
                                    </div>
                                </div>
                            <?php } ?>
                            
                            
                            
                            
                        </ul>
                    </li>
                    
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>index.php/Login/exitlogin" class="btn btn-sistema mr-sm-2 mb-1">Cerrar sesión</a>
                    </li>
                    
                    <li class="dropdown nav-item mr-1"><a class="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                        <div class="user d-md-flex d-none mr-2"><span class="text-right"><?php echo $_SESSION['usuario_tz'];?></span><span class="text-right text-muted font-small-3"><?php echo $_SESSION['perfil_nombre'];?></span></div><img class="avatar" src="<?php echo base_url(); ?>public/img/FAV.png" alt="avatar" height="35" width="35">
                    </a>
                    <div class="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2">
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url(); ?>index.php/Login/exitlogin">
                        <div class="d-flex align-items-center"><i class="ft-power mr-2"></i><span>Cerrar</span></div>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>
</div>
</nav>
<div class="wrapper">
<!-- main menu-->
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div class="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="<?php echo base_url(); ?>public/img/fondo_menu.jpg" data-scroll-to-active="true">
<!-- main menu header-->
<!-- Sidebar Header starts-->
<div class="sidebar-header">
    <div class="logo clearfix logo_tam" style="background: white; height: 114px;"><a class="logo-text float-left" href="<?php echo base_url()?>Inicio">
        <div class="logo-img"><span class="img_logo"><img src="<?php echo base_url(); ?>public/img/logo_inicio.png" alt="Logo" /></span></div>
        </a><a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;"><i class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i class="ft-x"></i></a></div>
    </div>
    <!-- Sidebar Header Ends-->
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="sidebar-content main-menu-content">
        <div class="nav-container">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <?php foreach ($menu->result() as $item){ ?>
                <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
                <ul class="menu-content">
                    <?php
                    $menu =  $item->MenuId;
                    $menusub = $this->ModeloSession->submenus($perfil,$menu,1);
                    foreach ($menusub->result() as $datos) { ?>
                    <li>
                        <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                            <i class="<?php echo $datos->Icon; ?>"></i>
                            <?php echo $datos->Nombre; ?>
                        </a>
                    </li>
                    <?php } ?>
                    
                </ul>
            </li>
            <?php } ?>
            <?php
            $menusub = $this->ModeloSession->submenus($perfil,0,0);
            foreach ($menusub->result() as $datos) { ?>
            <li class="nav-item">
                <a href="<?php echo base_url(); echo $datos->Pagina; ?>">
                    <i class="<?php echo $datos->Icon; ?>"></i>
                    <span data-i18n="" class="menu-title"><?php echo $datos->Nombre; ?></span>
                </a>
            </li>
            <?php }
            ?>
        </ul>
    </div>
</div>
<!-- main menu content-->
<div class="sidebar-background"></div>
<!-- main menu footer-->
<!-- include includes/menu-footer-->
<!-- main menu footer-->
<!-- / main menu-->
</div>
<div class="main-panel">
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">