var base_url = $('#base_url').val();
var tabla;
var cont_res=0;
$(document).ready(function() {
    loadtable();

    $("#add_resumen").on("click",function(){
        if($("#fecha").val()!="" && $("#descrip").val()!=""){
            html="<tr id='row_"+cont_res+"'>\
                  <td><input type='hidden' id='idt' value='0'><input type='hidden' id='fechat' value='"+$("#fecha").val()+"'> "+$('#fecha').val()+"</td>\
                  <td><input type='hidden' id='descript' value='"+$("#descrip").val()+"'> "+$('#descrip').val()+"</td>\
                  <td><input type='hidden' id='documentot' value='"+$("#documento").val()+"'> "+$('#documento').val()+"</td>\
                  <td><input type='hidden' id='realizat' value='"+$("#realiza").val()+"'> "+$('#realiza').val()+"</td>\
                  <td><input type='hidden' id='responsablet' value='"+$("#responsable").val()+"'> "+$('#responsable').val()+"</td>\
                  <td><a onclick='deleteCrono(0,"+cont_res+")' class='btn btn-outline-dark mb-1'><i class='icon-xl far fa-trash-alt'></i></td>\
              </tr>";
            $('#body_resumen').append(html);
        }else{
            swal("Alerta", "Ingresa una fecha y una descripción", "warning");   
        }
        cont_res++;
    });

    $("#tipo_resumen").on("change",function(){
        getTableResumen($("#id_opera").val());
    });

    $("#save_re").on("click",function(){
        saveResumen();
    });
});

function saveResumen(){
    var DATA  = [];
    var TABLA   = $("#table_resumen tbody > tr");
    TABLA.each(function(){ 
        item = {};
        item ["id"] = $(this).find("input[id*='idt']").val();
        item ["id_operacion"] = $("#id_opera").val();
        item ["tipo"] = $("select[id*='tipo_resumen'] option:selected").val();
        item ["fecha"] = $(this).find("input[id*='fechat']").val();
        item ["descrip"] = $(this).find("input[id*='descript']").val();
        item ["documento"] = $(this).find("input[id*='documentot']").val();
        item ["realiza"] = $(this).find("input[id*='realizat']").val();
        item ["responsable"] = $(this).find("input[id*='responsablet']").val();
        if(item ["fecha"]!=undefined){
            DATA.push(item);
        }
        //console.log("fechat: "+$(this).find("input[id*='fechat']").val());
    });     
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    //console.log("aInfo: "+aInfo);
    $.ajax({
        data: INFO, 
        type: 'POST',
        url : base_url + 'Operaciones/guarda_resumen',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            $("#modal_resumen").modal("hide");
            swal("Éxito", "Guardado correctamente", "success");  
            $("#fecha").val("");
            $("#descrip").val("");
            $("#documento").val("");
            $("#realiza").val("");
            $("#responsable").val("");
        }
    });
}

function deleteCrono(aux,id) {
    if(aux==0){
        $('#row_'+id).remove();
        cont_res--;
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar el registro?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Operaciones/delete_crono",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            $('#row_for_'+id).remove();
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        destry:true,
        "ajax": {
            "url": base_url+"Operaciones/getlistado",
            type: "post",
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"alias"},
            {"data":"num_expediente"},
            {"data":"fecha"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="btn-group">\
                    <a title="Levantamiento de datos de equipo" href="'+base_url+'Operaciones/levantamiento/'+row.id+'" class="btn btn-outline-dark mb-1"><i class="icon-xl fa fa-database"></i></a>\
                    <a target="_blank" title="Generar Memoria de Cálculo" href="'+base_url+'Operaciones/memoria/'+row.id+'" class="btn btn-danger mb-1"><i class="btn-danger icon-xl fa fa-file-pdf-o"></i></a>\
                    <button onclick="resumen('+row.id+')" title="Resumen Cronológico" class="btn btn-outline-dark mb-1"><i class="icon-xl fa fa-tasks"></i></button>\
                    <a target="_blank" title="Generar Expediente" href="'+base_url+'Operaciones/expediente/'+row.id+'" class="btn btn-danger mb-1"><i class="btn-danger icon-xl fa fa-file-pdf-o"></i></a>\
                    <a target="_blank" title="Generar Inspección por Ensayos" href="'+base_url+'Operaciones/inspeccion/'+row.id+'" class="btn btn-danger mb-1"><i class="btn-danger icon-xl fa fa-file-pdf-o"></i></a>\
                    <a href="'+base_url+'Operaciones/nueva/'+row.id+'" class="btn btn-outline-dark mb-1"><i class="icon-xl far fa-edit"></i></a>\
                    <a onclick="delete_data('+row.id+')" class="btn btn-outline-dark mb-1"><i class="icon-xl far fa-trash-alt"></i></a>';
                html+='</div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
    });
}

function resumen(id){
    $("#modal_resumen").modal();
    $("#id_opera").val(id);
    getTableResumen(id);
}

function getTableResumen(id){
    $("#table_resumen").DataTable({destroy:true});
    $.ajax({
        type:'POST',
        url: base_url+"Operaciones/resumen_crono",
        data: {
            id:id, tipo:$("#tipo_resumen option:selected").val()
        },
        success:function(response){ 
            //console.log(response);
            $("#body_resumen").html(response);
        }
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta operación?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Operaciones/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}