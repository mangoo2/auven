<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getUsuarios_user($id){
        $sql = "SELECT u.UsuarioID, u.Usuario,pe.personalId, pe.nombre, p.perfilId, p.nombre AS perfil, u.contrasena FROM usuarios AS u
                INNER JOIN perfiles AS p ON p.perfilId = u.perfilId
                INNER JOIN personal AS pe ON pe.personalId = u.personalId
                WHERE u.personalId = $id";
        $query = $this->db->query($sql);
        return $query->result();
    } 

    function get_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.celular',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre',
        );
        $columnsx = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.celular',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre as perfil',
            6=>'p.foto'
        );
        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsx[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.celular',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId');
        $this->db->join('perfiles per','per.perfilId=u.perfilId');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}