<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    } 

    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhereN($tables,$select,$cols,$values){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se ocupa para n select
        $this->db->update($catalogo);
        return $id;
    }

    function updateCatalogo_value($data,$info,$catalogo){
        $this->db->set($data);
        $this->db->where($info);/// Se ocupa para n select
        $this->db->update($catalogo);
    }

    public function getselectwhere_n_consulta($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhere_n_consulta2($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query;
    }

    public function getselect($tables){
        $this->db->select("*");
        $this->db->from($tables);
        //$this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function getData(){
        $datos = $this->db->get('tabla');
        return $datos->result();
    } 

    public function update_foto($data,$id,$tabla) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update($tabla); 
    }

    public function getselectwhererow($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->row();
    }

    public function getselectwhererow2($tables,$value){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($value);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->row();
    }

    public function getselect_like($tables,$title,$value,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($title,$value);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhere_tipo($select,$tables,$cols,$values){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

}