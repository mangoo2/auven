<div class="row">
	<div class="col-12">
		<div class="content-header">Operación</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body table-responsive">
					<div class="icons-tab-steps" id="icons-tab-steps">
                        <h6>Paso 1</h6>
                        <fieldset>
							<form method="post" id="form_p1">
								<input type="hidden" name="id" id="id_reg" value="<?php if(isset($r)) echo $r->id; else echo "0"; ?>">
								<div class="row">	
									<div class="col-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>Cliente</label>
										</div>
										<div class="col-9">
											<select id="id_cliente" name="id_cliente" class="form-control">
												<?php if(isset($r)) echo "<option value='".$r->id_cliente."'>".$r->alias."</option>"; ?>
											</select>
										</div>
									</div>
									<div class="col-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>Dirección</label>
										</div>
										<div class="col-9">
											<textarea rows="4" class="form-control" type="text" id="direccion" name="direccion"><?php if(isset($r)) echo $r->direccion; ?></textarea>
										</div>
									</div>
									<div class="col-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>No. Expediente</label>
										</div>
										<div class="col-9">
											<input class="form-control" type="text" name="num_expediente" value="<?php if(isset($r)) echo $r->num_expediente ?>">
										</div>
									</div>
									<div class="col-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>Fecha</label>
										</div>
										<div class="col-9">
											<input class="form-control" type="date" name="fecha" value="<?php if(isset($r)) echo $r->fecha ?>">
										</div>
									</div>
								</div>
								<!--<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p1" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso</button>
                                    </div>
                                </div>-->
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>
					</div>
					<hr>
					<div class="row">
                        <div class="col-lg-12 equipo_btn_text">
                            
                            <a href="<?php echo base_url()?>Operaciones/recipientes" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
				</div>
			</div>
		</div>
	</div>
</div>