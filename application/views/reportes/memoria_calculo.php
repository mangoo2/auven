<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');

    $GLOBALS["fecha_inspeccion"]=$fecha_inspeccion;
    $GLOBALS["num_expediente"]=$num_expediente;
    $GLOBALS["foto"]=$foto;
    $GLOBALS["logo_cli"]=$logo_cli;
    $GLOBALS["nombre"]=$nombre;
    $GLOBALS["direccion"]=$direccion;
    $GLOBALS["color"]=$color;
    $GLOBALS["consultor"]=$consultor;
    if($consultor==1){
        $GLOBALS["direccionc"]=$direccionc;
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        //log_message('error', "consultor: ".$GLOBALS["consultor"]);  
        if($GLOBALS["consultor"]==0){ //cliente directo --trae su logo que se carga en formulario clientes
            $logo_head = FCPATH.'uploads/empresa/'.$GLOBALS["foto"];
            $GLOBALS["logo"]=$logo_head;
        }else{ //consultor
            $logo_headcon = FCPATH.'uploads/consultor/'.$GLOBALS["foto"];
            $logo_head = FCPATH.'uploads/empresa/'.$GLOBALS["logo_cli"];
            $GLOBALS["logo"]=$logo_headcon;
        }
        
        $html = '<style>
            .ft9{ font-size:10px; text-align:center; align-items:center; font-weight:bold; }
            .backg{background-color:rgb(217,217,217);}
            .pspaces{ font-size:1.8px;}
            .just{ text-align:justify; }
        </style>';
        $html.='
                <table border="1" cellpadding="2" align="center" class="ft9">
                    <tr>
                        <td height="95px" style="align-items: center;" rowspan="6" width="25%"><p class="pspaces"></p><img src="'.$logo_head.'"></td>
                        <td rowspan="6" style="font-size:22px" width="50%"><p class="pspaces"></p>EXPEDIENTE</td>
                        <td width="25%"></td>
                    </tr>               
                    <tr class="backg">
                        <td>No. EXPEDIENTE</td>
                    </tr>
                    <tr>
                        <td>'.$GLOBALS["num_expediente"].' MEM</td>
                    </tr>
                    <tr class="backg">
                        <td>FECHA</td>
                    </tr>
                    <tr>
                        <!--<td>'.$GLOBALS["fecha_inspeccion"].'</td>-->
                        <td>'.date("d-m-Y").'</td>
                    </tr>
                    <tr>
                        <td> Página: '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                    </tr>
                </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        /*if($GLOBALS["consultor"]==0){ //cliente directo
            //$dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            //$color=$GLOBALS['color'];
            $dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            $color="green";
        }else{
            //$dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            //$color="green";
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            $color=$GLOBALS['color'];
        }*/
        if($GLOBALS["consultor"]==0){ //cliente directo
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
        }else{
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccionc"];
        }
        //$dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
        $color=$GLOBALS['color'];
        //log_message('error', "color: ".$color);  
      $html = '
        <style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center}
        </style> 
      <table class="table" width="100%" cellpadding="2">
        <tr><td colspan="2" class="back_foot"></td></tr>
        <tr><td colspan="2" class="footerpage" >'.$dir_foot.'</td></tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
      //$this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Memoria');
$pdf->SetTitle('Memoria de cálculo');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-020-STPS-2011');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(20,37,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(20);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);

// add a page
$pdf->AddPage('P', 'A4'); 

$htmlg='<style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center; font-size:14px;}
            .tab_info{ font-weight:bold;}
        </style> 
        <p></p>
        <table align="center" width="100%" cellpadding="2">
            <tr><td height="220px"><img width="220px" src="'.$GLOBALS["logo"].'"></td></tr>
        </table>
        <p></p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>INFORME DE RESULTADOS</td></tr>
            <tr><td>MEMORIA DE CÁLCULO</td></tr>
            <tr><td>NOM - 020 - STPS - 2011</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EMPRESA</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$GLOBALS["nombre"].'</td></tr>
            <tr><td>'.$GLOBALS["direccion"].'</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EQUIPO</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$l->nombre_equipo.'</td></tr>
            <tr><td>IDENTIFICACIÓN: '.$l->num_identifica.'</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 

if($l->estado_uso=="2") $estado_uso="USADO"; else $estado_uso="NUEVO";
$getF=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$l->id_fluido));
$fluido=$getF->nombre;
$getE=$this->ModeloGeneral->getselectwhererow2("equipo",array('id'=>$l2->id_equipo));
$equipo=$getE->marca." ".$getE->modelo;

$getM=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$l3->id_material));
$mat_cpo=$getM->spec_no;
if($l->codigo_norma=="1"){
    $codigo_norma="CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1";
}else{
    $codigo_norma="CÓDIGO ASME SECCIÓN I";
}

if($l4->tipo_medidor=="1") //Manómetro caratula
    $rango=$l4->rango." KG/CM2";
else if($l4->tipo_medidor=="2") //digital
    $rango="Digital";
else if($l4->tipo_medidor=="3") //Sin medidor
    $rango="Sin medidor";

$htmlg='<style type="text/css">
            .table{text-align:left; font-size:11px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}

        </style> 
        <p class="tab_info">1.- DESCRIPCIÓN Y DATOS DEL EQUIPO</p>
        <table class="table" border="1" rules="none" align="left" width="100%" cellpadding="6">
            <tr>
                <th>Nombre del Recipiente:</th>
                <td>'.$l->nombre_equipo.'</td>
            </tr>
            <tr>
                <th>Número de identificación:</th>
                <td>'.$l->num_identifica.'</td>
            </tr>
             <tr>
                <th>Usuario:</th>
                <td>'.$GLOBALS["nombre"].'</td>
            </tr>
            <tr>
                <th>Ubicación del equipo:</th>
                <td>'.$l->ubicacion.'</td>
            </tr>
            <tr>
                <th>Presión de Diseño:</th>
                <td>'.$l3->presion_diseno.' kg/cm² &nbsp; &nbsp; &nbsp; '.round(($l3->presion_diseno*98.0665),2).' kPa</td>
            </tr>
            <tr>
                <th>Presión de Operación:</th>
                <td>'.$l3->presion.' kg/cm² &nbsp; &nbsp; &nbsp; '.round(($l3->presion*98.0665),2).' kPa</td>
            </tr>
            <tr>
                <th>Presión Regulada del Dispositivo de Seguridad:</th>
                <td>'.$l4->presion_regulada.' kg/cm² &nbsp; &nbsp; &nbsp; '.round(($l4->presion_regulada*98.0665),2).' kPa</td>
            </tr>
            <tr>
                <th>Presión de Prueba Hidrostática:</th>
                <td>'.$l3->presion_hidro.'</td>
            </tr>

            <tr>
                <th>Presión Interior Máxima de Trabajo:</th>
                <td>'.$l3->presion_diseno.'kg/cm² &nbsp; &nbsp; &nbsp; '.round(($l3->presion_diseno*98.0665),2).' kPa</td>
            </tr>
             <tr>
                <th>Temperatura de Diseño:</th>
                <td>'.$l3->temp_diseno.' °C &nbsp; &nbsp; &nbsp; '.($l3->temp_diseno+273.15).' K</td>
            </tr>
            <tr>
                <th>Temperatura de Operación:</th>
                <td>'.$l3->temperatura_op.' °C &nbsp; &nbsp; &nbsp; '.($l3->temperatura_op+273.15).' K</td>
            </tr>
            <tr>
                <th>Diámetro Exterior del Envolvente:</th>
                <td>'.$l2->d_ext.' mm</td>
            </tr>
            <tr>
                <th>Longitud Parte Recta:</th>
                <td>'.$l2->cuerpo.' mm</td>
            </tr>
            <tr>
                <th>Capacidad Volumétrica:</th>
                <td>'.($l2->capacidad/1000).' m³ &nbsp; &nbsp; &nbsp; '.$l2->capacidad.' L</td>
            </tr>
            <tr>
                <th>Fabricante:</th>
                <td>'.$l->fabricante.'</td>
            </tr>
            <tr>
                <th>Número de Serie:</th>
                <td>'.$l->num_serie.'</td>
            </tr>
            <tr>
                <th>Año de Fabricación:</th>
                <td>'.$l->anio_fabrica.'</td>
            </tr>
            <tr>
                <th>Lugar de Origen:</th>
                <td>'.$l->origen.'</td>
            </tr>
            <tr>
                <th>Estado de Uso:</th>
                <td>'.$estado_uso.'</td>
            </tr>
            <tr>
                <th>Código o Norma de Cálculo:</th>
                <td>'.$codigo_norma.'</td>
            </tr>
            <tr>
                <th>Fluido:</th>
                <td>'.$fluido.'</td>
            </tr>
            <tr>
                <th>Instrumento de Medición:</th>
                <td>'.$rango.'</td>
            </tr>
            <tr>
                <th>Material del Envolvente:</th>
                <td>'.$mat_cpo.'</td>
            </tr> ';
            $i=1; $arr_l=array(); $arr_fr=array(); $arr_d_int=array(); $arr_mat=array(); $arr_esp=array(); $ide_tap="";
            foreach ($tap as $t) {
                $i++;
                $faldar=$t->falda_recta;
                if($faldar==0){
                    $faldar="-";
                }
                array_push($arr_l,$t->longitud);
                array_push($arr_fr,$faldar);
                array_push($arr_esp,$t->espesor);
                array_push($arr_d_int,$t->d_int);
                $getMat=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$t->id_material));
                array_push($arr_mat,$getMat->spec_no);
                /*if($i==2){
                    if($l->orientacion=="2") { $ide_tap="Izquierda"; } 
                    else { $ide_tap="Superior";  }
                }if($i==3){
                    if($l->orientacion=="2") { $ide_tap="Derecha"; } 
                    else { $ide_tap="Inferior"; }
                }*/
                if($i==2){
                    if($l->orientacion=="2") { $ide_tap="1"; } 
                    else { $ide_tap="1";  }
                }if($i==3){
                    if($l->orientacion=="2") { $ide_tap="2"; } 
                    else { $ide_tap="2"; }
                }
                $htmlg.="<tr>
                    <th>Material de tapa ".$ide_tap.":</th>
                    <td>".$getMat->spec_no."</td>
                </tr>";
            }  

        $htmlg.='</table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
// 1=vertical |
// 2=horizontal -
/*if($l->orientacion=="2") { $name_tap="Izquierda"; $name_tap2="Derecha"; } 
else { $name_tap="Superior"; $name_tap2="Inferior"; }*/

if($l->orientacion=="2") { $name_tap="1"; $name_tap2="2"; } 
else { $name_tap="1"; $name_tap2="2"; }

if($tap[0]->tipo_tapa==1){ $name_tapa="ELIPSOIDAL 2:1"; }
if($tap[0]->tipo_tapa==2){ $name_tapa="HEMISFERICA"; }
if($tap[0]->tipo_tapa==3){ $name_tapa="TORIESFÉRICA 100/06"; }
if($tap[0]->tipo_tapa==4){ $name_tapa="TORIESFÉRICA"; }
if($tap[0]->tipo_tapa==5){ $name_tapa="PLANA"; }
if($tap[0]->tipo_tapa==6){ $name_tapa="PLANA CIRCULAR APERNADA"; }
if($tap[0]->tipo_tapa==7){ $name_tapa="ABOMBADA"; }
if($tap[0]->tipo_tapa==8){ $name_tapa="CONICA"; }
if($tap[0]->tipo_tapa==9){ $name_tapa="TORICONICA"; }

if($tap[1]->tipo_tapa==1){ $name_tapa2="ELIPSOIDAL 2:1"; }
if($tap[1]->tipo_tapa==2){ $name_tapa2="HEMISFERICA"; }
if($tap[1]->tipo_tapa==3){ $name_tapa2="TORIESFÉRICA 100/06"; }
if($tap[1]->tipo_tapa==4){ $name_tapa2="TORIESFÉRICA"; }
if($tap[1]->tipo_tapa==5){ $name_tapa2="PLANA"; }
if($tap[1]->tipo_tapa==6){ $name_tapa2="PLANA CIRCULAR APERNADA"; }
if($tap[1]->tipo_tapa==7){ $name_tapa2="ABOMBADA"; }
if($tap[1]->tipo_tapa==8){ $name_tapa2="CONICA"; }
if($tap[1]->tipo_tapa==9){ $name_tapa2="TORICONICA"; }

$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.4px;}
        </style> 
        <p class="tab_info">2.- VARIABLES</p>
        <table width="100%">
            <tr>
                <th width="50%">
                    <table class="table" border="1" align="center" width="100%" cellpadding="6">
                        <thead><tr class="backg"><td colspan="4">DIMENSIONES:</td></tr></thead>
                        <tr>
                            <th width="45%">Diámetro exterior</th>
                            <td width="27%">'.$l2->d_ext.'</td>
                            <td width="15%">(Do)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%"><p class="pspaces"></p>Diámetro interior</th>';
                            $d_inttap=""; $val_d_int=0;
                            for($c=0; $c<count($arr_d_int); $c++){
                                $d_inttap.=$arr_d_int[$c].", ";
                            }
                            $htmlg.='<td width="27%">'.$l2->d_int.', '.$d_inttap.'</td>
                            <td width="15%"><p class="pspaces"></p>(Dint)</td>
                            <td width="13%"><p class="pspaces"></p>(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Espesor de Diseño</th>
                            <td width="27%">'.$l3->espesor_diseno.'</td>
                            <td width="15%">(td)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Espesor Act. Tapa '.$name_tap.'</th>
                            <td width="27%">'.$arr_esp[0].'</td>
                            <td width="15%">(ts)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Espesor Act. Tapa '.$name_tap2.'</th>
                            <td width="27%">'.$arr_esp[1].'</td>
                            <td width="15%">(Corr)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Espesor Act. Cilíndrico</th>
                            <td width="27%">'.$l2->espesor_act.'</td>
                            <td width="15%">(Fr)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Radio Interior</th>
                            <td width="27%">'.($tap[0]->d_int/2).', '.($tap[0]->d_int/2).', '.($l2->d_int/2).'</td>
                            <td width="15%">(Rint)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Falda Recta Tapas</th>';
                            $fal_tap="";
                            for($c=0; $c<count($arr_fr); $c++){
                                if($arr_fr[$c]!="-")
                                    $fal_tap.=$arr_fr[$c].", ";
                                else
                                    $fal_tap.="- ";
                            }
                            $htmlg.='<td width="27%">'.$fal_tap.'</td>
                            <td width="15%">(td)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Falda Recta Cuerpo</th>
                            <td width="27%">'.$l2->cuerpo.'</td>
                            <td width="15%">(ts)</td>
                            <td width="13%">(mm)</td>
                        </tr>
                        <tr>
                            <th width="45%">Factor C</th>
                            <td width="27%">'.$l2->factorc.'</td>
                            <td width="15%"></td>
                            <td width="13%">(mm)</td>
                        </tr>
                    </table>
                </th>
                <th width="50%">
                    <table class="table" border="1" align="center" width="100%" cellpadding="6">
                        <thead><tr class="backg"><td colspan="4">MATERIAL Y CONDICIONES DE DISEÑO:</td></tr></thead>
                        <tr>
                            <td width="48%">Material Tapas</td>';
                            $mat_tap="";
                            for($c=0; $c<count($arr_mat); $c++){
                                $mat_tap.=$arr_mat[$c].", ";
                            }
                            $htmlg.='<td width="20%">'.$mat_tap.'</td>
                            <td width="15%">Mat</td>
                            <td width="17%">-</td>
                        </tr>
                        <tr>
                            <th width="48%">Esfuerzo de Diseño</th>
                            <td width="20%">'.$l3->esfuerzo_diseno.', '.$l3->esfuerzo_diseno.'</td>
                            <td width="15%">(S)</td>
                            <td width="17%">kg/cm²</td>
                        </tr>
                        <tr>
                            <th width="48%">Esfuerzo de ruptura del Mat.</th>
                            <td width="20%">'.$l3->esfuerzo_ruptura.', '.$l3->esfuerzo_ruptura.'</td>
                            <td width="15%">(Sy)</td>
                            <td width="17%">kg/cm²</td>
                        </tr>
                        <tr>
                            <th width="48%">Factor de seg. del Material</th>
                            <td width="20%">'.$l3->factor_seguridad.'</td>
                            <td width="15%">(Fs)</td>
                            <td width="17%">-</td>
                        </tr>
                        <tr>
                            <th width="48%">Eficiencia de Sold.</th>
                            <td width="20%">'.$l3->eficiencia_solda.'</td>
                            <td width="15%">(E)</td>
                            <td width="17%">-</td>
                        </tr>
                        <tr>
                            <th width="48%">Material Envolvente</th>
                            <td width="20%">'.$mat_cpo.'</td>
                            <td width="15%">Mat</td>
                            <td width="17%">-</td>
                        </tr>
                        <tr>
                            <th width="48%">Esfuerzo de Diseño</th>
                            <td width="20%">'.$l3->esfuerzo_diseno.'</td>
                            <td width="15%">(S)</td>
                            <td width="17%">kg/cm²</td>
                        </tr>
                        <tr>
                            <th width="48%">Esfuerzo de ruptura del Mat.</th>
                            <td width="20%">'.$l3->esfuerzo_ruptura.'</td>
                            <td width="15%">(Sy)</td>
                            <td width="17%">kg/cm²</td>
                        </tr>
                        <tr>
                            <th width="48%">Factor de seg. del Material</th>
                            <td width="20%">'.$l3->factor_seguridad.'</td>
                            <td width="15%">(Fs)</td>
                            <td width="17%">-</td>
                        </tr>
                        <tr>
                            <th width="48%">Eficiencia de Sold.</th>
                            <td width="20%">'.$l3->eficiencia_solda.'</td>
                            <td width="15%">(E)</td>
                            <td width="17%">-</td>
                        </tr>
                    </table>
                </th>
            </tr>
        </table>
        <p class="tab_info">3.- CÁLCULO DEL ELEMENTO ( Tapa '.$name_tap.' )<br>DESCRIPCIÓN '.$name_tapa.'</p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">VOLUMEN</td>
            </tr>';
            if($tap[0]->tipo_tapa==1){ //ok elipsoidal --ok
                $htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π / 24) * '.$tap[0]->d_int.'³ ) + π * '.($tap[0]->d_int/2).'² * '.$tap[0]->longitud.') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[0]->d_int,3))+3.1416*pow($tap[0]->d_int/2,2)*$tap[0]->longitud)/pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==2){ //hemiesferica --ok
                $htmlg.='<tr>
                    <th width="40%">V = ((4/3) * π * r²) /1E6 </th>
                    <th width="40%"> (( 4 / 3) * 3.1416 * '.($tap[0]->d_int/2).'<sup>3</sup>) / 1E6</th>
                    <th width="20%">'.round( (4/3)*3.1416* pow(($tap[0]->d_int/2),3) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==3 || $tap[0]->tipo_tapa==4){ //toriesferica  -- ok
                $vol_abomb=round(3.1416 * pow($tap[0]->alt_concava,2)*($tap[0]->longitud-($tap[0]->alt_concava/3)) / pow(10,6),2);
                $vol_esferi=round((1/6)*(3.1416)*($tap[0]->altura_esferica) *(3*pow($tap[0]->radio_interior,2)+3*pow($tap[0]->radio_esferica,2)+$tap[0]->altura_esferica) / pow(10,6),2);
                $vol_cilin=round(3.1416*pow($tap[0]->radio_interno,2)*$tap[0]->parte_recta / pow(10,6),2);
                $volumen=$vol_abomb+$vol_esferi+$vol_cilin;
                $htmlg.='<tr>
                    <th width="40%">V = π * Ri² * Pr / 1E6</th>
                    <th width="40%"> 3.1416 * '.pow($tap[0]->radio_interno,2).' * '.$tap[0]->parte_recta.' / 1E6</th>
                    <th width="20%">'.$volumen.' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==5){ //Plana  --ok
                /*$htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π / 24) * '.$tap[0]->d_int.'³ ) + π * '.($tap[0]->d_int/2).'² * '.$arr_l[0].') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[0]->d_int,3))+(3.1416*pow($tap[0]->d_int/2,2)*$tap[0]->longitud))/pow(10,6),2).' L</th>
                </tr>';*/
                $htmlg.='<tr>
                    <th width="100%"><b>NO APLICA</b></th>
                </tr>';
            }else if($tap[0]->tipo_tapa==6){ //Plana circular apernada 
                $htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π⁡ / 24) * '.$tap[0]->d_int.'³ ) + π⁡ * '.($tap[0]->d_int/2).'² * '.$arr_l[0].') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[0]->d_int,3))+(3.1416*pow($tap[0]->d_int/2,2)*$tap[0]->longitud))/pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==7){ //abombada, ok
                $htmlg.='<tr>
                    <th width="40%"> Vt = π * m² (L − m / 3) / 1E6 </th>
                    <th width="40%"> 3.1416 * '.$tap[0]->alt_concava.'² ('.$tap[0]->d_int.' - '.$tap[0]->alt_concava.' / 3) / 1E6</th>
                    <th width="20%">'.round( 3.1416 * pow($tap[0]->alt_concava,2) * ($tap[0]->d_int - $tap[0]->alt_concava/3) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==8){ //conica, -- ok
                $htmlg.='<tr>
                    <th width="40%"> Vt = 1/3 π * Fr(r<sub>1</sub>² + r<sub>2</sub>² + (r<sub>1</sub> * r<sub>2</sub>)) / 1E6 </th>
                    <th width="40%"> 1/3 * 3.1416 * '.$tap[0]->falda_recta.'('.$tap[0]->radio_sup.'² + '.$tap[0]->radio_inf.'² + ('.$tap[0]->radio_sup.' * '.$tap[0]->radio_inf.')) / 1E6</th>
                    <th width="20%">'.round(1/3 * 3.1416 * $tap[0]->falda_recta*( pow($tap[0]->radio_sup,2) + pow($tap[0]->radio_inf,2) + ($tap[0]->radio_sup * $tap[0]->radio_inf )) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==9){ //toriconica
                $vol_coni=round(1/3 * 3.1416 * $tap[0]->falda_recta*( pow($tap[0]->radio_sup,2) + pow($tap[0]->radio_inf,2) + ($tap[0]->radio_sup * $tap[0]->radio_inf)) / pow(10,6),2);
                $vol_esferi=round((1/6)*(3.1416)*($tap[0]->altura_esferica) *(3*pow($tap[0]->radio_interior,2)+3*pow($tap[0]->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
                $vol_cilin=round(3.1416*pow($tap[0]->radio_interno,2)*$tap[0]->parte_recta / pow(10,6),2);
                $volumen=$vol_coni+$vol_esferi+$vol_cilin;
                $htmlg.='<tr>
                    <th width="40%">V = π * Ri² * Pr / 1E6</th>
                    <th width="40%"> 3.1416 * '.pow($tap[0]->radio_interno,2).' * '.$tap[0]->parte_recta.' / 1E6</th>
                    <th width="20%">'.$volumen.' L</th>
                </tr>';
            }

        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">ESPESOR MÍNIMO REQUERIDO * P INTERIOR</td>
            </tr>';
            if($tap[0]->tipo_tapa==1){
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * D / ( 2 * S * E - 0.2 * P )</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[0]->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==2){ //hemisferica
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * L / ( 2 * S * E - 0.2 * P )</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.$tap[0]->longitud.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[0]->longitud)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==3){ //Toriesférica 100/6
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = 0.885 * P * L / ( S * E - 0.1 * P )</th>
                    <th width="40%"> 0.885 * '.$l3->presion_diseno.' * '.($tap[0]->longitud).'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(0.885*$l3->presion_diseno*$tap[0]->longitud/($l3->esfuerzo_diseno*$l3->eficiencia_solda)-(0.1*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.1 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==4){ //Toriesférica 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * L *M / ( 2S * E - 0.2 * P )</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[0]->longitud.' * '.$l4->peso_molecular.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[0]->longitud*$l4->peso_molecular)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda)-(0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==5){ //Plana 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = d √ (C * P / S * E)</th>
                    <th width="40%"> '.$tap[0]->d_int.' √ '.$l2->factorc.' * '.$l3->presion_diseno.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round($tap[0]->d_int* sqrt(($l2->factorc*$l3->presion_diseno)/($l3->esfuerzo_diseno*$l3->eficiencia_solda)),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==7){ //Abombada 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = 5 * P * L / ( 6 * S)</th>
                    <th width="40%"> 5 * '.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((5*$l3->presion_diseno*$tap[0]->d_int)/(6*$l3->esfuerzo_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>6 * '.$l3->esfuerzo_diseno.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==8){ //conica
                /*$angulo = round($tap[0]->falda_recta/$tap[0]->radio_sup,2);  
                $angulo2 = round(atan($angulo),2); 
                $angulo2 = $angulo2/2;*/
                $c1=$tap[0]->falda_recta;
                $c2=$tap[0]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[0]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = PD / 2cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> (SE - 0.6P)</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[0]->d_int)/(2 * $cos * (($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno))),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2cos ('.$angulo2.') * ('.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.6 * '.$l3->presion_diseno.')</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==9){ //toriconica
                $c1=$tap[0]->falda_recta;
                $c2=$tap[0]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[0]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = PD / 2cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> (SE - 0.6P)</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[0]->d_int)/(2 * $cos * (($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno))),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2cos ('.$angulo2.') * ('.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.6 * '.$l3->presion_diseno.')</th>
                </tr>';
            }
        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">PRESIÓN INTERIOR MÁXIMA PERMISIBLE</td>
            </tr> ';
            if($tap[0]->tipo_tapa==1){
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( D + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[0].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[0])/($tap[0]->d_int+0.2*$arr_esp[0]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->d_int.' + 0.2 * '.$arr_esp[0].'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==2){ //hemisferica
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( D + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[0].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[0]/($tap[0]->longitud+(0.2*$arr_esp[0])),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->d_int.' + 0.2 * '.$arr_esp[0].'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==3){ //Toriesférica 100/6
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = S * E * t / (0.885 * L + 0.1 * t )</th>
                    <th width="40%">'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[0].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[0])/(0.885*$tap[0]->longitud)+(0.1*$arr_esp[0]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.(0.885*$tap[0]->d_int/2).' + 0.1 * '.$arr_esp[0].'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==4){ //Toriesférica 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( L * M + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[0].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[0])/($tap[0]->longitud*$l4->factor)+(0.2*$arr_esp[0]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.($tap[0]->d_int/2).' * '.$l4->factor.' + 0.2 * '.$arr_esp[0].'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==5){ //Planas 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = t² * S * E / C * d²</th>
                    <th width="40%">'.$arr_esp[0].'² * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(pow($arr_esp[0],2)*$l3->esfuerzo_diseno*$l3->eficiencia_solda/($l2->factorc*pow($tap[0]->d_int,2)),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->factorc.' * '.$tap[0]->d_int.'</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==7){ //Abombada 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 6 * S * t / ( 5 * L )</th>
                    <th width="40%">6 * '.$l3->esfuerzo_diseno.' * '.$arr_esp[0].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((6*$l3->esfuerzo_diseno*$arr_esp[0])/(5*$tap[0]->d_int),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th> 5 * '.$tap[0]->d_int.' </th>
                </tr>';
            }
            else if($tap[0]->tipo_tapa==8){ //conica
                /*$angulo = round($tap[0]->falda_recta/$tap[0]->radio_sup,2);  
                $angulo2 = round(atan($angulo),2); 
                $angulo2 = $angulo2/2;*/
                $c1=$tap[0]->falda_recta;
                $c2=$tap[0]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[0]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p> P = 2SEt cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> / D + 1.2 t cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> </th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$tap[0]->espesor * $cos)/($tap[0]->d_int+1.2*$tap[0]->espesor * $cos),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$tap[0]->d_int.' + 1.2 ('.$tap[0]->espesor.') * cos('.$angulo2.')</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==9){ //toriconica
                $c1=$tap[0]->falda_recta;
                $c2=$tap[0]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[0]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p> P = 2SEt cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"></th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[0]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$tap[0]->espesor * $cos)/($tap[0]->d_int+1.2*$tap[0]->espesor * $cos),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$tap[0]->d_int.' + 1.2 ('.$tap[0]->espesor.') * cos('.$angulo2.')</th>
                </tr>';
            }

        $htmlg.='</table>';

        //agregado y en espera de datos para la formula de velocidad de corrosión 
        if($l3->corrosion=="1"){
            $htmlg.='<table border="0" class="table" cellpadding="1">
                <tr>
                    <td></td>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VELOCIDAD DE CORROSIÓN A LARGO PLAZO (LT)</td>
                </tr>
                <tr>
                    <th width="40%">LT= T <sub>Dis</sub> - T <sub>Actual</sub> / tiempo entre (T <sub>Dis</sub> - T <sub>Actual</sub>)</th>
                    <th width="40%">LT= </th>
                    <th width="20%"> mm</th>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VIDA REMANENTE (RL)</td>
                </tr>
                <tr>
                    <th width="40%">RL= T <sub>Actual</sub> - T <sub>Req</sub> / LT</th>
                    <th width="40%">RL= </th>
                    <th width="20%"> AÑOS</th>
                </tr>
            </table>'; 
        }

        if($l->codigo_norma=="1"){
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1 UG32</p>';
        }else{
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN I</p>';
        }

$pdf->writeHTML($htmlg, true, false, true, false, '');


$pdf->AddPage('P', 'A4'); 

$htmlg='<style type="text/css">
            .table{text-align:center; font-size:11px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .cos { \221D }
        </style> 
        <p class="tab_info">4.- CALCULO DEL ELEMENTO ( ENVOLVENTE )</p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">VOLUMEN</td>
            </tr>
            <tr>
                <th width="40%">¶ * Ri² * Pr / 1E6</th>
                <th width="40%">¶ * '.($l2->d_int/2).'² * '.$l2->cuerpo.' / 1E6</th>
                <th width="20%">'.round((3.1416*pow($l2->d_int/2,2)*$l2->cuerpo)/pow(10,6),2).' L</th>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">ESPESOR MÍNIMO REQUERIDO * P INTERIOR</td>
            </tr>';
            if($l->tipo_env=="1"){ //cilindrico
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P * R / S * E - 0.6 * P</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.($l2->d_int/2).'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*($l2->d_int/2))/($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno),2).' mm</th>
                </tr>
                <tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.6 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else{
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P * R / 2 * S * E - 0.2 * P</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.($l2->d_int/2).'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*($l2->d_int/2))/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).' mm</th>
                </tr>
                <tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }
        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">PRESIÓN INTERIOR MÁXIMA PERMISIBLE</td>
            </tr>';
            
            if($l->tipo_env=="1"){ //cilindrico
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>S * E * t / ( R + 0.6 * t )</th>
                    <th width="40%">'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$l2->espesor_act.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->esfuerzo_diseno*$l3->eficiencia_solda*$l2->espesor_act)/(($l2->d_int/2)+0.6*$l2->espesor_act),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.($l2->d_int/2).' + 0.6 * '.$l2->espesor_act.'</th>
                </tr>';
            }else{
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>2 * S * E * t / ( R + 0.2 * t )</th>
                    <th width="40%">'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$l2->espesor_act.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$l2->espesor_act)/(($l2->d_int/2)+0.2*$l2->espesor_act),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.($l2->d_int/2).' + 0.2 * '.$l2->espesor_act.'</th>
                </tr>';
            }
        $htmlg.='</table>';
        //agregado y en espera de datos para la formula de velocidad de corrosión 
        if($l3->corrosion=="1"){
            $htmlg.='<table border="0" class="table" cellpadding="1">
                <tr>
                    <td></td>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VELOCIDAD DE CORROSIÓN A LARGO PLAZO (LT)</td>
                </tr>
                <tr>
                    <th width="40%">LT= T <sub>Dis</sub> - T <sub>Actual</sub> / tiempo entre (T <sub>Dis</sub> - T <sub>Actual</sub>)</th>
                    <th width="40%">LT= </th>
                    <th width="20%"> mm</th>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VIDA REMANENTE (RL)</td>
                </tr>
                <tr>
                    <th width="40%">RL= T <sub>Actual</sub> - T <sub>Req</sub> / LT</th>
                    <th width="40%">RL= </th>
                    <th width="20%"> AÑOS</th>
                </tr>
            </table>'; 
        }

        if($l->codigo_norma=="1"){
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1 UG27</p>';
        }else{
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN I</p>';
        }
        $htmlg.='<p class="tab_info">5.- CALCULO DEL ELEMENTO ( Tapa '.$name_tap2.' )<br>DESCRIPCIÓN '.$name_tapa2.'</p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">VOLUMEN</td>
            </tr>';
            if($tap[1]->tipo_tapa==1){ //ok elipsoidal --ok
                $htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π⁡ / 24) * '.$tap[1]->d_int.'³ ) + π * '.($tap[1]->d_int/2).'² * '.$tap[1]->longitud.') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[1]->d_int,3))+3.1416*pow($tap[1]->d_int/2,2)*$tap[1]->longitud)/pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==2){ //hemiesferica --ok
                $htmlg.='<tr>
                    <th width="40%">V = ((4/3) * π * r<sup>3</sup>) /1E6 </th>
                    <th width="40%"> (( 4 / 3) * 3.1416 * '.($tap[1]->d_int/2).'<sup>3</sup>) / 1E6</th>
                    <th width="20%">'.round( (4/3)*3.1416* pow(($tap[1]->d_int/2),3) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==3 || $tap[1]->tipo_tapa==4){ //toriesferica  -- ok
                $vol_abomb=round(3.1416 * pow($tap[1]->alt_concava,2)*($tap[1]->longitud-($tap[1]->alt_concava/3)) / pow(10,6),2);
                $vol_esferi=round((1/6)*(3.1416)*($tap[1]->altura_esferica) *(3*pow($tap[1]->radio_interior,2)+3*pow($tap[1]->radio_esferica,2)+$tap[1]->altura_esferica) / pow(10,6),2);
                $vol_cilin=round(3.1416*pow($tap[1]->radio_interno,2)*$tap[1]->parte_recta / pow(10,6),2);
                $vol_total=$vol_total+$vol_abomb+$vol_esferi+$vol_cilin;
                $volumen=$vol_abomb+$vol_esferi+$vol_cilin;
                $htmlg.='<tr>
                    <th width="40%">V = π * Ri² * Pr / 1E6</th>
                    <th width="40%"> 3.1416 * '.pow($tap[1]->radio_interno,2).' * '.$tap[1]->parte_recta.' / 1E6</th>
                    <th width="20%">'.$volumen.' L</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==5){ //Plana  --ok
                /*$htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π / 24) * '.$tap[0]->d_int.'³ ) + π * '.($tap[0]->d_int/2).'² * '.$arr_l[0].') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[0]->d_int,3))+(3.1416*pow($tap[0]->d_int/2,2)*$tap[0]->longitud))/pow(10,6),2).' L</th>
                </tr>';*/
                $htmlg.='<tr>
                    <th width="100%"><b>NO APLICA</b></th>
                </tr>';
            }else if($tap[1]->tipo_tapa==6){ //Plana circular apernada 
                $htmlg.='<tr>
                    <th width="40%">V = (( π / 24) * D³ ) + π * Ri² * Fr ) / 1E6</th>
                    <th width="40%">(( π⁡ / 24) * '.$tap[1]->d_int.'³ ) + π⁡ * '.($tap[1]->d_int/2).'² * '.$arr_l[1].') / 1E6</th>
                    <th width="20%">'.round((((3.1416/24)*pow($tap[1]->d_int,3))+(3.1416*pow($tap[1]->d_int/2,2)*$tap[1]->longitud))/pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==7){ //abombada, ok
                $htmlg.='<tr>
                    <th width="40%"> Vt = π * m² (L − m / 3) / 1E6 </th>
                    <th width="40%"> 3.1416 * '.$tap[1]->alt_concava.'² ('.$tap[1]->d_int.' - '.$tap[1]->alt_concava.' / 3) / 1E6</th>
                    <th width="20%">'.round( 3.1416 * pow($tap[1]->alt_concava,2) * ($tap[1]->d_int - $tap[1]->alt_concava/3) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==8){ //conica, -- ok
                $htmlg.='<tr>
                    <th width="40%"> Vt = 1/3 π * Fr(r<sub>1</sub>² + r<sub>2</sub>² + (r<sub>1</sub> * r<sub>2</sub>)) / 1E6 </th>
                    <th width="40%"> 1/3 * 3.1416 * '.$tap[1]->falda_recta.'('.$tap[1]->radio_sup.'² + '.$tap[1]->radio_inf.'² + ('.$tap[0]->radio_sup.' * '.$tap[1]->radio_inf.')) / 1E6</th>
                    <th width="20%">'.round(1/3 * 3.1416 * $tap[1]->falda_recta*( pow($tap[1]->radio_sup,2) + pow($tap[1]->radio_inf,2) + ($tap[1]->radio_sup * $tap[1]->radio_inf )) / pow(10,6),2).' L</th>
                </tr>';
            }else if($tap[0]->tipo_tapa==9){ //toriconica
                $vol_coni=round(1/3 * 3.1416 * $tap[1]->falda_recta*( pow($tap[1]->radio_sup,2) + pow($tap[1]->radio_inf,2) + ($tap[1]->radio_sup * $tap[1]->radio_inf)) / pow(10,6),2);
                $vol_esferi=round((1/6)*(3.1416)*($tap[1]->altura_esferica) *(3*pow($tap[1]->radio_interior,2)+3*pow($tap[1]->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
                $vol_cilin=round(3.1416*pow($tap[1]->radio_interno,2)*$tap[1]->parte_recta / pow(10,6),2);
                $vol_total=$vol_total+$vol_coni+$vol_esferi+$vol_cilin;
                $volumen=$vol_coni+$vol_esferi+$vol_cilin;
                $htmlg.='<tr>
                    <th width="40%">V = π * Ri² * Pr / 1E6</th>
                    <th width="40%"> 3.1416 * '.pow($tap[1]->radio_interno,2).' * '.$tap[1]->parte_recta.' / 1E6</th>
                    <th width="20%">'.$volumen.' L</th>
                </tr>';
            }
        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">ESPESOR MÍNIMO REQUERIDO * P INTERIOR</td>
            </tr>';
            if($tap[1]->tipo_tapa==1){
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * D / ( 2 * S * E - 0.2 * P )</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[1]->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==2){ //hemisferica
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * L / ( 2 * S * E - 0.2 * P )</th>
                    <th width="40%">'.$l3->presion_diseno.' * '.$tap[1]->longitud.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[1]->longitud)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==3){ //Toriesférica 100/6
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = 0.885 * P * L / ( S * E - 0.1 * P )</th>
                    <th width="40%"> 0.885 * '.$l3->presion_diseno.' * '.$tap[1]->longitud.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(0.885*$l3->presion_diseno*$tap[1]->longitud/($l3->esfuerzo_diseno*$l3->eficiencia_solda)-(0.1*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.1 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==4){ //Toriesférica 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = P * L *M / ( 2S * E - 0.2 * P )</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[1]->longitud.' * '.$l4->peso_molecular.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[1]->longitud*$l4->peso_molecular)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda)-(0.2*$l3->presion_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.2 * '.$l3->presion_diseno.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==5){ //Plana 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = d √ (C * P / S * E)</th>
                    <th width="40%"> '.$tap[1]->d_int.' √ '.$l2->factorc.' * '.$l3->presion_diseno.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round($tap[1]->d_int* sqrt(($l2->factorc*$l3->presion_diseno)/($l3->esfuerzo_diseno*$l3->eficiencia_solda)),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==7){ //Abombada 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = 5 * P * L / ( 6 * S)</th>
                    <th width="40%"> 5 * '.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((5*$l3->presion_diseno*$tap[1]->d_int)/(6*$l3->esfuerzo_diseno),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>6 * '.$l3->esfuerzo_diseno.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==8){ //conica
                /*$angulo = round($tap[1]->falda_recta/$tap[1]->radio_sup,2);  
                $angulo2 = round(atan($angulo),2); 
                $angulo2 = $angulo2/2;*/
                $c1=$tap[1]->falda_recta;
                $c2=$tap[1]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[1]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = PD / 2cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> (SE - 0.6P)</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[1]->d_int)/(2 * $cos * (($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno))),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2cos ('.$angulo2.') * ('.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.6 * '.$l3->presion_diseno.')</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==9){ //toriconica
                $c1=$tap[1]->falda_recta;
                $c2=$tap[1]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[1]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>t = PD / 2cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> (SE - 0.6P)</th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->presion_diseno*$tap[1]->d_int)/(2 * $cos * (($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno))),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>2cos ('.$angulo2.') * ('.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' - 0.6 * '.$l3->presion_diseno.')</th>
                </tr>';
            }
        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td colspan="3" class="backg">PRESIÓN INTERIOR MÁXIMA PERMISIBLE</td>
            </tr>';
            if($tap[1]->tipo_tapa==1){
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( D + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[1].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[1])/($tap[1]->d_int+0.2*$arr_esp[0]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->d_int.' + 0.2 * '.$arr_esp[1].'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==2){ //hemisferica
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( D + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[1].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[1]/($tap[1]->longitud+(0.2*$arr_esp[1])),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->d_int.' + 0.2 * '.$arr_esp[0].'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==3){ //Toriesférica 100/6
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = S * E * t / (0.885 * L + 0.1 * t )</th>
                    <th width="40%">'.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[1].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(($l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[1])/(0.885*$tap[1]->longitud)+(0.1*$arr_esp[1]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.(0.885*$tap[1]->d_int/2).' + 0.1 * '.$arr_esp[1].'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==4){ //Toriesférica 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 2 * S * E * t / ( L * M + 0.2 * t )</th>
                    <th width="40%">2 * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.' * '.$arr_esp[1].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$arr_esp[1])/($tap[1]->longitud*$l4->factor)+(0.2*$arr_esp[1]),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.($tap[1]->d_int/2).' * '.$l4->factor.' + 0.2 * '.$arr_esp[1].'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==5){ //Planas 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = t² * S * E / (C * d²)</th>
                    <th width="40%">'.$arr_esp[1].'² * '.$l3->esfuerzo_diseno.' * '.$l3->eficiencia_solda.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round(pow($arr_esp[1],2)*$l3->esfuerzo_diseno*$l3->eficiencia_solda/($l2->factorc*pow($tap[1]->d_int,2)),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th>'.$l2->factorc.' * '.$tap[1]->d_int.'</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==7){ //Abombada 
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p>P = 6 * S * t / ( 5 * L )</th>
                    <th width="40%">6 * '.$l3->esfuerzo_diseno.' * '.$arr_esp[1].'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((6*$l3->esfuerzo_diseno*$arr_esp[1])/(5*$tap[1]->d_int),2).' kg/cm²</th>
                </tr>
                <tr>
                    <th> 5 * '.($tap[1]->d_int/2).' </th>
                </tr>';
            }else if($tap[1]->tipo_tapa==8){ //conica
                /*$angulo = round($tap[1]->falda_recta/$tap[1]->radio_sup,2);  
                $angulo2 = round(atan($angulo),2); 
                $angulo2 = $angulo2/2;*/
                $c1=$tap[1]->falda_recta;
                $c2=$tap[1]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[1]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p> P = 2SEt cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> / D + 1.2 t cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"> </th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$tap[1]->espesor * $cos)/($tap[1]->d_int+1.2*$tap[1]->espesor * $cos),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$tap[1]->d_int.' + 1.2 ('.$tap[1]->espesor.') * cos('.$angulo2.')</th>
                </tr>';
            }else if($tap[1]->tipo_tapa==9){ //toriconica
                $c1=$tap[1]->falda_recta;
                $c2=$tap[1]->radio_sup/2;
                $c1 = pow($c1,2);
                $c2 = pow($c2,2);
                $h=$c1+$c2;
                $h=round(sqrt($h),2);
                $angulo = $tap[1]->falda_recta/$h;
                $ang = asin($angulo);
                $angulo2 = round(acos($angulo)/pi()*180,2);
                if(cos($angulo2)<0){
                  $cos = cos($angulo2)* -1; 
                }else{
                  $cos = cos($angulo2); 
                }
                $htmlg.='<tr>
                    <th rowspan="2" width="40%"><p class="pspaces"></p> P = 2SEt cos <img src="'.FCPATH.'public/img/formulas/angulo.png" width="10px"></th>
                    <th width="40%"> '.$l3->presion_diseno.' * '.$tap[1]->d_int.'</th>
                    <th rowspan="2" width="20%"><p class="pspaces"></p>'.round((2*$l3->esfuerzo_diseno*$l3->eficiencia_solda*$tap[1]->espesor * $cos)/($tap[1]->d_int+1.2*$tap[1]->espesor * $cos),2).' mm</th>
                </tr>';
                $htmlg.='<tr>
                    <th>'.$tap[1]->d_int.' + 1.2 ('.$tap[1]->espesor.') * cos('.$angulo2.')</th>
                </tr>';
            }
        $htmlg.='</table>';

        //agregado y en espera de datos para la formula de velocidad de corrosión 
        if($l3->corrosion=="1"){
            $htmlg.='<table border="0" class="table" cellpadding="1">
                <tr>
                    <td></td>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VELOCIDAD DE CORROSIÓN A LARGO PLAZO (LT)</td>
                </tr>
                <tr>
                    <th width="40%">LT= T <sub>Dis</sub> - T <sub>Actual</sub> / tiempo entre (T <sub>Dis</sub> - T <sub>Actual</sub>)</th>
                    <th width="40%">LT= </th>
                    <th width="20%"> mm</th>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">CÁLCULO DE VIDA REMANENTE (RL)</td>
                </tr>
                <tr>
                    <th width="40%">RL= T <sub>Actual</sub> - T <sub>Req</sub> / LT</th>
                    <th width="40%">RL= </th>
                    <th width="20%"> AÑOS</th>
                </tr>
            </table>'; 
        }

        if($l->codigo_norma=="1"){
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1 UG32</p>';
        }else{
            $htmlg.='<p style="font-size:10px">CÓDIGO ASME SECCIÓN I</p>';
        }

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 

/*if($l->orientacion=="1") { //vertical
    $area_total=6*$l4->capacidad*(sqrt($l4->peso_molecular*$l4->temp_absoluta*1)/($l4->constante_rot*$l4->coeficiente_desc*$l4->presion_absoluta*1));
    $area_total= round($area_total,2)." cm²";
    $area_tot_fin= round($area_total,2)." cm²";
    $img_formula = '<td width="30%" rowspan="2"><p class="pspaces"></p>6 V √  (M T Z) / (C Kd P1 Kb)</td>';
    $formula='<table><tr>
                <th rowspan="3"><p class="pspaces"></p>6* '.$l4->capacidad.' * &#8730;</th>
                <th>('.$l4->peso_molecular.' * '.$l4->temp_absoluta.' * 1)</th>
            </tr>
            <tr>
                <th>_____________________</th>
            </tr>
            <tr>
                <th>('.$l4->constante_rot.' * '.$l4->coeficiente_desc.' * '.$l4->presion_absoluta.' * 1)</th>
            </tr>
        </table>';
}else{ //horizontal
    $area_total=$l4->capacidad/($l4->constante_rot*$l4->coeficiente_desc*$l4->presion_absoluta*sqrt($l4->peso_molecular/$l4->temp_absoluta));
    $area_total= round($area_total,4)." cm² = ".round($area_total*100,2)." mm²";
    $area_tot_fin= round($area_total*100,2)." mm²";
    $img_formula = '<td width="30%"><img src="'.base_url().'public/img/formulas/formula1.png" width="120px"></td>';
    $formula='<table><tr>
                <th>'.$l4->capacidad.'</th>
            </tr>
            <tr>
                <th>_________________________________</th>
            </tr>
            <tr>
                <th>('.$l4->constante_rot.')('.$l4->coeficiente_desc.')('.$l4->presion_absoluta.') &#8730;'.$l4->peso_molecular.'/'.$l4->temp_absoluta.'</th>
            </tr>
        </table>';
}*/

$img_formula=""; $formula=""; $area_total=0; $area_tot_fin=0;
if($l4->tipo_disp==1){
    $img_formula = '<td width="30%"><img src="'.FCPATH.'public/img/formulas/formula1.png" width="120px"></td>';
    $formula='<table><tr>
            <th>'.$l4->capacidad.'</th>
        </tr>
        <tr>
            <th>_________________________________</th>
        </tr>
        <tr>
            <th>('.$l4->constante_rot.')('.$l4->coeficiente_desc.')('.$l4->presion_absoluta.') &#8730;'.$l4->peso_molecular.'/'.$l4->temp_absoluta.'</th>
        </tr>
    </table>';
    $area_total=$l4->capacidad/($l4->constante_rot*$l4->coeficiente_desc*$l4->presion_absoluta*sqrt(($l4->peso_molecular/$l4->temp_absoluta)));
    $area_tot_fin= round($area_total,2)." mm²";
    $area_total= round($area_total/10,4)." cm² = ".round($area_total,2)." mm²";   
}
else if($l4->tipo_disp==2){
    $img_formula = '<td width="30%"><img src="'.FCPATH.'public/img/formulas/formula2.png" width="120px"></td>';
    $formula='<table><tr>
            <th>'.$l4->capacidad.'</th>
        </tr>
        <tr>
            <th>_________________________________</th>
        </tr>
        <tr>
            <th>('.$l4->constante_rot.')('.$l4->coeficiente_desc.')('.$l4->presion_absoluta.')</th>
        </tr>
    </table>';
    $area_total=$l4->capacidad/($l4->constante_rot*$l4->coeficiente_desc*$l4->presion_absoluta);
    $area_tot_fin= round($area_total,2)." mm²";
    $area_total= round($area_total/10,4)." cm² = ".round($area_total,2)." mm²";
}else if($l4->tipo_disp==3){
    $img_formula = '<td width="30%"><img src="'.FCPATH.'public/img/formulas/formula3.png" width="120px"></td>';
    $formula='<table><tr>
            <th>'.$l4->capacidad.'</th>
        </tr>
        <tr>
            <th>_________________________________</th>
        </tr>
        <tr>
            <th>(5.092 * '.sqrt($l4->presion_absoluta-$l4->presion_atmo*$l4->densidad_flui).')</th>
        </tr>
    </table>';
    $area_total=$l4->capacidad/($l4->constante_rot*$l4->coeficiente_desc)*sqrt($l4->presion_absoluta-$l4->presion_atmo*$l4->densidad_flui);
    $area_tot_fin= round($area_total,2)." mm²";
    $area_total= round($area_total/10,4)." cm² = ".round($area_total,2)." mm²";
}

$img_equipo="";
if($l5->img_equipo!=""){
    $img_equipo='<img src="'.FCPATH."uploads/equipos_cliente/".$l5->img_equipo.'" width="480px" height="440px">';
}
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
        </style> 

        <table border="1" class="table" cellpadding="3">
            <tr>
                <td class="backg">6.- FOTOGRAFÍA DEL EQUIPO</td>
            </tr>
            <tr>
                <th height="440px">'.$img_equipo.'</th>
            </tr>
        </table>
        ';
        if($l4->tipo_disp!="4"){
            $htmlg.='<p class="tab_info">7.- VÁLVULA DE SEGURIDAD</p>';
            if($l4->tipo_disp=="1"){ //formula 1
                $htmlg.='<table border="1" class="table" cellpadding="6">
                    <tr>
                        <td colspan="8" class="backg">ÁREA MÍNIMA REQUERIDA</td>
                    </tr>
                    <tr>
                        <th width="29%">Capacidad:</th>
                        <th width="5%">(W)</th>
                        <th width="8%">'.$l4->capacidad.'</th>
                        <th width="7%">Kg/h</th>
                        <th width="25%">Presión Absoluta:</th>
                        <th width="10%">(P1)</th>
                        <th width="8%">'.$l4->presion_absoluta.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Peso Molecular:</th>
                        <th width="5%">(M)</th>
                        <th width="8%">'.$l4->peso_molecular.'</th>
                        <th width="7%">kg-mol</th>
                        <th width="25%">Presión Regulada:</th>
                        <th width="10%">Preg</th>
                        <th width="8%">'.$l4->presion_regulada.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Temperatura Absoluta a la Entrada:</th>
                        <th width="5%">(T)</th>
                        <th width="8%">'.$l4->temp_absoluta.'</th>
                        <th width="7%">K</th>
                        <th width="25%">Presión Atmosférica:</th>
                        <th width="10%">(Patm)</th>
                        <th width="8%">'.$l4->presion_atmo.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Constante C para aire en SI:</th>
                        <th width="5%">(C)</th>
                        <th width="8%">'.$l4->constante_rot.'</th>
                        <th width="7%">-</th>
                        <th width="25%">Diámetro de válvula Instalada:</th>
                        <th width="10%">(Dinst)</th>
                        <th width="8%">'.$l4->diametro_valvula.'</th>
                        <th width="8%">mm</th>
                    </tr>
                    <tr>
                        <th width="29%">Coeficiente de Descarga:</th>
                        <th width="5%">(Kd)</th>
                        <th width="8%">'.$l4->coeficiente_desc.'</th>
                        <th width="7%">-</th>
                        <th width="25%"></th>
                        <th width="10%"></th>
                        <th width="8%"></th>
                        <th width="8%"></th>
                    </tr>
                </table>';
            }else if($l4->tipo_disp=="2"){ //formula 2
                $htmlg.='<table border="1" class="table" cellpadding="6">
                    <tr>
                        <td colspan="8" class="backg">ÁREA MÍNIMA REQUERIDA</td>
                    </tr>
                    <tr>
                        <th width="29%">Capacidad:</th>
                        <th width="5%">(W)</th>
                        <th width="8%">'.$l4->capacidad.'</th>
                        <th width="7%">Kg/h</th>
                        <th width="25%">Presión Absoluta:</th>
                        <th width="10%">(P1)</th>
                        <th width="8%">'.$l4->presion_absoluta.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        th width="29%">Constante C para aire en SI:</th>
                        <th width="5%">(C)</th>
                        <th width="8%">'.$l4->constante_rot.'</th>
                        <th width="7%">-</th>
                        <th width="25%">Presión Regulada:</th>
                        <th width="10%">Preg</th>
                        <th width="8%">'.$l4->presion_regulada.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Coeficiente de Descarga:</th>
                        <th width="5%">(Kd)</th>
                        <th width="8%">'.$l4->coeficiente_desc.'</th>
                        <th width="7%">-</th>
                        <th width="25%">Presión Atmosférica:</th>
                        <th width="10%">(Patm)</th>
                        <th width="8%">'.$l4->presion_atmo.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%"></th>
                        <th width="5%"></th>
                        <th width="8%"></th>
                        <th width="7%"></th>
                        <th width="25%">Diámetro de válvula Instalada:</th>
                        <th width="10%">(Dinst)</th>
                        <th width="8%">'.$l4->diametro_valvula.'</th>
                        <th width="8%">mm</th>
                    </tr>
                </table>';
            }else if($l4->tipo_disp=="3"){ //formula 2
                $htmlg.='<table border="1" class="table" cellpadding="6">
                    <tr>
                        <td colspan="8" class="backg">ÁREA MÍNIMA REQUERIDA</td>
                    </tr>
                    <tr>
                        <th width="29%">Capacidad:</th>
                        <th width="5%">(W)</th>
                        <th width="8%">'.$l4->capacidad.'</th>
                        <th width="7%">Kg/h</th>
                        <th width="25%">Presión Absoluta:</th>
                        <th width="10%">(P1)</th>
                        <th width="8%">'.$l4->presion_absoluta.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        th width="29%">Densidad del fluido:</th>
                        <th width="5%">(ρ)</th>
                        <th width="8%">'.$l4->densidad_flui.'</th>
                        <th width="7%">Kg/m<sup>3</th>
                        <th width="25%">Presión Regulada:</th>
                        <th width="10%">Preg</th>
                        <th width="8%">'.$l4->presion_regulada.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Constante C para aire en SI:</th>
                        <th width="5%">(C)</th>
                        <th width="8%">'.$l4->constante_rot.'</th>
                        <th width="7%">-</th>
                        <th width="25%">Presión Atmosférica:</th>
                        <th width="10%">(Patm)</th>
                        <th width="8%">'.$l4->presion_atmo.'</th>
                        <th width="8%">Kg/cm²</th>
                    </tr>
                    <tr>
                        <th width="29%">Diámetro de válvula Instalada:</th>
                        <th width="5%">(Dinst)</th>
                        <th width="8%">'.$l4->diametro_valvula.'</th>
                        <th width="7%">mm</th>
                        <th width="25%">Diámetro de válvula Instalada:</th>
                        <th width="10%">(kd)</th>
                        <th width="8%">'.$l4->coeficiente_desc.'</th>
                        <th width="8%">-</th>
                    </tr>
                </table>';
            }
            
            /*$htmlg.='<table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="8" class="backg">ÁREA MÍNIMA REQUERIDA</td>
                </tr>
                <tr>
                    <th width="29%">Capacidad:</th>
                    <th width="5%">(W)</th>
                    <th width="8%">'.$l4->capacidad.'</th>
                    <th width="7%">Kg/h</th>
                    <th width="25%">Presión Absoluta:</th>
                    <th width="10%">(P1)</th>
                    <th width="8%">'.$l4->presion_absoluta.'</th>
                    <th width="8%">Kg/cm²</th>
                </tr>
                <tr>
                    <th width="29%">Constante de relación de calores específicos:</th>
                    <th width="5%">(C)</th>
                    <th width="8%">'.$l4->constante_rot.'</th>
                    <th width="7%">-</th>
                    <th width="25%">Presión Regulada:</th>
                    <th width="10%">Preg</th>
                    <th width="8%">'.$l4->presion_regulada.'</th>
                    <th width="8%">Kg/cm²</th>
                </tr>
                <tr>
                    <th width="29%">Temperatura Absoluta a la Entrada:</th>
                    <th width="5%">(T)</th>
                    <th width="8%">'.$l4->temp_absoluta.'</th>
                    <th width="7%">K</th>
                    <th width="25%">Presión Atmosférica:</th>
                    <th width="10%">(Patm)</th>
                    <th width="8%">'.$l4->presion_atmo.'</th>
                    <th width="8%">Kg/cm²</th>
                </tr>
                <tr>
                    <th width="29%">Peso Molecular:</th>
                    <th width="5%">(M)</th>
                    <th width="8%">'.$l4->peso_molecular.'</th>
                    <th width="7%">-</th>
                    <th width="25%">Diámetro de válvula Instalada:</th>
                    <th width="10%">(Dinst)</th>
                    <th width="8%">'.$l4->diametro_valvula.'</th>
                    <th width="8%">mm</th>
                </tr>
                <tr>
                    <th width="29%">Coeficiente de Descarga:</th>
                    <th width="5%">(Kd)</th>
                    <th width="8%">'.$l4->coeficiente_desc.'</th>
                    <th width="7%">-</th>
                    <th width="25%"></th>
                    <th width="10%"></th>
                    <th width="8%"></th>
                    <th width="8%"></th>
                </tr>
            </table>';*/
            $htmlg.='<table><tr><th></th></tr></table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3" class="backg">ÁREA MÍNIMA REQUERIDA</td>
                </tr>
                <!-- dependiendo del tipo de valvula es el calculo y formula -->
                <tr>
                    '.$img_formula.'
                    <td width="40%">'.$formula.'</td>
                    <td width="30%" rowspan="2"><p class="pspaces"></p>'.$area_total.'</td>
                </tr>
            </table>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td colspan="3">* VÁLVULA Instalada en el equipo de:</td>
                </tr>
                <tr>
                    <td width="33.333%">Diámetro de salida: '.$l4->diametro_valvula.' mm</td>';
                    if($l4->tipo_disp=="1") $tipo="Válvula de seguridad (gas)";
                    else if($l4->tipo_disp=="2") $tipo="Válvula de seguridad (vapor)";
                    else if($l4->tipo_disp=="3") $tipo="Válvula de alivio (liquido)";
                    else if($l4->tipo_disp=="4") $tipo="Protegido por el sistema";
                    $htmlg.='<td width="33.333%">Tipo: '.$tipo.'</td>
                    <td width="33.333%">Calibrada a '.$l4->pesion_calibra.' kg/cm²</td>
                </tr>
                <tr>
                    <td width="33.333%"> > Área de desfogue total: '.$l4->area_total.' mm²</td>
                    <td width="33.333%"> > Área mínima requerida:: '.$area_tot_fin.'</td>
                    <td width="33.333%"> </td>
                </tr>
            </table>';
        }else{
            $htmlg.='<p class="tab_info">7.- DISPOSITIVO PROTEGIDO POR SISTEMA</p>
            <table border="1" class="table" cellpadding="6">
                <tr>
                    <td class="backg">JUSTIFICACIÓN TÉCNICA</td>
                </tr>
                <tr>
                    <td>'.$l4->justif_tecnica.'</td>
                </tr>
            </table>';
        }
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info">8.- CONCLUSIONES</p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td class="backg">CONCLUSIONES</td>
            </tr>
            <tr>
                <th><p class="p_just">Este Informe de Resultados se extiende a solicitud expresa del usuario para dar evidencia de las condiciones actuales del equipo y dar cumplimiento con los requisitos establecidos en la <b>NOM-020-STPS-2011</b> de la Secretaria de Trabajo y Previsión Social.</p>
                    <p class="p_just">La determinación del material de construcción nos ha permitido realizar la memoria de cálculo para el espesor mínimo requerido en las condiciones de operación actuales del equipo con Número de Identificación <b>'.$l->num_identifica.'</b> con lo que aseguramos que cuenta con el espesor requerido para soportar y transmitir las cargas inducidas por presión interna.</p>
                    <p class="p_just"><b>La Memoria de calculo actualizada que contenga lo siguiente:</b></p>
                    <ol>
                        <li> - La presión interna máxima que soporte el equipo, en sus partes críticas, tales como envolventes, tapas, hogar, espejos y tubos, entre otros, según aplique;</li>
                        <li> - Los espesores mínimos requeridos, en sus partes;</li>
                        <li> - El área de desfogue de los dispositivos de seguridad.</li>
                        <li> - La superficie de calefacción, cuando se trate de generadores de vapor o calderas;</li>
                        <li> - La capacidad volumétrica, en el caso de recipientes sujetos a presión y recipientes criogénicos, y</li>
                        <li> - La capacidad generativa, cuando se trate de generadores de vapor o calderas;</li>
                    </ol>
                </th>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td class="backg">CONCLUSIONES</td>
            </tr>
            <tr>
                <th><p class="p_just">En virtud de los resultados obtenidos en la Memoria de calculo, se concluye que el:</p>
                    <table><tr><th style="text-align:left">Nombre del Recipiente:</th>
                            <td><u>'.$l->nombre_equipo.'</u></td>
                        </tr>
                        <tr><th style="text-align:left">Número de Identificación:</th>
                            <td><u>'.$l->num_identifica.'</u></td>
                        </tr>
                        <tr><th style="text-align:left">Número de Serie:</th>
                            <td><u>'.$l->num_serie.'</u></td>
                        </tr>
                        <tr><th style="text-align:left">Ubicación del Equipo:</th>
                            <td><u>'.$l->ubicacion.'</u></td>
                        </tr>
                        <tr><th style="text-align:left">Utilizado por:</th>
                            <td><u>'.$GLOBALS["nombre"].'</u></td>
                        </tr>
                    </table>
                </th>
            </tr>
            <tr>
                <th><p class="p_just">SI CUMPLE con los requisitos técnicos para ser utilizado como Recipiente Sujeto a Presión mientras conserve los Espesores Mínimos Calculados en esta Memoria de Cálculo, no presente defectos estructurales y no sean modificados tanto su construcción, instalación así como sus parámetros de diseño.</p>
                    <p class="p_just">El Técnico en Ensayos No Destructivos recomienda al usuario que proporcione el mantenimiento adecuado al equipo a fin de conservarlo en óptimas condiciones y cumplir con la Normatividad vigente. Así mismo se recomienda proporcionar al personal que opera el equipo la Capacitación y Adiestramiento necesario para reducir al mínimo su deterioro.</p>
                    <p class="p_just">La firma del presente no representa garantía explícita o implícita de ningún tipo, por lo que nuestra empresa no puede ser ni será responsable por daños de cualquier tipo que puedan originarse en los equipos e instalaciones.</p>
                </th>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <td class="backg">FIRMA</td>
            </tr>
            <tr>
                <th><p class="p_just">El presente documento se firma en la ciudad de Puebla, el. '.$GLOBALS["fecha_inspeccion"].'</p>
                <p class="pspaces2"></p>
                <p><b>___________________________________</b></p>
                <p><b>'.$tecnico.'</b></p>
                <p><b>CED. PROF. '.$cedula.'</b></p></th>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->Output('memoria_calculo.pdf', 'I');

?>