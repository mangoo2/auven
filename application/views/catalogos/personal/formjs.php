<script src="<?php echo base_url(); ?>public/fileinput/fileinput.js?v=<?php echo date("Ymd"); ?>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/fileinput/fileinput.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/canvas.js?v=<?php echo date('YmdGis'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/form_personal.js?v=<?php echo date('YmdGis') ?>"></script> 
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($firma!=''){ ?>
			$('#cont_firma').hide();
			$('#cont_firma_view').show();
			$('.firma_img').html('<img src="<?php echo base_url().'public/firmas/'.$firma;?>">');
		<?php } ?>
		$("#cedula").fileinput({
            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["pdf"],
            browseLabel: 'Seleccionar escaneo',
            uploadUrl: base_url+'Personal/cargacedula',
            maxFilePreviewSize: 5000,
            <?php if($cedula!=''){ ?>
            	initialPreviewAsData: true,
            	initialPreview: ["<?php echo base_url().'public/doc_cedula/'.$cedula;?>"],
            	initialPreviewConfig: [
			        {type: 'pdf', description: "<h5>PDF File One</h5> This is a representative placeholder description number one for this PDF file.", size: 3072}
			    ],
            <?php }?>
            uploadExtraData: function (previewId, index) {
              var info = {
                    id:$('#personalId_tem').val()

                  };
              return info;
          }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        });
	});
</script>