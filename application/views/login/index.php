

<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
        height: 400px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -200px;
        margin-top: -200px;
        z-index: 1;
        display: none;" align="center"> 
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <h2 style="color: white;">Iniciando Sesión</h2>
        <h3 style="color: white;">Conectado a la base de datos</h3>
    </div>
    <div class="login-box">
        <div class="">
            <div class="body margen_m">
                <form id="login-form" action="#" role="form">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div class="align-center">
                                <img style="width: 60%;" src="<?php echo base_url();?>public/img/logo_icon.png"/>
                                <h3 style="color: white;">Welcome back, please login to your account</h3>
                            </div>
                            <br>
                            <div class="form-group input-group">
                                <label style="color: white;">Usuario</label>
                                <input style="text-align: center;" type="text" class="form-control input_login" name="txtUsuario" id="txtUsuario" required autofocus>
                            </div>
                            <div class="form-group input-group">
                                <label style="color: white;">Contraseña</label>
                                <input style="text-align: center;" type="password" class="form-control input_login" name="txtPass" id="txtPass" required>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button style="border-radius: 18px;" class="btn btn-block bg-blue waves-effect" type="submit" id="login-submit">Iniciar sesión</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="alert bg-pink" id="error" style="display: none">
                    <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
                </div>

            </div>
        </div>
    </div>

