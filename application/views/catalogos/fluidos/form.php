<div class="row">
	<div class="col-12">
		<div class="content-header"><?php echo $tittle ?> fluidos</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body table-responsive">
					<form method="post" id="form_data">
						<input type="hidden" name="id" id="id_reg" value="<?php echo $id ?>">
						<div class="row">
							<div class="col-md-4">
                                <div class="media-body" align="center">
                                    <span class="img_foto" align="center">
                                    <?php if($foto!=''){ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>uploads/fluidos/<?php echo $foto ?>" class="rounded mr-3" height="80" width="80">
                                    <?php }else{ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>public/img/carpeta.png" height="80" width="80">
                                    <?php } ?>
                                    </span>
                                    <div class="col-12 ">
                                        <br>
                                        <label class="btn btn-sm bg-light-primary mb-sm-0" for="foto_avatar">Subir foto</label>
                                        <input type="file" id="foto_avatar" hidden>
                                        <a class="btn btn-sm bg-light-secondary ml-sm-2" onclick="reset_img()">Reiniciar</a>
                                    </div>
                                    <p class="text-muted mb-0 mt-1 mt-sm-0">
                                        <small>Permitido JPG, GIF or PNG. Tamaño máximo de 800kB</small>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-8">
                            	<div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Nombre fluido</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>">
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <div class="col-lg-3" align="right">
                                        <label>Peligroso</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <select id="peligroso" name="peligroso" class="form-control">
                                            <option value="0"></option>
                                            <option <?php if($peligroso=="1") echo "selected"; ?> value="1">Si</option>
                                            <option <?php if($peligroso=="2") echo "selected"; ?> value="2">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </form> 
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" onclick="add_form()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Fluidos" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
				</div>
			</div>
		</div>
	</div>
</div>