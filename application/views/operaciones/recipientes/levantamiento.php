<div class="row">
	<div class="col-12">
		<div class="content-header">Levantamiento de datos de equipo</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body table-responsive">
					<div class="icons-tab-steps" id="icons-tab-steps">
                        <h6>Paso 1</h6>
                        <fieldset>
							<form method="post" id="form_p1">
								<input type="hidden" name="id_operacion" id="id_operacion" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_reg" value="<?php if(isset($l)) echo $l->id; else echo "0"; ?>">
								<div class="row">	
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Nombre del equipo</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="nombre_equipo" value="<?php if(isset($l)) echo $l->nombre_equipo; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>Número de serie</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="num_serie" value="<?php if(isset($l)) echo $l->num_serie; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Ubicación</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="ubicacion" value="<?php if(isset($l)) echo $l->ubicacion; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Fabricante</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="text" name="fabricante" value="<?php if(isset($l)) echo $l->fabricante; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>En operación</label>
										</div>
										<div class="col-md-3">
											<select id="en_opera" name="en_opera" class="form-control">
												<option <?php if(isset($l) && $l->en_opera=="1") echo "selected"; ?> value='1'>Si</option>
												<option <?php if(isset($l) && $l->en_opera=="2") echo "selected"; ?> value='2'>No</option>
											</select>
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Estado de uso</label>
										</div>
										<div class="col-md-3">
											<select id="estado_uso" name="estado_uso" class="form-control">
												<option <?php if(isset($l) && $l->estado_uso=="1") echo "selected"; ?> value='1'>Nuevo</option>
												<option <?php if(isset($l) && $l->estado_uso=="2") echo "selected"; ?> value='2'>Usado</option>
											</select>
										</div>
							
										<div class="col-md-3 text-right align-self-center">
											<label>Fluido</label>
										</div>
										<div class="col-md-3">
											<select id="id_fluido" name="id_fluido" class="form-control">
												<?php $sel=""; foreach ($fluido as $f) { if(isset($l->id_fluido) && $l->id_fluido==$f->id) $sel="selected"; else $sel="";
													echo "<option data-peligro='".$f->peligroso."' ".$sel." value='".$f->id."'>".$f->nombre."</option>";
												} ?>
											</select>
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Orientación</label>
										</div>
										<div class="col-md-3">
											<select id="orientacion" name="orientacion" class="form-control">
												<option <?php if(isset($l) && $l->orientacion=="1") echo "selected"; ?> value='1'>Vertical</option>
												<option <?php if(isset($l) && $l->orientacion=="2") echo "selected"; ?> value='2'>Horizontal</option>
											</select>
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Origen</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="text" name="origen" value="<?php if(isset($l)) echo $l->origen; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Certificado de fabricante</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="certif_fabrica" value="<?php if(isset($l)) echo $l->certif_fabrica; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Número de identificación</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="num_identifica" value="<?php if(isset($l)) echo $l->num_identifica; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Año de fabricación</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="text" name="anio_fabrica" value="<?php if(isset($l)) echo $l->anio_fabrica; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Tipo Envolvente</label>
										</div>
										<div class="col-md-3">
											<select id="tipo_env" name="tipo_env" class="form-control">
												<option <?php if(isset($l) && $l->tipo_env=="1") echo "selected"; ?> value='1'>Cilindríco</option>
												<option <?php if(isset($l) && $l->tipo_env=="2") echo "selected"; ?> value='2'>Esférico</option>
											</select>
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Código o norma de cálculo</label>
										</div>
										<div class="col-md-9">
											<!--<input class="form-control" type="text" name="codigo_norma" value="<?php if(isset($l)) echo $l->codigo_norma; ?>">-->
											<select id="codigo_norma" name="codigo_norma" class="form-control">
												<option value='1'>CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1</option>
												<option value='2'>CÓDIGO ASME SECC. I</option>
											</select>
										</div>
									</div>
									
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Fecha de inspección</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="date" name="fecha_inspeccion" value="<?php if(isset($l)) echo $l->fecha_inspeccion; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Responsable de empresa</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="responsable" value="<?php if(isset($l)) echo $l->responsable; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Puesto</label>
										</div>
										<div class="col-md-9">
											<input class="form-control" type="text" name="puesto" value="<?php if(isset($l)) echo $l->puesto; ?>">
										</div>
									</div>
									<div class="col-12 form-group d-flex">
		                                <div class="col-md-3 text-right align-self-center">
											<label>Consultor</label>
										</div>
										<div class="col-md-2">
											<div class="custom-switch custom-control-inline mb-1 mb-xl-0">
			                                    <input type="checkbox" class="custom-control-input" id="consultor" <?php if(isset($l) && $l->consultor==1) echo "checked"; ?>>
			                                    <label class="custom-control-label mr-1" for="consultor">
			                                        <span style="color: #ff000000;">.</span>
			                                    </label>
			                                </div>
										</div>
		                                <div class="col-md-2 text-right align-self-center">
											<label>Técnico</label>
										</div>
										<div class="col-md-5">
											<select id="id_tecnico" name="id_tecnico" class="form-control">
												<option selected disabled value=''>Elige un técnico:</option>
												<?php $sel=""; foreach($tec as $t) { if(isset($l) && $l->id_tecnico==$t->personalId) $sel="selected"; else $sel="";
													echo "<option ".$sel." value='".$t->personalId."'>".$t->nombre."</option>";
												} ?>
											</select>
										</div>
		                            </div>
		       
	                            	<div class="col-12 form-group d-flex" id="cont_des_consult">
		                                <div class="col-md-3 text-right align-self-center">
											<label>Dirección</label>
										</div>
										<div class="col-lg-9">
	                                        <textarea rows="4" class="form-control" name="direccion"><?php if(isset($l)) echo $l->direccion; ?></textarea>
	                                    </div>
		                            </div>
		                           <div class="col-12 form-group d-flex" id="cont_des_consult2">
		                                <div class="col-md-3 text-right align-self-center">
											<label>Seleccionar color</label>
										</div>
										<div class="col-md-2">
											<input type="color" class="form-control" name="color" value="<?php if(isset($l)) echo $l->color ?>">
										</div>
		                            </div>
		                            <div class="col-12 form-group d-flex" id="cont_des_consult2">
		                            	<div class="col-md-1">
										</div>
										<div class="col-md-6 text-right align-self-center">
			                                <div class="media-body" align="center">
			                                    <span class="img_foto" align="center">
			                                    <?php if(isset($l) && $l->logo!=''){ ?>
			                                        <img id="img_avatar" src="<?php echo base_url(); ?>uploads/consultor/<?php echo $l->logo; ?>" class="rounded mr-3" height="80" width="80">
			                                    <?php }else{ ?>
			                                        <img id="img_avatar" src="<?php echo base_url(); ?>public/img/empresa.png" height="80" width="80">
			                                    <?php } ?>
			                                    </span>
			                                    <div class="col-12 ">
			                                        <br>
			                                        <label class="btn btn-sm bg-light-primary mb-sm-0" for="foto_avatar">Subir logo</label>
			                                        <input type="file" id="foto_avatar" hidden>
			                                        <a class="btn btn-sm bg-light-secondary ml-sm-2" onclick="reset_img()">Reiniciar</a>
			                                    </div>
			                                    <p class="text-muted mb-0 mt-1 mt-sm-0">
			                                        <small>Permitido JPG, JPEG, o PNG. Tamaño máximo de 800kB</small>
			                                    </p>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="col-12 form-group d-flex">
		                                <div class="col-md-3 text-right align-self-center">
											<label>Descripción breve de la operación</label>
										</div>
										<div class="col-md-9">
											<textarea rows="7" class="form-control" type="text" name="descrip_opera" value="<?php if(isset($l)) echo $l->descrip_opera; ?>"><?php if(isset($l)) echo $l->descrip_opera; ?></textarea>
										</div>
		                            </div>
								</div>
								<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p1" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso 1</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>

						<h6>Paso 2</h6>
                        <fieldset>
							<form method="post" id="form_p2">
								<input type="hidden" name="id_operacion" id="id_operacion2" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_regp2" value="<?php if(isset($l2)) echo $l2->id; else echo "0"; ?>">
								<div class="row">	
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center">
											<label>Cuerpo (L)</label>
										</div>
										<div class="col-md-2">
											<input class="form-control" type="number" name="cuerpo" value="<?php if(isset($l2)) echo $l2->cuerpo; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Espesor actual</label>
										</div>
										<div class="col-md-2">
											<input class="form-control" type="number" id="espesor_act" name="espesor_act" value="<?php if(isset($l2)) echo $l2->espesor_act; ?>">
										</div>
										<div class="col-md-2 align-self-center">
											<label>Factor C</label>
										</div>
										<div class="col-md-2">
											<input class="form-control" type="number" id="factorc" name="factorc" value="<?php if(isset($l2)) echo $l2->factorc; ?>">
										</div>

										<div class="col-md-2 text-right align-self-center" style="display: none;">
											<label>Capacidad Vol.</label>
										</div>
										<div class="col-md-2" style="display: none;">
											<input class="form-control" type="number" name="capacidad" value="<?php if(isset($l2)) echo $l2->capacidad; ?>" placeholder="Capacidad Vol. (L)">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center">
											<label>Diametro Ext.</label>
										</div>
										<div class="col-md-2">
											<input class="form-control" type="number" id="d_ext" name="d_ext" value="<?php if(isset($l2)) echo $l2->d_ext; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Diametro Int.</label>
										</div>
										<div class="col-md-2">
											<input class="form-control" type="number" id="d_int" name="d_int" value="<?php if(isset($l2)) echo $l2->d_int; ?>">
										</div>
										
										<div class="col-md-1 text-right align-self-center">
											<label>Equipo</label>
										</div>
										<div class="col-md-2">
											<select id="id_equipo" name="id_equipo" class="form-control">
												<?php $sel=""; foreach ($equi as $e) { if(isset($l2) && $l2->id_equipo==$e->id) $sel="selected"; else $sel="";
													echo "<option ".$sel." value='".$e->id."'>".$e->marca."/".$e->modelo."</option>";
												} ?>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2 form-group"></div>
									<div class="col-md-12 form-group">
										<table class="table-responsive text-center m-4" id="table_tapas" style="font-size: 11px;">
					                        <thead>
					                        	<tr class="table">
					                            	<th style="display:none" class="td_a_tt" colspan="6"></th>
					                                <th style="display:none" class="td_a_tt" colspan="1">Sección Abombada</th>
					                                <th style="display:none" class="td_a_tt" colspan="3">Sección Esférica</th>
					                                <th style="display:none" class="td_a_tt" colspan="2">Sección Cilindrica</th>
					                            </tr>
					                            <tr class="table">
					                            	<th style="display:none" class="td_a_ttorico" colspan="6"></th>
					                                <th style="display:none" class="td_a_ttorico" colspan="3">Sección Abombada</th>
					                                <th style="display:none" class="td_a_ttorico" colspan="3">Sección Esférica</th>
					                                <th style="display:none" class="td_a_ttorico" colspan="2">Sección Cilindrica</th>
					                            </tr>
					                            <tr>
					                                <th nowrap>Tipo de tapa</th>
					                                <th nowrap>Material</th>
					                                <th nowrap>Espesor actual (mm)</th>
					                                <th nowrap>D ext(mm)</th>
					                                <th nowrap>D int(mm)</th>
					                                <th nowrap>L (mm)</th>
					                                <th style="display:none" class="td_a_ta" nowrap>Altura, sección cóncava</th>
					                                <th style="display:none" class="td_a_tc" nowrap>Radio Sup.</th>
					                                <th style="display:none" class="td_a_tc" nowrap>Radio Inf.</th>
					                                <th style="display:none" class="td_a_tc" nowrap>Falda recta</th>
					                                <th style="display:none" class="td_a_tt" nowrap>m</th>
					                                <th style="display:none" class="td_a_tt" nowrap>h</th>
					                                <th style="display:none" class="td_a_tt" nowrap>r<sub>1</sub></th>
					                                <th style="display:none" class="td_a_tt" nowrap>r<sub>2</sub></th>
					                                <th style="display:none" class="td_a_tt" nowrap>Ri</th>
					                                <th style="display:none" class="td_a_tt" nowrap>Pr</th>

					                                <th style="display:none" class="td_a_ttorico" nowrap>Fr</th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>1</sub></th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>2</sub></th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>h</th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>1</sub></th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>2</sub></th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>Ri</th>
					                                <th style="display:none" class="td_a_ttorico" nowrap>Pr</th>
					                                <th nowrap></th>
					                            </tr>
					                        </thead>
					                        <tbody id="t_body">

					                        </tbody>
					                    </table>
									</div>
								</div>
								<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p2" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso 2</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>

						<h6>Paso 3</h6>
                        <fieldset>
							<form method="post" id="form_p3">
								<input type="hidden" name="id_operacion" id="id_operacion3" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_reg3" value="<?php if(isset($l3)) echo $l3->id; else echo "0"; ?>">
								<div class="row">	
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Presión de operación</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="presion" value="<?php if(isset($l3)) echo $l3->presion; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Temperatura de Operación</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="temperatura_op" value="<?php if(isset($l3)) echo $l3->temperatura_op; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-3 text-right align-self-center">
											<label>Presión hidrostática</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="text" name="presion_hidro" value="<?php if(isset($l3)) echo $l3->presion_hidro; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Material</label>
										</div>
										<div class="col-md-3">
											<select id="id_material" name="id_material" class="form-control">
												<?php $sel=""; foreach ($mat as $m) { if(isset($l3) && $l3->id_material==$m->id) $sel="selected"; else $sel="";
													echo "<option ".$sel." value='".$m->id."'>".$m->nominal_composition." - ".$m->type_grande."</option>";
												} ?>
											</select>
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Temperatura diseño</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" id="temp_diseno" name="temp_diseno" value="<?php if(isset($l3)) echo $l3->temp_diseno; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Presón diseño</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="presion_diseno" value="<?php if(isset($l3)) echo $l3->presion_diseno; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Espesor diseño</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="espesor_diseno" value="<?php if(isset($l3)) echo $l3->espesor_diseno; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Esfuerzo de diseño</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" id="esfuerzo_diseno" name="esfuerzo_diseno" value="<?php if(isset($l3)) echo $l3->esfuerzo_diseno; ?>">
										</div>
										
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Esfuerzo de ruptura</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" id="esfuerzo_ruptura" name="esfuerzo_ruptura" value="<?php if(isset($l3)) echo $l3->esfuerzo_ruptura; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Eficiencia de soldadura</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="eficiencia_solda" value="<?php if(isset($l3)) echo $l3->eficiencia_solda; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Factor de seguridad</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="factor_seguridad" value="<?php if(isset($l3)) echo $l3->factor_seguridad; ?>">
										</div>
										<div class="col-md-3 text-right align-self-center">
											<label>Velocidad de corrosíón</label>
										</div>
										<div class="col-md-2">
											<div class="custom-switch custom-control-inline mb-1 mb-xl-0">
			                                    <input type="checkbox" class="custom-control-input" id="corrosion" <?php if(isset($l3) && $l3->corrosion==1) echo "checked"; ?>>
			                                    <label class="custom-control-label mr-1" for="corrosion">
			                                        <span style="color: #ff000000;">.</span>
			                                    </label>
			                                </div>
										</div>
									</div>
								</div>
								<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p3" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso 3</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>

						<h6>Paso 4</h6>
                        <fieldset>
							<form method="post" id="form_p4">
								<input type="hidden" name="id_operacion" id="id_operacion4" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_reg4" value="<?php if(isset($l4)) echo $l4->id; else echo "0"; ?>">
								<div class="row">
									<div class="col-md-12 form-group d-flex text-center">
										<div class="col-md-6">
											<h4>Dispositivo de seguridad</h4>
										</div>
										<div class="col-md-6">
											<h4>Medidor de presión</h4>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-5 text-right align-self-center">
												<label>Tipo</label>
											</div>
											<div class="col-md-7">
												<select id="tipo_disp" name="tipo_disp" class="form-control">
													<option <?php if(isset($l4) && $l4->tipo_disp=="1") echo "selected"; ?> value="1">Válvula de seguridad (gas)</option>
													<option <?php if(isset($l4) && $l4->tipo_disp=="2") echo "selected"; ?> value="2">Válvula de seguridad (vapor)</option>
													<option <?php if(isset($l4) && $l4->tipo_disp=="3") echo "selected"; ?> value="3">Válvula de alivio (liquido)</option>
													<option <?php if(isset($l4) && $l4->tipo_disp=="4") echo "selected"; ?> value="4">Protegido por el sistema</option>
												</select>
											</div>
										</div>
										<div id="cont_justif" style="display: none;">
											<div class="col-md-12 form-group d-flex">
												<div class="col-md-5 text-right align-self-center">
													<label>Justificación técnica</label>
												</div>
												<div class="col-md-7">
													<textarea rows="7" class="form-control" type="text" name="justif_tecnica" value="<?php if(isset($l4)) echo $l4->justif_tecnica; ?>"></textarea>
												</div>
											</div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-5 text-right align-self-center">
												<label>Presión de calibración kg/cm<sup>2</sup></label>
											</div>
											<div class="col-md-7">
												<input class="form-control" type="number" name="pesion_calibra" value="<?php if(isset($l4)) echo $l4->pesion_calibra; ?>">
											</div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-5 text-right align-self-center">
												<label>Diametro de desfogue (mm)</label>
											</div>
											<div class="col-md-7">
												<input class="form-control" type="number" name="diametro_desfogue" value="<?php if(isset($l4)) echo $l4->diametro_desfogue; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-5 text-right align-self-center">
												<label>Tipo</label>
											</div>
											<div class="col-md-7">
												<select id="tipo_medidor" name="tipo_medidor" class="form-control">
													<option <?php if(isset($l4) && $l4->tipo_medidor=="1") echo "selected"; ?> value="1">Manómetro caratula</option>
													<option <?php if(isset($l4) && $l4->tipo_medidor=="2") echo "selected"; ?> value="2">Digital</option>
													<option <?php if(isset($l4) && $l4->tipo_medidor=="3") echo "selected"; ?> value="3">Sin medidor</option>
												</select>
											</div>
										</div>
										<div class="col-md-12 form-group d-flex" id="cont_rango">
											<div class="col-md-5 text-right align-self-center">
												<label>Rango kg/cm<sup>2</sup></label>
											</div>
											<div class="col-md-7">
												<input class="form-control" type="number" name="rango" id="rango" value="<?php if(isset($l4)) echo $l4->rango; ?>">
											</div>
										</div>
									</div>
								</div>

								<div class="row" id="cont_calc_area">
									<div class="col-md-12 form-group d-flex text-center">
										<div class="col-md-6">
											<br><br>
											<h4>Cálculo de área requerida</h4>
										</div>
									</div>	
									<div class="col-md-12 form-group d-flex t1 t2 t3">
										<div class="col-2 text-right align-self-center">
											<label>Capacidad (Wa o Va)</label>
										</div>
										<div class="col-md-4">
											<input class="form-control calc_area" id="capacidad_wa" type="number" name="capacidad" value="<?php if(isset($l4)) echo $l4->capacidad; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Presión absoluta kg/cm<sup>2</sup></label>
										</div>
										<div class="col-md-4">
											<input class="form-control calc_area" type="number" name="presion_absoluta" value="<?php if(isset($l4)) echo $l4->presion_absoluta; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex t1 t2 t3">
										<div class="col-md-2 text-right align-self-center">
											<label>Constante C para aire en SI:</label>
										</div>
										<div class="col-md-4">
											<input class="form-control calc_area" type="number" name="constante_rot" value="<?php if(isset($l4)) echo $l4->constante_rot; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Presión regulada kg/cm<sup>2</sup></label>
										</div>
										<div class="col-md-4">
											<input class="form-control" type="number" name="presion_regulada" value="<?php if(isset($l4)) echo $l4->presion_regulada; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center t1">
											<label>Temperatura absoluta a la entrada(Ta)</label>
										</div>
										<div class="col-md-4 t1">
											<input class="form-control calc_area" type="number" name="temp_absoluta" value="<?php if(isset($l4)) echo $l4->temp_absoluta; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center t1 t2 t3">
											<label>Presión atmosférica kg/cm<sup>2</sup></label>
										</div>
										<div class="col-md-4 t1 t2 t3">
											<input class="form-control" type="number" name="presion_atmo" value="<?php if(isset($l4)) echo $l4->presion_atmo; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center t1">
											<label>Peso molecular (M)</label>
										</div>
										<div class="col-md-4 t1">
											<input class="form-control calc_area" type="number" name="peso_molecular" value="<?php if(isset($l4)) echo $l4->peso_molecular; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center t1 t2 t3">
											<label>Diámetro de valvula instalada (mm)</label>
										</div>
										<div class="col-md-4 t1 t2 t3">
											<input class="form-control" type="number" name="diametro_valvula" value="<?php if(isset($l4)) echo $l4->diametro_valvula; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center t1 t2 t3">
											<label>Coeficiente de descarga (Kd)</label>
										</div>
										<div class="col-md-4 t1 t2 t3">
											<input class="form-control calc_area" type="number" name="coeficiente_desc" value="<?php if(isset($l4)) echo $l4->coeficiente_desc; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Factor de compresibilidad (z)</label>
										</div>
										<div class="col-md-4">
											<input class="form-control calc_area" type="number" name="factor_compre" value="<?php if(isset($l4)) echo $l4->factor_compre; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center">
											<label>Factor líquido (Kp)</label>
										</div>
										<div class="col-md-4">
											<input class="form-control" type="number" name="factor_liq" value="<?php if(isset($l4)) echo $l4->factor_liq; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Factor (kb)</label>
										</div>
										<div class="col-md-4">
											<input class="form-control" type="number" name="factor" value="<?php if(isset($l4)) echo $l4->factor; ?>">
										</div>
									</div>
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-2 text-right align-self-center t3">
											<label>Densidad del fluido</label>
										</div>
										<div class="col-md-4 t3">
											<input class="form-control" type="number" name="densidad_flui" value="<?php if(isset($l4)) echo $l4->densidad_flui; ?>">
										</div>
										<div class="col-md-2 text-right align-self-center">
											<label>Área total de desfogue mm<sup>2</sup></label>
										</div>
										<div class="col-md-4">
											<input class="form-control" type="number" name="area_total" value="<?php if(isset($l4)) echo $l4->area_total; ?>">
										</div>
									</div>
								</div>

								<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p4" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso 4</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>

						<h6>Paso 5</h6>
                        <fieldset>
							<form role="form" method="post" id="form_p5">
								<input type="hidden" name="id_operacion" id="id_operacion5" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_reg5" value="<?php if(isset($l5)) echo $l5->id; else echo "0"; ?>">
								<div class="row">
									<div class="col-md-12 form-group d-flex text-center">
										<div class="col-md-12">
											<h4>Inspección Volumetrica</h4><hr>
										</div>									</div>
									<div class="col-md-12 form-group d-flex text-center">
										<div class="col-md-12">
											<p>Medición de espesores (UT)</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-2 text-right align-self-center">
												<label>Equipo</label>
											</div>
											<div class="col-md-4">
												<!-- poder elegir un vacio, significa que no realziaron esa lectura -->
												<select id="id_equipo" name="id_equipo" class="form-control">
													<?php $sel=""; foreach ($equi as $e) { if(isset($l5) && $l5->id_equipo==$e->id) $sel="selected"; else $sel="";
														echo "<option ".$sel." value='".$e->id."'>".$e->marca."/".$e->modelo."</option>";
													} ?>
											</select>
											</div>
										
											<div class="col-md-2 text-right align-self-center">
												<label>Cuerpo</label>
											</div>
											<div class="col-md-2">
												<input readonly class="form-control" type="number" id="cuerpo" value="<?php if(isset($l2)) echo $l2->cuerpo; ?>">
											</div>
											<!--<div class="col-md-2 text-right align-self-center">
												<label>Espesor máximo</label>
											</div>
											<div class="col-md-2">
												<input class="form-control" type="number" id="espesor_max_cpo" value="<?php if(isset($l2)) echo $l2->espesor_max; ?>">
											</div>
											<div class="col-md-2 text-right align-self-center">
												<label>No de Puntos</label>
											</div>
											<div class="col-md-2">
												<input class="form-control" type="number" id="num_puntos_cpo" value="<?php if(isset($l2)) echo $l2->num_puntos; ?>">
											</div>-->

										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-3 text-right align-self-center">
												<label>Niveles del envolvente</label>
											</div>
											<div class="col-md-3">
												<input class="form-control" type="number" name="niveles" id="niveles" value="<?php if(isset($l5)) echo $l5->niveles; ?>">
											</div>
									
											<div class="col-md-3 text-right align-self-center">
												<label>Lecturas realizadas por nivel</label>
											</div>
											<div class="col-md-3">
												<input class="form-control" type="number" name="lecturas_nivel" id="lecturas_nivel" value="<?php if(isset($l5)) echo $l5->lecturas_nivel; ?>">
											</div>
										</div>
										<div class="row">
											<div class="col-md-12"><br><br></div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<table class=" text-center m-2" id="table_tapas_lev" width="100%">
						                        <thead>
						                            <tr>
						                            	<th nowrap>Tapa</th>
						                                <th nowrap>Tipo</th>
						                                <th nowrap>Espesor actual (mm)</th>
						                                <th nowrap>D ext (mm)</th>
						                                <th nowrap>D int (mm)</th>
						                                <th nowrap>L (mm)</th>
						                                <!--<th>Espesor máximo</th>
						                                <th>No de Puntos</th>-->
						                                <th nowrap style="display:none" class="td_a_ta">Altura, sección cóncava</th>
						                                <th nowrap style="display:none" class="td_a_tc">Radio Sup.</th>
						                                <th nowrap style="display:none" class="td_a_tc">Radio Inf.</th>
						                                <th nowrap style="display:none" class="td_a_tc">Falda recta</th>
						                                <th nowrap style="display:none" class="td_a_tt" >m</th>
						                                <th nowrap style="display:none" class="td_a_tt" >h</th>
						                                <th nowrap style="display:none" class="td_a_tt" >r<sub>1</sub></th>
						                                <th nowrap style="display:none" class="td_a_tt" >r<sub>2</sub></th>
						                                <th nowrap style="display:none" class="td_a_tt" >Ri</th>
						                                <th nowrap style="display:none" class="td_a_tt" >Pr</th>

						                                <th style="display:none" class="td_a_ttorico" nowrap>Fr</th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>1</sub></th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>2</sub></th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>h</th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>1</sub></th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>r<sub>2</sub></th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>Ri</th>
						                                <th style="display:none" class="td_a_ttorico" nowrap>Pr</th>
						                            </tr>
						                        </thead>
						                        <tbody id="t_body_lev">

						                        </tbody>
						                    </table>
										</div>
										<div class="row">
											<div class="col-md-2 form-group"></div>
											<div class="col-md-12 form-group">
												<div class="col-md-12 text-center">
													<h4>Lectura de espesores en mm</h4><hr>
												</div>
												<table class=" text-center m-1" id="table_esp_taps" width="100%">
							                        <thead>
							                            <tr>
							                                <th>#</th>
							                                <th id="th_tap1">Tapa 1</th>
							                                <th id="th_tap2">Tapa 2</th>
							                                <th>Cuerpo</th>
							                                <th></th>
							                            </tr>
							                        </thead>
							                        <tbody id="t_body_esp">

							                        </tbody>
							                    </table>
											</div>
											<div class="col-md-2 form-group"></div>
										</div>

										<div class="col-md-12 form-group d-flex">
											<div class="col-md-6">
												<h4>Foto de croquis</h4><hr>
												<input class="form-control" type="hidden" id="file_aux" value="<?php if(isset($l5)) echo $l5->img_croquis; ?>">
												<input class="form-control" type="file" name="file" id="file">
											</div>
										
											<div class="col-md-6">
												<h4>Foto de placa de datos</h4><hr>
												<input class="form-control" type="hidden" id="file_aux2" value="<?php if(isset($l5)) echo $l5->file_placa; ?>">
												<input class="form-control" type="file" name="file_placa" id="file_placa">
											</div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-6">
												<h4>Foto del equipo</h4><hr>
												<input class="form-control" type="hidden" id="file_aux_equipo" value="<?php if(isset($l5)) echo $l5->img_equipo; ?>">
												<input class="form-control" type="file" name="img_equipo" id="img_equipo">
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<br><br><hr>
									</div>

									<div class="col-md-12 form-group">
										<div class="col-md-12 form-group d-flex text-center">
											<div class="col-md-12">
												<h4>Inspección Superficial</h4><hr>
											</div>
										</div>
										<div class="col-md-12 form-group d-flex text-center">
											<div class="col-md-12">
												<p>Partículas magnéticas (MT)</p>
											</div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-4 text-right align-self-center">
												<label>Equipo</label>
											</div>
											<div class="col-md-5">
												<select id="id_equipo2" name="id_equipo2" class="form-control">
													<!-- poder elegir un vacio, significa que no realziaron esa lectura -->
													<?php $sel=""; foreach ($equi as $e) { if(isset($l5) && $l5->id_equipo2==$e->id) $sel="selected"; else $sel="";
														echo "<option ".$sel." value='".$e->id."'>".$e->marca."/".$e->modelo."</option>";
													} ?>
											</select>
											</div>
										</div>
										<div class="col-md-12 form-group d-flex">
											<div class="col-md-6">
												<h4>Evidencia Fotográfica</h4><hr>
												<input class="form-control" type="file" name="files" id="files" multiple="false">
											</div>
											<div class="col-md-6">
												<h4>Foto de croquis</h4><hr>
												<input class="form-control" type="hidden" id="file_aux_croq2" value="<?php if(isset($l5)) echo $l5->img_croquis_mt; ?>">
												<input class="form-control" type="file" name="file_croq2" id="file_croq2">
											</div>
										</div>
										<div class="controls" id="cont_imgs">
			                                <div class="col-md-6">
			                                    
			                                </div>
			                            </div>
									</div>
								</div>
								<div class="row">
                                    <div class="col-md-12"><br></div>
                                    <div class="col-md-12" style="text-align: end;">
                                        <button type="button" id="btn_save_p5" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar Paso 5</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><br><br></div>
                                </div>
							</form>
						</fieldset>

						<h6>Paso 6</h6>
                        <fieldset>
							<form method="post" id="form_p6">
								<input type="hidden" name="id_operacion" id="id_operacion6" value="<?php echo $id_operacion; ?>">
								<input type="hidden" name="id" id="id_reg6" value="<?php if(isset($l6)) echo $l6->id; else echo "0"; ?>">
								<div class="row">
									<div class="col-md-12 form-group"></div>
									<div class="col-md-12 form-group">
										<table class="table text-center m-0" id="table_equi_bloque">
					                        <thead>
					                            <tr>
					                            	<th></th>
					                                <th>EQUIPO</th>
					                                <th>SERIE</th>
					                                <th>CERTIFICADOS</th>
					                                <th>PASOS</th>
					                                <th>TIPO</th>
					                                <th>RANGO DE MEDIDA</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                        	<?php $cont_eb=0; foreach ($eq_blo as $eb) {
					                        		$cont_eb++;
					                        		if(isset($l6) && $l6->id_equipo_bloque==$eb->id) $chk="checked";
					                        		else $chk="";
					                        		echo '<tr>
								                        <td>
								                        	<div class="custom-switch_radio custom-control-inline mb-1 mb-xl-0">
							                                    <input type="radio" class="custom-control-radio" name="tipo_equi" id="tipo_equi'.$cont_eb.'" '.$chk.'>
							                                </div>
						                            	</td>
						                                <td>'.$eb->equipo.'</td>
						                                <td>'.$eb->serie.'</td>
						                                <td>'.$eb->certificado.'</td>
						                                <td>'.$eb->pasos.'</td>
						                                <td>'.$eb->tipo.'</td>
						                                <td>'.$eb->rango_medida.'</td>
						                            </tr>';
					                        	} ?>
					                        </tbody>
					                       </table>	
									<div class="col-md-12 form-group"><br></div>
									<div class="col-md-12 form-group">
										<br>
										<table class="table text-center m-0" id="table__res_tapas">
					                        <thead>
					                            <tr>
					                                <th></th>
					                                <th>ESPESOR MÍNIMO REQUERIDO (mm)</th>
					                                <th>PRESIÓN INTERIOR MÁXIMA PERMISIBLE kg/cm<sup>2</sup></th>
					                                <th>VOLUMEN (L)</th>
					                            </tr>
					                        </thead>
					                        <tbody id="t_body_res">
					                        	<?php $emr=0; $pimp=0; $volumen=0; $name_tapa=""; $vol_total=0;

					                        		$gp3=$this->ModeloGeneral->getselectwhererow2('levantamiento_paso3',array('estatus'=>1,'id_operacion'=>$id_operacion));

					                        		$tap=$this->ModeloGeneral->getselectwhere_n_consulta('tapas_paso2',array('estatus'=>1,'id_operacion'=>$id_operacion));
					                        		foreach($tap as $t){
					                        			if($t->tipo_tapa=="1"){ //elipsoidal -- ok
													        $volumen=(((3.1416/24)*pow($t->d_int,3))+3.1416*pow($t->d_int/2,2)*$t->longitud)/pow(10,6);
													        $vol_total=$vol_total+$volumen;
													        $emr=($gp3->presion_diseno*$t->d_int)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
													        $pimp=(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->d_int+0.2*$t->espesor);
													    }else if($t->tipo_tapa=="2"){ //hemisferica -- ok
													        $volumen=round( (2/3)*3.1416* pow(($t->d_int/2),2) / pow(10,6),2);
													        $vol_total=$vol_total+$volumen;
													        $emr=($gp3->presion_diseno*$t->d_int)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
													        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->d_int+0.2*$t->espesor),2);
													    }else if($t->tipo_tapa=="3"){ //toriesfericas 100/6 --ok
													        $vol_abomb=round(3.1416 * pow($t->alt_concava,2)*($t->longitud-($t->alt_concava/3)) / pow(10,6),2);
													        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
													        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
													        $vol_total=$vol_total+$vol_abomb+$vol_esferi+$vol_cilin;
													        $volumen=$vol_abomb+$vol_esferi+$vol_cilin;

													        $emr=round(0.885*$gp3->presion_diseno*$t->longitud/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda)-(0.1*$gp3->presion_diseno),2);
													        $pimp=round($gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor/(0.885*$t->longitud)+(0.1*$t->espesor),2);
													    }else if($t->tipo_tapa=="4"){ //toriesfericas --ok
													        $vol_abomb=round(3.1416 * pow($t->alt_concava,2)*($t->longitud-($t->alt_concava/3)) / pow(10,6),2);
													        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
													        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
													        $vol_total=$vol_total+$vol_abomb+$vol_esferi+$vol_cilin;
													        $volumen=$vol_abomb+$vol_esferi+$vol_cilin;

													        $emr=round(($gp3->presion_diseno*$t->longitud*$l4->peso_molecular)/(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda)-(0.2*$gp3->presion_diseno),2);
													        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor)/($t->longitud*$l4->factor)+(0.2*$t->espesor),2);
													    }else if($t->tipo_tapa=="5"){ //Plana -- ok
													        $volumen=0;
													        $vol_total=$vol_total+$volumen;
													        $emr=round($t->d_int * sqrt(($l2->factorc*$gp3->presion_diseno)/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda)),2);
													        $pimp=round(pow($t->espesor,2)*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda/($l2->factorc*pow($t->d_int,2)) ,2);
													    }else if($t->tipo_tapa=="6"){ //Plana circular apernada 
													        $volumen=round((3.1416/37)*pow($t->d_int,3) + (3.1416*pow(($t->d_int/2),2)*$t->longitud) / pow(10,6),2);
													        $vol_total=$vol_total+$volumen;
													        $emr=round(($t->d_int * sqrt($l4->factor_compre*$gp3->presion_diseno))/($gp3->esfuerzo_diseno*$gp3->eficiencia_solda),2);
													        $pimp=round(pow($t->espesor,2)*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda/$l4->factor_compre*pow($t->d_int,2),2);
													    }else if($t->tipo_tapa=="7"){ //abombada -- volumen formula ajustada con excel de formulas 
													        $volumen=round( 3.1416 * pow($t->alt_concava,2) * ($t->d_int - $t->alt_concava/3) / pow(10,6),2);
													        $vol_total=$vol_total+$volumen;
													        $emr=round((5*$gp3->presion_diseno*$t->d_int)/(6*$gp3->esfuerzo_diseno),2); //pendientes por realizar- no existen en excel
													        $pimp=round((6*$gp3->esfuerzo_diseno*$t->espesor)/(5*$t->d_int),2); //pendientes por realizar- no existen en excel
													    }else if($t->tipo_tapa=="8"){ //volumen formula ajustada con excel de formulas 
													        $volumen=round(1/3 * 3.1416 * $t->falda_recta*( pow($t->radio_sup,2) + pow($t->radio_inf,2) + ($t->radio_sup * $t->radio_inf)) / pow(10,6),2);
													        $vol_total=$vol_total+$volumen;
													        $c1=$t->falda_recta;
													        $c2=$t->radio_sup/2;
													        $c1 = pow($c1,2);
													        $c2 = pow($c2,2);
													        $h=$c1+$c2;
													        $h=round(sqrt($h),2);
													        $angulo = $t->falda_recta/$h;
													        //log_message('error', "angulo: ".$angulo);
													        $ang = asin($angulo);
													        $angulo2 = round(acos($angulo)/pi()*180,2);

													        $emr=round(($gp3->presion_diseno*$t->d_int) / (2 * cos($angulo2) *(($gp3->esfuerzo_diseno*$gp3->eficiencia_solda - 0.6 * $gp3->presion_diseno))),2); //pendientes por validar
													        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor * cos($angulo2))/($t->d_int+1.2*$t->espesor * cos($angulo2)),2); //pendientes por valida
													    }
													    else if($t->tipo_tapa=="9"){ //toriconicas, faltan formulas de espesor y presion
													        $vol_coni=round(1/3 * 3.1416 * $t->falda_recta*( pow($t->radio_sup,2) + pow($t->radio_inf,2) + ($t->radio_sup * $t->radio_inf)) / pow(10,6),2);
													        $vol_esferi=round((1/6)*(3.1416)*($t->altura_esferica) *(3*pow($t->radio_interior,2)+3*pow($t->radio_esferica,2)+$t->altura_esferica) / pow(10,6),2);
													        $vol_cilin=round(3.1416*pow($t->radio_interno,2)*$t->parte_recta / pow(10,6),2);
													        $vol_total=$vol_total+$vol_coni+$vol_esferi+$vol_cilin;
													        $volumen=$vol_coni+$vol_esferi+$vol_cilin;

													        $c1=$t->falda_recta;
													        $c2=$t->radio_sup/2;
													        $c1 = pow($c1,2);
													        $c2 = pow($c2,2);
													        $h=$c1+$c2;
													        $h=round(sqrt($h),2);
													        $angulo = $t->falda_recta/$h;
													        //log_message('error', "angulo: ".$angulo);
													        $ang = asin($angulo);
													        $angulo2 = round(acos($angulo)/pi()*180,2);
													        $emr=round(($gp3->presion_diseno*$t->d_int) / (2 * cos($angulo2) *(($gp3->esfuerzo_diseno*$gp3->eficiencia_solda - 0.6 * $gp3->presion_diseno))),2); //pendientes por validar
													        $pimp=round((2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$t->espesor * cos($angulo2))/($t->d_int+1.2*$t->espesor * cos($angulo2)),2); 
													    }
					                        			
					                        			if($t->tipo_tapa=="1"){ $name_tapa="Elipsoidal"; }
					                        			if($t->tipo_tapa=="2"){ $name_tapa="Hemisferica"; }
					                        			if($t->tipo_tapa=="3"){ $name_tapa="Toriesférica 100/6"; }
					                        			if($t->tipo_tapa=="4"){ $name_tapa="Toriesférica"; }
					                        			if($t->tipo_tapa=="5"){ $name_tapa="Plana"; }
					                        			if($t->tipo_tapa=="6"){ $name_tapa="Plana circular apernada"; }
					                        			if($t->tipo_tapa=="7"){ $name_tapa="Abombada"; }
					                        			if($t->tipo_tapa=="8"){ $name_tapa="Conica"; }
					                        			if($t->tipo_tapa=="9"){ $name_tapa="Toriconica"; }
					                        			echo "<tr>
					                        					<td>".$name_tapa."</td>
					                        					<td>".round($emr,2)."</td>
					                        					<td>".round($pimp,2)."</td>
					                        					<td>".round($volumen,2)."</td>
					                        				</tr>";
					                        		}
					                        		$volumen_env=0; $emr_env=0; $pimp_env=0;
					                        		$gp2=$this->ModeloGeneral->getselectwhere_n_consulta2('levantamiento_paso2',array('estatus'=>1,'id_operacion'=>$id_operacion));

					                        		if($gp2->num_rows()>0){
					                        			$gp2=$gp2->row();
					                        			if($l->tipo_env=="1"){ //cilindrico
													        $volumen_env=(3.1416*pow($gp2->d_int/2,2)*$gp2->cuerpo)/pow(10,6); //--ok
													        $emr_env=$gp3->presion_diseno*($gp2->d_int/2) / ($gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.6*$gp3->presion_diseno);
													        $pimp_env=$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$gp2->espesor_act/(($gp2->d_int/2)+0.6*$gp2->espesor_act);
													    }else{  //esferico
													        $volumen_env=(3.1416*pow($gp2->d_int/2,2)*$gp2->cuerpo)/pow(10,6);
													        $emr_env=$gp3->presion_diseno*($gp2->d_int/2) / (2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda-0.2*$gp3->presion_diseno);
													        $pimp_env=(2*$gp3->esfuerzo_diseno*$gp3->eficiencia_solda*$gp2->espesor_act)/(($gp2->d_int/2)+0.2*$gp2->espesor_act);
													    }
					                        		}
					                        		echo "<tr>
			                        					<td>Cuerpo</td>
			                        					<td>".round($emr_env,2)."</td>
			                        					<td>".round($pimp_env,2)."</td>
			                        					<td>".round($volumen_env,2)."</td>
			                        				</tr>";
					                        	?>
					                        </tbody>
					                    </table>
									</div>
								</div>
								<div class="row col-md-12">	
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Volumen Total</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="vol_total" id="vol_total" value="<?php if(isset($l6)) echo $l6->vol_total; else echo round($vol_total,2); ?>">
										</div>
								
										<div class="col-md-3 text-right align-self-center">
											<label>PMTP</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="hidden" id="pmtp_aux" value="<?php if(isset($l6)) echo $l6->pmtp; else echo "0"; ?>">
											<input class="form-control" type="number" name="pmtp" id="pmtp" value="<?php if(isset($l6)) echo $l6->pmtp; ?>">
										</div>
									</div>
								</div>
								<div class="row col-md-12">	
									<div class="col-md-12 form-group d-flex">
										<div class="col-md-3">
											<label>Categoría del RSP</label>
										</div>
										<div class="col-md-3">
											<!--<input class="form-control" type="text" name="categoria_rsp" id="categoria_rsp" value="<?php if(isset($l6)) echo $l6->categoria_rsp; ?>">-->
											<select id="categoria_rsp" name="categoria_rsp" class="form-control" onchange="contSTPS()">
												<option <?php if(isset($l) && $l6->categoria_rsp=="I") echo "selected"; ?> value='I'>I</option>
												<option <?php if(isset($l) && $l6->categoria_rsp=="II") echo "selected"; ?> value='II'>II</option>
												<option <?php if(isset($l) && $l6->categoria_rsp=="III") echo "selected"; ?> value='III'>III</option>
											</select>
										</div>
								
										<div class="row col-md-12" id="cont_area">
											<div class="col-md-3">
												<label>Área mínima requerida</label>
											</div>
											<div class="col-md-3">
												<input class="form-control" type="number" name="area_minima" id="area_minima" value="<?php if(isset($l6)) echo $l6->area_minima; ?>">
											</div>
										</div>
									</div>								
									<!--<div class="col-md-12 form-group d-flex">
										<div class="col-md-3 text-right align-self-center">
											<label>Niveles del envolvente</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="niveles" id="niveles" value="<?php if(isset($l6)) echo $l6->niveles; ?>">
										</div>
								
										<div class="col-md-3 text-right align-self-center">
											<label>Lecturas realizadas por nivel</label>
										</div>
										<div class="col-md-3">
											<input class="form-control" type="number" name="lecturas_nivel" id="lecturas_nivel" value="<?php if(isset($l6)) echo $l6->lecturas_nivel; ?>">
										</div>
									</div>-->

								</div>
								<div class="row col-md-12">	
									<div class="col-md-12" id="cont_stps" style="display: none;">
										<div class="form-group d-flex">
											<div class="col-md-3">
												<label>No. de STPS:</label>
											</div>
											<div class="col-md-9">
												<input class="form-control" type="text" name="num_stps" id="num_stps" value="<?php if(isset($l6) && $l6->num_stps!=""){echo $l6->num_stps; }else{ echo "VER DOCUMENTO CORRESPONDIENTE"; } ?>">
											</div> 
										</div>
									</div>
								</div>
								
							</form>
							<div class="row">
	                            <div class="col-md-12"><br><br><br><br><br></div>
	                            <div class="col-md-12">
	                                <div class="col-md-6">
	                                </div>
	                                <div class="col-md-12" style="text-align: end;">
	                                    <button type="button" id="calcula_btn" class="btn btn-sistema mr-sm-4 mb-3 btn_registro"><i class="fa fa-calculator"></i> Calcular</button>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12"><br><br></div>
	                        </div>
						</fieldset>
						
					</div>
					<hr>
					<div class="row">
                        <div class="col-lg-12 equipo_btn_text">
                            
                            <a href="<?php echo base_url()?>Operaciones/recipientes" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
				</div>
			</div>
		</div>
	</div>
</div>