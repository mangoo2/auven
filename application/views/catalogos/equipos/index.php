<div class="row">
    <div class="col-12">
        <div class="content-header">Listado de equipos</div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body table-responsive">
                    <div class="row">
                        <div class="col-2">
                            <a href="<?php echo base_url(); ?>Equipo/registrar/" class="btn btn-sistema mr-sm-2 mb-3"><i class="fa fa-newspaper-o"></i> &nbsp; Nuevo equipo</a>
                        </div>
                        <div class="col-3">
                            <select id="tipo_equipo" class="form-control" onchange="reload_registro()">
                                <option value="0" selected="">Selecciona tipo de equipo </option>
                                <option value="0">Todos</option>
                                <option value="1">Equipo MT</option>
                                <option value="2">Equipo UT</option>
                            </select>
                        </div>
                    </div>
                    <table class="table text-center m-0" id="table_data">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tipo de equipo</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Serie</th>
                                <th>No. Certificado</th>
                                <th>Fecha Cal</th>
                                <th>Vigencia Cal</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>