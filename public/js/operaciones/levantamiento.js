var base_url = $('#base_url').val();
$(document).ready(function($) {
    wizard=$(".icons-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="step">#index#</span> #title#',
        enableAllSteps: true,
        labels: {
          next: '<button type="button" class="btn btn-sistema">Siguiente <i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
          previous: '<button type="button" class="btn btn-sistema"><i class="fa fa-arrow-left" aria-hidden="true"></i> Anterior</button>',
          finish: '<button id="savef" type="button" onclick="finalizarProceso()" class="btn btn-sistema"><i class="fa fa-save" aria-hidden="true"></i> Finalizar</button>'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            return true; 
        },
        onFinished: function (event, currentIndex,newIndex) {
            return true;
        }
    });
    $("#btn_save_p1").on("click",function(){
        saveLevantamiento(0);
    });
    $('#foto_avatar').change(function(){
         document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
    });
    setNameTap();
    $("#orientacion").on("change",function(){
        setNameTap();
    });
    /* ****************************** */
    $("#btn_save_p2").on("click",function(){
        saveLevantamiento2(0);
    });

    if($("#id_regp2").val()!=0){
        table_data_tapas($("#id_operacion").val(),0);
    }else{
        tapa_add();
    }
    $("#espesor_act").on("change",function(){
        calculaDiametro(0);
    }); 
    $("#d_int").on("change",function(){
        calculaDiametro(1);
    }); 
    $("#d_ext").on("change",function(){
        calculaDiametro(2);
    }); 

    /* ****************************** */
    $("#btn_save_p3").on("click",function(){
        saveLevantamiento3(0);
    });
    $("#id_material,#temp_diseno").on("change",function(){
        getDetalles();
    });

    /* ****************************** */
    $("#btn_save_p4").on("click",function(){
        saveLevantamiento4(0);
    });
    $("#icons-tab-steps-t-4").on("click",function(){
        table_data_tapas($("#id_operacion").val(),1);
    });
    $("#tipo_disp").on("change",function(){
        tipoDispositivo(this.value);
        datosCalculo();
    });
    tipoDispositivo($("#tipo_disp option:selected").val());
    $(".calc_area").on("click",function(){
        calculaArea();
    });

    $("#tipo_medidor").on("change",function(){
        tipoMedidor(this.value);
    });
    calculaArea();
    tipoMedidor($("#tipo_medidor option:selected").val());

    datosCalculo();
    /* ****************************** */
    $("#icons-tab-steps-t-4").on("click",function(){
        table_esp_tapas($("#id_operacion").val(),0);
        if($("input[id*='cuerpo']").val()==""){
            var cpo = $("input[name*='cuerpo']").val();
            $("input[id*='cuerpo']").val(cpo);
        }
    });
    $("#btn_save_p5").on("click",function(){
        saveLevantamiento5(0);
    });
    if($("#id_reg5").val()!="0"){
        getFotosL5();
    }
    $("#niveles,#lecturas_nivel").on("change",function(){
        generarTablaLectura();
    });
        
    var id_reg5=$("#id_reg5").val();
    var preview ="";
    if($("#file_aux").val()!=""){
        preview=''+base_url+"uploads/croquis/"+$("#file_aux").val()+'';
    }
    $("#file").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Operaciones/cargaimagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+preview+'',    
        ],
        initialPreviewAsData: "false",
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type:"image", url: ""+base_url+"Operaciones/deleteImgP5/"+id_reg5+"/1", caption: $("#file_aux").val(), key:2}
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"file",
                    id_operacion:$("#id_operacion").val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    var previewp ="";
    if($("#file_aux2").val()!=""){
        previewp=''+base_url+"uploads/placas/"+$("#file_aux2").val()+'';
    }
    $("#file_placa").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Operaciones/cargaimagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+previewp+'',    
        ],
        initialPreviewAsData: "false",
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type:"image", url: ""+base_url+"Operaciones/deleteImgP5/"+id_reg5+"/2", caption: $("#file_aux2").val(), key:2}
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"file_placa",
                    id_operacion:$("#id_operacion").val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });

    var previewe ="";
    if($("#file_aux_equipo").val()!=""){
        previewe=''+base_url+"uploads/equipos_cliente/"+$("#file_aux_equipo").val()+'';
    }
    $("#img_equipo").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Operaciones/cargaimagen',
        maxFilePreviewSize: 8000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+previewe+'',    
        ],
        initialPreviewAsData: "false",
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type:"image", url: ""+base_url+"Operaciones/deleteImgP5/"+id_reg5+"/4", caption: $("#file_aux_equipo").val(), key:3}
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
            var info = {   
                        input_name:"img_equipo",
                        id_operacion:$("#id_operacion").val()
                    };
            return info;
        }
    });

    $("#files").fileinput({
        showCaption: false,
        showUpload: false,
        //maxFileCount: 6,
        language:'es',
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Operaciones/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    input_name:"files",
                    id_operacion:$("#id_operacion").val()
                };
        return info;
      }
    }).on('fileuploaded', function(event, files, extra) {
        getFotosL5();
        //console.log("cargar realizada");
        $("#files").fileinput("clear");
    }).on('filedeleted', function(event, files, extra) {
        //console.log("eliminacion realizada");
        $(".row_1").remove();
        getFotosL5();
    });
    var previewc ="";
    if($("#file_aux_croq2").val()!=""){
        previewc=''+base_url+"uploads/croquis/"+$("#file_aux_croq2").val()+'';
    }
    $("#file_croq2").fileinput({
        showCaption: false,
        showUpload: false,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen',
        uploadUrl: base_url+'Operaciones/cargaimagen',
        maxFilePreviewSize: 5000,
        elErrorContainer: '#kv-avatar-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        initialPreview: [
            ''+previewc+'',    
        ],
        initialPreviewAsData: "false",
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: [
            {type:"image", url: ""+base_url+"Operaciones/deleteImgP5/"+id_reg5+"/3", caption: $("#file_aux_croq2").val(), key:4}
        ],
        previewFileIconSettings: {
            'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>'
        },
        uploadExtraData: function (previewId, index) {
        var info = {   
                    input_name:"file_croq2",
                    id_operacion:$("#id_operacion").val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    $(".file-drop-zone-title").html("Arrastra aquí o selecciona imagen");

    $("#icons-tab-steps-t-5").on("click",function(){ //click al paso 6
        calcular_resultados();
        selectCatego();
    });

    /* ****************************/
    $("#calcula_btn").on("click",function(){
        calcular_resultados();
        calculaArea();
    });
    selectCatego();

    /*$("#categoria_rsp").on("change",function(){
        if(this.val=="I" || this.val=="II"){
            $("#cont_stps").hide("slow");
        }else{
            $("#cont_stps").show("slow");
        }
    });*/
});

function contSTPS(){
    if($("#categoria_rsp option:selected").val()=="I" || $("#categoria_rsp option:selected").val()=="II"){
        $("#cont_stps").hide("slow");
        $("#num_stps").val("NO APLICA");
    }else{
        $("#cont_stps").show("slow");
        $("#num_stps").val("VER DOCUMENTO CORRESPONDIENTE");
    }
}

function selectCatego(){ //acá me quedo para poner la categoria automaticamente en paso 6 dependiendo fluido peligroso y volumen
    var capacidad=$("input[name*='capacidad']").val();
    var capvol=capacidad/1000;
    if($("#tipo_disp option:selected").val()!="4"){
        var presion_regulada=parseFloat($("input[name*='presion_regulada']").val());
    }else{ //protegido por el sistema
        var presion_regulada=parseFloat($("input[name*='presion']").val());
        if(presion_regulada<=20){
            presion_regulada=presion_regulada+2;
        }else if(presion_regulada>20){
            presion_regulada=presion_regulada*1.10;
        }
    }
    //console.log("presion_regulada: "+presion_regulada);
    var val_Calibra = parseFloat(presion_regulada*98.0665).toFixed(2);
    var peligroso=$("#id_fluido option:selected").data("peligro");
    //console.log("val_Calibra: "+val_Calibra);

    if(peligroso==2 && capvol<=0.5){ //no peligroso
        $("#categoria_rsp").val("I");
        $("#cont_stps").hide("slow");
    }else if(peligroso==2 && val_Calibra<=490.33 && capvol>0.5 || peligroso==2 && val_Calibra>490.33 && val_Calibra<=784.53 && capvol<=1 || peligroso==1 && val_Calibra<=686.47 && capvol<=1){
        $("#categoria_rsp").val("II");
        $("#cont_stps").hide("slow");
    }else if(peligroso==2 && val_Calibra>490.33 && val_Calibra<= 784.53 && capvol>1 || peligroso==2 && val_Calibra>784.53 || peligroso==1 && val_Calibra<=686.47 && capvol>1 || peligroso==1 && val_Calibra>686.47){
        $("#categoria_rsp").val("III");
        $("#cont_stps").show("slow");
    }
}

function finalizarProceso(){
    /*saveLevantamiento(1);
    saveLevantamiento2(1);    
    saveLevantamiento3(1);
    saveLevantamiento4(1);*/
    saveLevantamiento5(1);
    saveLevantamiento6();

    swal("Éxito", "Finalizado correctamente", "success");
    setTimeout(function(){ 
        window.location = base_url+'Operaciones/recipientes';
    }, 3500);
}

function saveLevantamiento(band){
	var form_register = $('#form_p1');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre_equipo: {
                required: true
            },
            num_serie: {
                required: true
            },
            ubicacion:{
              required: true
            },
            fabricante:{
              required: true
            },
            estado_uso:{
              required: true
            },
            id_fluido:{
              required: true
            },
            origen:{
              required: true
            },
            certif_fabrica:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_p1").valid();
    if ($valid) {
        ////////////////////
        var consultor=0;
        if($("#consultor").is(":checked")==true){
            consultor=1;
        }
        $('#savef').attr('disabled',true);
        $('#btn_save_p1').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardarLevantamiento',
            data: datos+"&tabla=levantamiento&consultor="+consultor,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $('#savef').attr('disabled',false);
                $('#btn_save_p1').attr('disabled',false);
                add_file(data);
                $("#id_reg").val(data);
                if(band==0){
            	   swal("Éxito", "Guardado Correctamente", "success");
                }
                /*if(band==1){
                    setTimeout(function(){ 
                        window.location = base_url+'Operaciones/recipientes';
                    }, 1500);
                }*/
            }
        });           
    }
}

function setNameTap(){
    if($("#orientacion").val()=="2") { 
        $("#th_tap1").html("Izquierda");  
        $("#th_tap2").html("Derecha"); 
    }else{
        $("#th_tap1").html("Superior");  
        $("#th_tap2").html("Inferior");
    }
}

function reset_img(){
    $('.img_foto').html('<img id="img_avatar" src="'+base_url+'public/img/empresa.png" class="rounded mr-3" height="80" width="80">');
}
function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val();
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Operaciones/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
/* ******************************* */

function saveLevantamiento2(band){
    var form_register = $('#form_p2');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_equipo: {
                required: true
            },
            cuerpo: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_p2").valid();
    if ($valid) {
        ////////////////////
        $('#savef').attr('disabled',true);
        $('#btn_save_p2').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardarLevantamiento',
            data: datos+"&tabla=levantamiento_paso2",
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $('#savef').attr('disabled',false);
                $('#btn_save_p2').attr('disabled',false);

                $("#id_regp2").val(data);
                guardar_tapas(0);
                if(band==0){
                   swal("Éxito", "Guardado Correctamente", "success");
                }
                calcular_resultados();
                /*if(band==1){
                    setTimeout(function(){ 
                        window.location = base_url+'Operaciones/recipientes';
                    }, 1500);
                }*/
            }
        });           
    }
}

function tapa_add(){
    tabla_tapas(0,0,'','','',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);     
}

var cont=0;
function tabla_tapas(id,id_mat,tipo,d_ext,d_int,l_val,band_ta,espesor,falda,espesor_max,num_puntos,alt_concava,radio_sup,radio_inf,falda_recta,altura_esferica,radio_esferica,radio_interior,radio_interno,parte_recta){
    var btn_x=''; var name_tapa="";
    getMateriales(id_mat,cont);
    sel1=""; sel2=""; sel3=""; sel4=""; sel5="";sel6=""; sel7=""; sel8=""; sel9="";
    if(tipo==1){ sel1="selected"; name_tapa="Elipsoidal"; }
    if(tipo==2){ sel2="selected"; name_tapa="Hemisferica"; }
    if(tipo==3){ sel3="selected"; name_tapa="Toriesférica 100/6"; $(".td_a_tt").show(); }
    if(tipo==4){ sel4="selected"; name_tapa="Toriesférica"; $(".td_a_tt").show(); }
    if(tipo==5){ sel5="selected"; name_tapa="Plana"; }
    if(tipo==6){ sel6="selected"; name_tapa="Plana circular apernada"; }
    if(tipo==7){ sel7="selected"; name_tapa="Abombada"; $(".td_a_ta").show(); }
    if(tipo==8){ sel8="selected"; name_tapa="Conica"; $(".td_a_tc").show(); }
    if(tipo==9){ sel9="selected"; name_tapa="Toriconica"; $(".td_a_ttorico").show(); }

    if(cont==0){
        btn_x='<button type="button" class="btn btn-social-icon btn-sistema round" onclick="tapa_add()"><i class="fa fa-plus"></i></button>';
    }else{
        btn_x='<button type="button" class="btn btn-social-icon btn-google round mr-2" onclick="remove_reg('+id+','+cont+')"><i class="fa fa-trash"></i></button>';
    }
    id_aux=0;
    if(id>0){
        id_aux=id;
    }else{
        id_aux=cont+1;
    }
    if(cont%2==0){
        if($("#orientacion").val()=="2") { name_tap="Izquierda"; } 
        else { name_tap="Superior"; }
    }else{
        if($("#orientacion").val()=="2") { name_tap="Derecha"; } 
        else { name_tap="Inferior"; }
    }
    if(band_ta==0){
        var html='<tr class="row_'+cont+'">\
            <td><input class="form-control" type="hidden" id="id_tapas" value="'+id+'"><label>'+name_tap+'</label>\
                <select style="font-size:10px" id="tipo_tapa" class="form-control" onchange="changeTapa('+cont+',this.value)">\
                    <option '+sel1+' value="1">Elipsoidal</option>\
                    <option '+sel2+' value="2">Hemisferica</option>\
                    <option '+sel3+' value="3">Toriesférica 100/06</option>\
                    <option '+sel4+' value="4">Toriesférica</option>\
                    <option '+sel5+' value="5">Plana</option>\
                    <option '+sel6+' value="6" disabled>Plana circular apernada</option>\
                    <option '+sel7+' value="7">Abombada</option>\
                    <option '+sel8+' value="8">Conica</option>\
                    <option '+sel9+' value="9">Toriconica</option>\
                </select></td>\
             <td><label></label><select style="font-size:10px" id="id_mat_tapa" class="form-control id_mat_tapa_'+cont+'">\
            </select></td>\
            <td><label></label><input style="font-size:10px" class="form-control espesor_'+id_aux+'" onchange="calculaDiametroTapa(0,'+id_aux+')" type="number" id="espesor" placeholder="Espesor actual"  value="'+espesor+'"></td>\
            <td><label></label><input style="font-size:10px" class="form-control d_ext_'+id_aux+'" onchange="calculaDiametroTapa(2,'+id_aux+')" type="number" id="d_ext" value="'+d_ext+'"></td>\
            <td><label></label><input style="font-size:10px" class="form-control d_int_'+id_aux+'" onchange="calculaDiametroTapa(1,'+id_aux+')" type="number" id="d_int" value="'+d_int+'"></td>\
            <td><label></label><input style="font-size:10px" class="form-control" type="number" id="longitud" placeholder="Valor" value="'+l_val+'"></td>';
            if(tipo==3 || tipo==4){  //toriesfericas
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control alt_concava_tt" type="number" id="alt_concava" placeholder="Altura, sección cóncava" value="'+alt_concava+'"></td>';
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="altura_esferica" placeholder="Altura zona esférica" value="'+altura_esferica+'"></td>';
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_esferica" placeholder="Radio zona esférica" value="'+radio_esferica+'"></td>';
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_interior" placeholder="Radio Interior" value="'+radio_interior+'"></td>';
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_interno" placeholder="Radio Interno" value="'+radio_interno+'"></td>';
                html+='<td class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="parte_recta" placeholder="Parte recta" value="'+parte_recta+'"></td>';
            }else if(tipo!=3 && tipo!=4){
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="alt_concava" placeholder="Altura, sección cóncava" value="0"></td>';
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="altura_esferica" placeholder="Altura zona esférica" value="0"></td>';
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_esferica" placeholder="Radio zona esférica" value="0"></td>';
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_interior" placeholder="Radio Interior" value="0"></td>';
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_interno" placeholder="Radio Interno" value="0"></td>';
                html+='<td style="display:none" class="td_a_tt_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="parte_recta" placeholder="Parte recta" value="0"></td>';
            }
            if(tipo==7){ 
                html+='<td class="td_a_ta_'+cont+'"><label></label><input style="font-size:10px" class="form-control alt_concava_abomb" type="number" id="alt_concava" placeholder="Altura, sección cóncava" value="'+alt_concava+'"></td>';
            }else{
                html+='<td style="display:none" class="td_a_ta_'+cont+'"><label></label><input style="font-size:10px" class="form-control alt_concava_abomb" type="number" id="alt_concava" placeholder="Altura, sección cóncava" value="0"></td>';
            }
            if(tipo==8){ 
                html+='<td class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_sup" placeholder="Radio Superior" value="'+radio_sup+'"></td>';
                html+='<td class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_inf" placeholder="Radio Inferior" value="'+radio_inf+'"></td>';
                html+='<td class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="falda_recta" placeholder="Radio Superior" value="'+falda_recta+'"></td>';
            }else{
                html+='<td style="display:none" class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_sup" value="0"></td>';
                html+='<td style="display:none" class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="radio_inf" value="0"></td>';
                html+='<td style="display:none" class="td_a_tc_'+cont+'"><label></label><input style="font-size:10px" class="form-control" type="number" id="falda_recta" value="0"></td>';
            }
            if(tipo==9){  //toriconica
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control falda_recta_tori" type="number" placeholder="Falda recta, longitud del cono" value="'+falda_recta+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_sup_tori" type="number" placeholder="Radio superior cono" value="'+radio_sup+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_inf_tori" type="number" placeholder="Radio inferior cono" value="'+radio_inf+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control altura_esferica_tori" type="number" placeholder="Altura zona esférica" value="'+altura_esferica+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_esferica_tori" type="number" placeholder="Radio zona esférica" value="'+radio_esferica+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_interior_tori" type="number" placeholder="Radio interior de tapa" value="'+radio_interior+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_interno_tori" type="number" placeholder="Radio interno" value="'+radio_interno+'"></td>';
                html+='<td class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control parte_recta_tori" type="number" placeholder="Parte recta" value="'+parte_recta+'"></td>';
            }else{
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control falda_recta_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_sup_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_inf_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control altura_esferica_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_esferica_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_interior_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control radio_interno_tori" type="number" value="0"></td>';
                html+='<td style="display:none" class="td_a_ttorico_'+cont+'"><label></label><input style="font-size:10px" class="form-control parte_recta_tori" type="number" value="0"></td>';
            }
            html+='<td>'+btn_x+'</td>\
        </tr>';
        $('#t_body').append(html);
    }
    
    if(band_ta==1){
        var html2='<tr class="row_'+cont+'">\
            <td><label>'+name_tap+'</label></td>\
            <td><input class="form-control" type="hidden" id="id_tapas2" value="'+id+'">\
                <input class="form-control" readonly type="text" id="name_tapa" value="'+name_tapa+'"></td>\
            <td><input class="form-control" readonly type="text" id="espesor" placeholder="Espesor actual"  value="'+espesor+'"></td>\
            <td><input class="form-control" readonly type="text" id="d_ext" value="'+d_ext+'"></td>\
            <td><input class="form-control" readonly type="text" id="d_int" value="'+d_int+'"></td>\
            <td><input class="form-control" readonly type="text" id="longitud" placeholder="Valor"  value="'+l_val+'"></td>';
        if(tipo==3 || tipo==4){  //toriesfericas
            html2+='<td><input class="form-control" readonly type="number" id="alt_concava" value="'+alt_concava+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" id="altura_esferica" value="'+altura_esferica+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" id="radio_esferica" value="'+radio_esferica+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" id="radio_interior" value="'+radio_interior+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" id="radio_interno" value="'+radio_interno+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" id="parte_recta" value="'+falda_recta+'"></td>';
        }
        if(tipo==7){ 
            html2+='<td><input class="form-control alt_concava_abomb" readonly type="text" id="alt_concava" value="'+alt_concava+'"></td>';
        }
        if(tipo==8){ 
            html2+='<td><input class="form-control" readonly type="text" id="radio_sup" value="'+radio_sup+'"></td>';
            html2+='<td><input class="form-control" readonly type="text" id="radio_inf" value="'+radio_inf+'"></td>';
            html2+='<td><input class="form-control" readonly type="text" id="falda_recta" value="'+falda_recta+'"></td>';
        }
        if(tipo==9){  //toriconica
            html2+='<td><input class="form-control" readonly type="number" value="'+falda_recta+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+radio_sup+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+radio_inf+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+altura_esferica+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+radio_esferica+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+radio_interior+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+radio_interno+'"></td>';
            html2+='<td><input class="form-control" readonly type="number" value="'+parte_recta+'"></td>';
        }
        html2+='<!--<td><input class="form-control" readonly type="text" id="falda" placeholder="Falda Recta o L"  value="'+falda+'"></td>-->\
            <!--<td><input class="form-control" type="text" id="espesor_max" placeholder="Espesor Máximo"  value="'+espesor_max+'"></td>\
            <td><input class="form-control" type="text" id="num_puntos" placeholder="Número de puntos"  value="'+num_puntos+'"></td>-->\
        </tr>';
        $('#t_body_lev').append(html2);  
    }
    cont++;
}

function changeTapa(cont,val){
    if(val==3 || val==4){ //toriesfericas
        $(".td_a_tt").show();
        $(".td_a_tt_"+cont+"").show();
    }else{
        $(".td_a_tt").hide();
        $(".td_a_tt_"+cont+"").hide();
    }
    if(val==7){
        $(".td_a_ta").show();
        $(".td_a_ta_"+cont+"").show();
    }else{
        $(".td_a_ta").hide();
        $(".td_a_ta_"+cont+"").hide();
    }
    if(val==8){
        $(".td_a_tc").show();
        $(".td_a_tc_"+cont+"").show();
    }else{
        $(".td_a_tc").hide();
        $(".td_a_tc_"+cont+"").hide();
    }
    if(val==9){
        $(".td_a_ttorico").show();
        $(".td_a_ttorico_"+cont+"").show();
    }else{
        $(".td_a_ttorico").hide();
        $(".td_a_ttorico_"+cont+"").hide();
    }
    
}

function getMateriales(id_mat,cont){
    $.ajax({
        type:'POST',
        url: base_url+'Operaciones/getMateriales',
        data: {id_mat:id_mat},
        success:function(result){
            $(".id_mat_tapa_"+cont+"").html(result);
        }
    });
}

function remove_reg(id,cont){
    if(id==0){
        $('.row_'+cont).remove();
        cont--;
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar el registro?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Operaciones/delete_tapa",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            $('.row_'+cont).remove();
                            cont--;
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function guardar_tapas(band_save){
    if(band_save==0){
        var DATA  = [];
        var TABLA   = $("#table_tapas tbody > tr");
        TABLA.each(function(){ 
            item = {};
            item ["id_operacion"] = $("#id_operacion2").val();
            item ["id"] = $(this).find("input[id*='id_tapas']").val();
            item ["tipo_tapa"] = $(this).find("select[id*='tipo_tapa'] option:selected").val();
            item ["id_mat_tapa"] = $(this).find("select[id*='id_mat_tapa'] option:selected").val();
            item ["d_ext"] = $(this).find("input[id*='d_ext']").val();
            item ["d_int"] = $(this).find("input[id*='d_int']").val();
            item ["longitud"] = $(this).find("input[id*='longitud']").val();
            item ["espesor"] = $(this).find("input[id*='espesor']").val();
            //item ["falda"] = $(this).find("input[id*='falda']").val();
            if($(this).find("select[id*='tipo_tapa'] option:selected").val()==3 || $(this).find("select[id*='tipo_tapa'] option:selected").val()==4){
                item ["alt_concava"] = $(this).find("input[class*='alt_concava_tt']").val();
                item ["altura_esferica"] = $(this).find("input[id*='altura_esferica']").val();  
                item ["radio_esferica"] = $(this).find("input[id*='radio_esferica']").val();
                item ["radio_interior"] = $(this).find("input[id*='radio_interior']").val(); 
                item ["radio_interno"] = $(this).find("input[id*='radio_interno']").val();
                item ["parte_recta"] = $(this).find("input[id*='parte_recta']").val();
            }
            if($(this).find("select[id*='tipo_tapa'] option:selected").val()==7){
                console.log("tapa abombada");
                item ["alt_concava"] = $(this).find("input[class*='alt_concava_abomb']").val();  
                console.log("alt_concava: "+item ["alt_concava"]);
            }
            if($(this).find("select[id*='tipo_tapa'] option:selected").val()==8){
                //console.log("tapa conica");
                item ["radio_sup"] = $(this).find("input[id*='radio_sup']").val(); 
                item ["radio_inf"] = $(this).find("input[id*='radio_inf']").val();  
                item ["falda_recta"] = $(this).find("input[id*='falda_recta']").val(); 
            }
            if($(this).find("select[id*='tipo_tapa'] option:selected").val()==9){
                item ["falda_recta_tori"] = $(this).find("input[class*='falda_recta_tori']").val();
                item ["radio_sup_tori"] = $(this).find("input[class*='radio_sup_tori']").val();  
                item ["radio_inf_tori"] = $(this).find("input[class*='radio_inf_tori']").val();
                item ["altura_esferica_tori"] = $(this).find("input[class*='altura_esferica_tori']").val();
                item ["radio_esferica_tori"] = $(this).find("input[class*='radio_esferica_tori']").val(); 
                item ["radio_interior_tori"] = $(this).find("input[class*='radio_interior_tori']").val();
                item ["radio_interno_tori"] = $(this).find("input[class*='radio_interno_tori']").val();
                item ["parte_recta_tori"] = $(this).find("input[class*='parte_recta_tori']").val();
            }
            DATA.push(item);
        });     
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url+'Operaciones/guarda_tapas',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });
    }
}

function table_data_tapas(id,band_tab){
    $.ajax({
        type:'POST',
        url: base_url+"Operaciones/get_tabla_tapas",
        data: {id:id, tabla:"tapas_paso2"},
        success: function (response){
            var array = $.parseJSON(response);
            $("#t_body_lev").html("");
            if(array.length>0) {
                array.forEach(function(element) {
                    tabla_tapas(element.id,element.id_material,element.tipo_tapa,element.d_ext,element.d_int,element.longitud,band_tab,element.espesor,element.falda,element.espesor_max,element.num_puntos,element.alt_concava,element.radio_sup,element.radio_inf,element.falda_recta,element.altura_esferica,element.radio_esferica,element.radio_interior,element.radio_interno,element.parte_recta);
                });
            }else{
                tapa_add();
            }  
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function calculaDiametro(tipo) {
    var espesor_act=parseFloat($("#espesor_act").val()).toFixed(2);
    var d_int=parseFloat($("#d_int").val()).toFixed(2);
    var d_ext=parseFloat($("#d_ext").val()).toFixed(2);
    var espesor_act2 = espesor_act*2;
    d_int_calc=0;
    d_ext_calc=0;
    if(d_int!=""){ //genera diametro exterior
        d_ext_calc=d_int + espesor_act2; 
    }
    if(d_ext!=""){ //genera diametro interior
        d_int_calc=d_ext - espesor_act2; 
    }
    if(tipo==0){
        $("#d_int").val(parseFloat(d_int_calc).toFixed(2));
        $("#d_ext").val(parseFloat(d_ext_calc).toFixed(2));
    }if(tipo==1){//int
        $("#d_ext").val(parseFloat(d_ext_calc).toFixed(2));
    }if(tipo==2){//ext
        $("#d_int").val(parseFloat(d_int_calc).toFixed(2));
    }
}

function calculaDiametroTapa(tipo,id) {
    var espesor_act=parseFloat($(".espesor_"+id+"").val()).toFixed(2);
    var d_int=parseFloat($(".d_int_"+id+"").val()).toFixed(2);
    var d_ext=parseFloat($(".d_ext_"+id+"").val()).toFixed(2);
    var espesor_act2 = espesor_act*2;
    d_int_calc=0;
    d_ext_calc=0;
    if(d_int!=""){ //genera diametro exterior
        d_ext_calc=d_int + espesor_act2; 
    }
    if(d_ext!=""){ //genera diametro interior
        d_int_calc=d_ext - espesor_act2; 
    }
    if(tipo==0){
        $(".d_int_"+id+"").val(parseFloat(d_int_calc).toFixed(2));
        $(".d_ext_"+id+"").val(parseFloat(d_ext_calc).toFixed(2));
    }if(tipo==1){//int
        $(".d_ext_"+id+"").val(parseFloat(d_ext_calc).toFixed(2));
    }if(tipo==2){//ext
        $(".d_int_"+id+"").val(parseFloat(d_int_calc).toFixed(2));
    }
}

/* ******************************* */

function saveLevantamiento3(band){
    var form_register = $('#form_p3');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            presion: {
                required: true
            },
            temperatura_op: {
                required: true
            },
            /*presion_hidro: {
                required: true
            },*/
            presion_diseno: {
                required: true
            },
            temp_diseno: {
                required: true
            },
            id_material: {
                required: true
            },
            /*espesor_diseno: {
                required: true
            },*/
            esfuerzo_ruptura: {
                required: true
            },
            eficiencia_solda: {
                required: true
            },
            factor_seguridad: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_p3").valid();
    if ($valid) {
        ////////////////////
        $('#savef').attr('disabled',true);
        $('#btn_save_p3').attr('disabled',true);
        var corrosion=0;
        if($("#corrosion").is(":checked")==true){
            corrosion=1;
        }
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardarLevantamiento',
            data: datos+"&tabla=levantamiento_paso3&corrosion="+corrosion,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $('#savef').attr('disabled',false);
                $('#btn_save_p3').attr('disabled',false);
                $("#id_reg3").val(data);
                if(band==0){
                   swal("Éxito", "Guardado Correctamente", "success");
                }
                calcular_resultados();
                /*if(band==1){
                    setTimeout(function(){ 
                        window.location = base_url+'Operaciones/recipientes';
                    }, 1500);
                }*/
            }
        });           
    }
}
function getDetalles() {
    $.ajax({
        type:'POST',
        url: base_url+"Operaciones/getDetallesMaterial",
        data: {id:$("#id_material").val(),temp_dise:$("input[name*='temp_diseno']").val()}, 
        success: function (response){
            var array = $.parseJSON(response);
            //console.log(response);
            $("input[name*='esfuerzo_diseno']").val(parseFloat(array.y).toFixed(2));// es por temperatura o interpolar
            ksi_kg = parseFloat(array.ksi).toFixed(2) * 1000 / 14.22;
            $("input[name*='esfuerzo_ruptura']").val(parseFloat(ksi_kg).toFixed(2));
            var factor=parseFloat(ksi_kg).toFixed(2) / parseFloat(array.y).toFixed(2);
            $("input[name*='factor_seguridad']").val(factor.toFixed(2));
        },
        error: function(response){
            //swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

/* ******************************* */
function tipoMedidor(val){
    if(val=="1"){
        $("#cont_rango").show("slow");
        $("#cont_rango").addClass("d-flex");
    }else{
        $("#cont_rango").removeClass("d-flex");
        $("#rango").val("0");
        $("#cont_rango").hide("slow");
    }
}

function datosCalculo(){
    //$("#cont_area").show();
    $("#area_minima").attr("readonly",false);
    if($("#tipo_disp option:selected").val()=="1"){
        $(".t2").hide();
        $(".t3").hide();
        $(".t1").show();
    }else if($("#tipo_disp option:selected").val()=="2"){
        $(".t1").hide();
        $(".t3").hide();
        $(".t2").show();
    }else if($("#tipo_disp option:selected").val()=="3"){
        $(".t1").hide();
        $(".t2").hide();
        $(".t3").show();
    }else if($("#tipo_disp option:selected").val()==4){ //protegido
        //$("#cont_area").hide();
        $("#area_minima").attr("readonly",true);
        $("#area_minima").val(0);
    }
}

function saveLevantamiento4(band){
    var form_register = $('#form_p4');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    if($("#tipo_disp option:selected").val()!="4"){
        $('#form_p4').removeData('validator');
        var $validator1 = form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                tipo_disp: {
                    required: true
                },
                pesion_calibra: {
                    required: true
                },
                diametro_desfogue: {
                    required: true
                },
                tipo_medidor: {
                    required: true
                },
                /*rango: {
                    required: true
                },*/
                capacidad: {
                    required: true
                },
                presion_absoluta: {
                    required: true
                },
                constante_rot: {
                    required: true
                },
                presion_regulada: {
                    required: true
                },
                temp_diseno: {
                    required: true
                },
                presion_atmo: {
                    required: true
                },
                /*peso_molecular: {
                    required: true
                },*/
                diametro_valvula: {
                    required: true
                },
                coeficiente_desc: {
                    required: true
                },
                /*factor_compre: {
                    required: true
                },
                factor_liq: {
                    required: true
                },
                factor: {
                    required: true
                },
                area_total: {
                    required: true
                }*/
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register, -100);
            },
            highlight: function(element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function(element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function(label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
    }else{
        $('#form_p4').removeData('validator');
        var $validator1 = form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                tipo_disp: {
                    required: true
                },
                justif_tecnica: {
                    required: true
                },
                pesion_calibra: {
                    required: true
                },
                diametro_desfogue: {
                    required: true
                },
                tipo_medidor: {
                    required: true
                },
            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register, -100);
            },
            highlight: function(element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
            },
            unhighlight: function(element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },
            success: function(label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
    }

    var $valid = $("#form_p4").valid();
    if ($valid) {
        ////////////////////
        $('#savef').attr('disabled',true);
        $('#btn_save_p4').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardarLevantamiento',
            data: datos+"&tabla=levantamiento_paso4&vol_total="+$("#vol_total").val(),
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $('#savef').attr('disabled',false);
                $('#btn_save_p4').attr('disabled',false);
                $("#id_reg4").val(data);
                if(band==0){
                   swal("Éxito", "Guardado Correctamente", "success");
                }
                /*if(band==1){
                    setTimeout(function(){ 
                        window.location = base_url+'Operaciones/recipientes';
                    }, 1500);
                }*/
            }
        });           
    }
}

function tipoDispositivo(value) {
    if(value=="4"){ //protegido por sistema
        $("#cont_justif").show();
        $("#cont_calc_area").hide();
    }else{
        $("#cont_justif").hide();
        $("#cont_calc_area").show();
    }
}

/* ******************************* */

function saveLevantamiento5(band){
    var form_register = $('#form_p5');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            id_equipo: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_p5").valid();
    if ($valid) {
        ////////////////////
        $('#savef').attr('disabled',true);
        $('#btn_save_p5').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Operaciones/guardarLevantamiento',
            data: datos+"&tabla=levantamiento_paso5",
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                $('#savef').attr('disabled',false);
                $('#btn_save_p5').attr('disabled',false);
                $("#id_reg5").val(data);
                if($("#file").val()!=""){
                    $('#file').fileinput('upload');
                }
                if($("#file_placa").val()!=""){
                    $('#file_placa').fileinput('upload');
                }
                if($("#file_croq2").val()!=""){
                    $('#file_croq2').fileinput('upload');
                }
                if($("#img_equipo").val()!=""){
                    $('#img_equipo').fileinput('upload');
                }
                //$('#files').fileinput('upload');
                //guardar_tapas(1);
                guardar_tapas_esp();
                if(band==0){
                   swal("Éxito", "Guardado Correctamente", "success");
                }
                setTimeout(function(){ 
                    table_esp_tapas($("#id_operacion").val(),1);
                    //window.location = base_url+'Operaciones/recipientes';
                }, 2000);
            }
        });           
    }
}

function getFotosL5(){
    $("#cont_imgs").html("");
    $.ajax({
        type:'POST',
        url: base_url+'Operaciones/getImages',
        data: { id_opera: $("#id_operacion").val() },
        success:function(result){
            $("#cont_imgs").html(result);
        }
    });
}

function tapa_add_esp(){
    tabla_tapas_esp(0,'','','');     
}

var cont_esp=1;
function tabla_tapas_esp(id,tapa1,tapa2,cuerpo,band_cont,cont_int){
    var btn_x='';
    if(band_cont==0){
        cont_esp=cont_int;
    }

    if(cont_esp==1){
        btn_x='<button type="button" class="btn btn-social-icon btn-sistema round" onclick="tapa_add_esp()"><i class="fa fa-plus"></i></button>';
    }else{
        btn_x='<button type="button" class="btn btn-social-icon btn-google round mr-2" onclick="remove_reg_esp('+id+','+cont_esp+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_esp_'+cont_esp+'">\
        <td>'+cont_esp+'</td>\
        <td><input class="form-control" type="hidden" id="id_tapas_esp" value="'+id+'">\
            <input class="form-control" type="number" id="esp_tapa1" value="'+tapa1+'"></td>\
        <td><input class="form-control" type="number" id="esp_tapa2" value="'+tapa2+'"></td>\
        <td><input class="form-control" type="number" id="esp_cuerpo" value="'+cuerpo+'"></td>\
        <td>'+btn_x+'</td>\
    </tr>';
    $('#t_body_esp').append(html);
    cont_esp++;
    if($("#id_reg5").val()>0 && $("#niveles").val()=="0"){
        $("#niveles").val(cont_esp);
        $("#lecturas_nivel").val(1);
    }
}

function remove_reg_esp(id,cont){
    if(id==0){
        $('.row_esp_'+cont).remove();
        cont_esp--;
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar el registro?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Operaciones/delete_tapa_esp",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            $('.row_esp_'+cont).remove();
                            cont--;
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function guardar_tapas_esp(){
    var DATA  = [];
    var TABLA   = $("#table_esp_taps tbody > tr");
    TABLA.each(function(){ 
        item = {};
        item ["id_operacion"] = $("#id_operacion5").val();
        item ["id"] = $(this).find("input[id*='id_tapas_esp']").val();
        item ["esp_tapa1"] = $(this).find("input[id*='esp_tapa1']").val();
        item ["esp_tapa2"] = $(this).find("input[id*='esp_tapa2']").val();
        item ["esp_cuerpo"] = $(this).find("input[id*='esp_cuerpo']").val();
        DATA.push(item);

    });     
    INFO  = new FormData();
    aInfo   = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO, 
        type: 'POST',
        url : base_url + 'Operaciones/guarda_espesor_tapas',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
        }
    });   
}

function table_esp_tapas(id,band_cont){
    $.ajax({
        type:'POST',
        url: base_url+"Operaciones/get_tabla_tapas",
        data: {id:id, tabla:"espesores_paso5"},
        success: function (response){
            var array = $.parseJSON(response);
            $("#t_body_esp").html("");
            var cont_int=0;
            if(array.length>0) {
                array.forEach(function(element) {
                    cont_int++;
                    tabla_tapas_esp(element.id,element.esp_tapa1,element.esp_tapa2,element.esp_cuerpo,band_cont,cont_int);
                });
            }else{
                tapa_add_esp();
            }  
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function generarTablaLectura(){
    var niveles=$("#niveles").val();
    var lecturas_nivel=$("#lecturas_nivel").val();
    var total = niveles * lecturas_nivel;
    total = total - cont_esp;
    for(var i=0; i<=total; i++){
        tabla_tapas_esp(0,'','',''); 
    }
}

/* *********************************/
function saveLevantamiento6(){
    $('#savef').attr('disabled',true);
    $('#btn_save_p6').attr('disabled',true);
    var TABLA   = $("#table_equi_bloque tbody > tr");
    var cont_eb=0; var tipo_equi=0;
    TABLA.each(function(){ 
        cont_eb++;
        if($(this).find("input[id*='tipo_equi"+cont_eb+"']").is(":checked")==true){
            tipo_equi=cont_eb;
        }
    });  

    var datos = $('#form_p6').serialize();
    $.ajax({
        type: 'POST',
        url: base_url + 'Operaciones/guardarLevantamiento',
        data: datos+"&tabla=levantamiento_paso6&id_equipo_bloque="+tipo_equi,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            $('#savef').attr('disabled',false);
            $('#btn_save_p6').attr('disabled',false);
            $("#id_reg6").val(data);
        }
    });           
    
}

function calcular_resultados(){
    $.ajax({
        type:'POST',
        url: base_url+"Operaciones/calcular",
        data: {id_operacion:$("#id_operacion").val()},
        success: function (response){
            //console.log("response: "+response);
            var array = $.parseJSON(response);
            $("#t_body_res").html(array.html);
            $("#vol_total").val(parseFloat(array.vol_total).toFixed(2));
            $("#capacidad").val(parseFloat(array.vol_total).toFixed(2));
            if($("#pmtp_aux").val()=="0"){
                $("#pmtp").val(parseFloat(array.pmtp).toFixed(2));
            }
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}

function calculaArea(){
    var capacidad=$("input[id*='capacidad_wa']").val();
    var constante_rot=$("input[name*='constante_rot']").val();
    var temp_absoluta=$("input[name*='temp_absoluta']").val();
    var peso_molecular=$("input[name*='peso_molecular']").val();
    var coeficiente_desc=$("input[name*='coeficiente_desc']").val();
    var presion_absoluta=$("input[name*='presion_absoluta']").val();
    var presion_atmo=$("input[name*='presion_atmo']").val();
    var densidad_flui=$("input[name*='densidad_flui']").val();

    var area_total=0;

    //modificar calculo de acuerdo a orientacion del envolvente
    /*if($("#orientacion").val()=="1"){//Vertical
        area_total=6*capacidad*(Math.sqrt(peso_molecular*temp_absoluta*1)/(constante_rot*coeficiente_desc*presion_absoluta*1));
    }else{ //horizontal
        area_total=capacidad/(constante_rot*coeficiente_desc*presion_absoluta*Math.sqrt(peso_molecular/temp_absoluta));
    }*/

    //dependiendo del tipo de valvula es el calculo y formula 
    if($("#tipo_disp option:selected").val()==1){ //aire
        area_total=capacidad/constante_rot*coeficiente_desc*presion_absoluta*Math.sqrt((peso_molecular/temp_absoluta));
        /*console.log("capacidad: "+capacidad);
        console.log("constante_rot: "+constante_rot);
        console.log("coeficiente_desc: "+coeficiente_desc);
        console.log("presion_absoluta: "+presion_absoluta);
        console.log("peso_molecular: "+peso_molecular);
        console.log("temp_absoluta: "+temp_absoluta);*/
    }else if($("#tipo_disp option:selected").val()==2){ //vapor
        area_total=capacidad/(constante_rot*coeficiente_desc*presion_absoluta);
    }else if($("#tipo_disp option:selected").val()==3){ //alivio
        area_total=capacidad/(constante_rot * coeficiente_desc) *Math.sqrt(presion_absoluta-presion_atmo*densidad_flui);
    }

    $("#area_minima").val(parseFloat(area_total).toFixed(2));
}