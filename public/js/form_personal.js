var base_url = $('#base_url').val();
$(document).ready(function() {
    $('#foto_avatar').change(function(){
	     document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
	});
});

function reset_img(){
	$('.img_personal').html('<img id="img_avatar" src="'+base_url+'public/img/avatar.png" class="rounded mr-3" height="64" width="64">');
}

function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {
                required: true
            },
            correo: {
                required: true
            },
            celular:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();

    if ($valid) {
        ////////////////////
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Personal/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            	var personalId=parseInt(data);
            	$('#personalId_tem').val(personalId);
            	$('#personalId').val(personalId);
            	add_file(data);
            	var canvas = document.getElementById('patientSignature');
        		var dataURL = canvas.toDataURL();
        		if(dataURL.length>2000){
        			savefirma(personalId);
        		}
        		setTimeout(function(){ 
        			if($('#cedula').val()!=""){
                   		$('#cedula').fileinput('upload');
        			}
                }, 1000);

            	swal("Éxito", "Guardado Correctamente", "success");
                //setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                //}, 2000);
                setTimeout(function(){ 
                	console.log("guardado y redireccionando");
                    window.location.href = base_url+"Personal";
                }, 3000);
            }
        });           
    }
}

function add_user(){
	var idper=$('#personalId').val();
	if(idper!=0){
		$.validator.addMethod("usuarioexiste", function(value, element) {
	    var respuestav;
	    $.ajax({
	        type: 'POST',
	        url: base_url + 'Personal/validar',
	        data: {
	            Usuario: value
	        },
	        async: false,
	        statusCode: {
	            404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
	        },
	        success: function(data) {
	            if (data == 1) {
	                if ($('#UsuarioID').val() > 0) {
	                    respuestav = true;
	                } else {
	                    respuestav = false;
	                }

	            } else {
	                respuestav = true;
	            }



	        }
	    });
	    console.log(respuestav);
	    return respuestav;
		});

		var form_register = $('#form_data_user');
		var error_register = $('.alert-danger', form_register);
		var success_register = $('.alert-success', form_register);

		var $validator1 = form_register.validate({
		    errorElement: 'div', //default input error message container
		    errorClass: 'vd_red', // default input error message class
		    focusInvalid: false, // do not focus the last invalid input
		    ignore: "",
		    rules: {
		        Usuario: {
		            usuarioexiste: true,
		            minlength: 3,
		            required: true
		        },
		        contrasena: {
		            minlength: 6,
		            required: true

		        },
		        contrasena2: {
		            equalTo: contrasena,
		            required: true
		        },
		        perfilId: {
		            required: true
		        },

		    },
		    messages: {
		        Usuario: {
		            usuarioexiste: 'Seleccione otro nombre de usuario'
		        }

		    },
		    errorPlacement: function(error, element) {
		        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
		            element.parent().append(error);
		        } else if (element.parent().hasClass("vd_input-wrapper")) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },

		    invalidHandler: function(event, validator) { //display error alert on form submit              
		        success_register.fadeOut(500);
		        error_register.fadeIn(500);
		        scrollTo(form_register, -100);

		    },

		    highlight: function(element) { // hightlight error inputs

		        $(element).addClass('vd_bd-red');
		        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		    },

		    unhighlight: function(element) { // revert the change dony by hightlight
		        $(element)
		            .closest('.control-group').removeClass('error'); // set error class to the control group
		    },

		    success: function(label, element) {
		        label
		            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
		            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
		        $(element).removeClass('vd_bd-red');
		    }
		});
		var $valid = $("#form_data_user").valid();
		if ($valid) {
			$('.btn_registro').attr('disabled',true);
		    var datos = form_register.serialize()+'&personalId='+$('#personalId').val();
	        $.ajax({
	            type: 'POST',
	            url: base_url + 'Personal/add_user',
	            data: datos,
	            statusCode: {
		            404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
	                },
	                500: function(){
	                    swal("Error!", "500", "error");
	                }
	            },
	            success: function(data) {
	            	$('#UsuarioID').val(data);
	                swal("Éxito", "Guardado Correctamente", "success");
	                setTimeout(function(){ 
	                	window.location = base_url+'Personal';
	                    //$('.btn_registro').attr('disabled',false);
	                }, 2000);
	            }
	        });
		}
	}else{
		swal("¡Atención!", "Favor de guardar previamente datos generales", "error");
	}
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_avatar');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Personal/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            async:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}
function savefirma(id){
	var canvas = document.getElementById('patientSignature');
    var dataURL = canvas.toDataURL();
    $.ajax({
        type:'POST',
        url: base_url+'Personal/guardarFirma',
        async:false,
        data: {
            firma:dataURL, id:id
        },
        success:function(data){
            
        }
    });
}
function editfirma(){
	$('#cont_firma').show();
	$('#cont_firma_view').hide();
}