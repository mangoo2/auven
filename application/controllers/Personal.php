<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
  function __construct(){
      parent::__construct();
      $this->load->helper('url');
      $this->load->model('ModeloGeneral');
      $this->load->model('ModeloCatalogos');
      $this->load->model('ModelPersonal');
      if (!$this->session->userdata('logeado')){
          redirect('/Login');
      }else{
          $this->perfilid=$this->session->userdata('perfilid_tz');
          $this->idpersonal=$this->session->userdata('idpersonal_tz');
          //ira el permiso del modulo
          $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,1);// 2 es el id del submenu
          if ($permiso==0) {
              redirect('/Sistema');
          }
      }
  }

  public function index(){
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/personal/index');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/personal/indexjs');
	}

  public function registrar($id=0){
    if($id==0){
       $data['personalId']=0;
       $data['nombre']='';
       $data['correo']='';
       $data['celular']='';
       $data['UsuarioID'] = 0;
       $data['perfilId'] = 0;
       $data['personalId'] = 0;
       $data['User'] = '';
       $data['contrasena'] = '';
       $data['foto']='';
       $data['firma']='';
       $data['cedula']='';
       $data['cedula_prof']='';
    }else{
      $resul=$this->ModeloGeneral->getselectwhere('personal','personalId',$id);
      foreach ($resul as $item) {
        $data['personalId']=$item->personalId;
        $data['nombre']=$item->nombre;
        $data['correo']=$item->correo;
        $data['celular']=$item->celular;
        $data['foto']=$item->foto;
        $data['firma']=$item->firma;
        $data['cedula']=$item->cedula;
        $data['cedula_prof']=$item->cedula_prof;
      }
      $result = $this->ModelPersonal->getUsuarios_user($id);
      foreach ($result as $item) {
        $data['UsuarioID'] = $item->UsuarioID;
        $data['perfilId'] = $item->perfilId;
        $data['personalId'] = $item->personalId;
        $data['User'] = $item->Usuario;
      }
      $data['contrasena'] = 'xxxxxx';
    } 
    $data['get_perfil']=$this->ModeloGeneral->getselectwhere('perfiles','estatus',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/personal/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/personal/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['personalId'];
    unset($datos['personalId']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'personalId',$id,'personal');
      $id_reg=$id; 
    }else{
      $id_reg=$this->ModeloGeneral->tabla_inserta('personal',$datos);
    }  
    echo $id_reg;
  }

  public function delete(){
      $id = $this->input->post('id');
      $data = array('estatus' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'personalId',$id,'personal');
      echo $resul;
  }
    
  function validar(){
    $Usuario = $this->input->post('Usuario');
    $result=$this->ModeloGeneral->getselectwhere('usuarios','Usuario',$Usuario);
    $resultado=0;
    foreach ($result as $row) {
        $resultado=1;
    }
    echo $resultado;
  }

  public function add_user(){
    $datos = $this->input->post();
    $pss_verifica = $datos['contrasena'];
    $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
    $datos['contrasena'] = $pass;
    if($pss_verifica == 'xxxxxx'){
       unset($datos['contrasena']);
    }
    $id=$datos['UsuarioID'];
    unset($datos['UsuarioID']);
    unset($datos['contrasena2']);
    $id_reg=0;
    if ($id>0) {
        $this->ModeloGeneral->updateCatalogo($datos,'UsuarioID',$id,'usuarios');
        $id_reg=$id;
    }else{
        $id_reg=$this->ModeloGeneral->tabla_inserta('usuarios',$datos);
    }   
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelPersonal->get_result($params);
    $totaldata= $this->ModelPersonal->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  function cargafiles(){
    $id=$this->input->post('id');
    $folder="personal";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'personalId',$id,'personal');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 
  function guardarFirma(){
    $params = $this->input->post();
    $id = $params['id'];
    $firma = $params['firma'];

    $namefile=date('ymd_His').'.png';
    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $firma));
    file_put_contents(FCPATH.'public/firmas/'.$namefile, $imagenBinaria);



        $this->ModeloGeneral->updateCatalogo_value(array('firma'=>$namefile),array('personalId'=>$id),'personal');
  }
  function cargacedula(){
    $rid = $_POST['id'];
       

        $config['upload_path']          = FCPATH.'public/doc_cedula/';
        $config['allowed_types']        = 'gif|jpg|png|pdf';
        $config['max_size']             = 5000;
        $config['file_name']='cedufile_'.date('YmdGis').'_'.rand(0, 99);       
        $output = [];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('cedula')){
            $error = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($error));
                        
        }else{
             $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension

            $this->ModeloGeneral->updateCatalogo_value(array('cedula'=>$file_name),array('personalId'=>$rid),'personal');

            $data = array('upload_data' => $this->upload->data());
            
            log_message('error', json_encode($data));
        }
        echo json_encode($output);
  }

}    