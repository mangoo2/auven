<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fluidos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelFluidos');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid, 4); // 2 es el id del submenu
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/fluidos/index');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/fluidos/indexjs');
  }

  public function registrar($id=0)
  {
    if($id==0){
      $data['tittle']='Nuevo';
      $data['id']='';
      $data['foto']='';
      $data['nombre']='';
      $data['peligroso']='';
    }else{
      $data['tittle']='Edición de '; 
      $resul=$this->ModeloGeneral->getselectwhere('fluidos','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['foto']=$x->foto;
        $data['nombre']=$x->nombre;
        $data['peligroso']=$x->peligroso;
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/fluidos/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/fluidos/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'fluidos');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('fluidos',$datos);
    }  
    echo $id_reg;
  }

  function cargafiles(){
    $id=$this->input->post('id');
    $folder="fluidos";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $nombre_archivo = $this->eliminar_acentos($nombre_archivo);
    $nombre_archivo = str_replace(" ","", $nombre_archivo);
    $newfile='doc_'.$fecha.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'fluidos');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelFluidos->get_result($params);
    $totaldata= $this->ModelFluidos->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'fluidos');
      echo $resul;
  }

  /* ***************************************************/
  public function eliminar_acentos($cadena){
    //Reemplazamos la A y a
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ç', 'ç'),
    array('C', 'c'),
    $cadena
    );

    return $cadena;
  }

}
