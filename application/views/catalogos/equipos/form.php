<div class="row">
	<div class="col-12">
		<div class="content-header"><?php echo $tittle ?> equipo</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-content">
				<div class="card-body table-responsive">

					<form method="post" id="form_data">
						<input type="hidden" name="id" id="id_reg" value="<?php echo $id ?>">

						<div class="row">
							<div class="col-12 form-group d-flex">
								<div class="col-3 text-right align-self-center">
									<label>Tipo de equipo</label>
								</div>
								<div class="col-9">
									<select class="form-control" name="tipo" id="tipo" onchange="tipo_equipo()">
										<option disabled="" selected="">Seleccionar una opción</option>
										<option value="1" <?php if($tipo==1) echo 'selected'?>>Equipo MT</option>
										<option value="2" <?php if($tipo==2) echo 'selected'?>>Equipo UT</option>
									</select>
								</div>
							</div>
						</div>
						<div class="equipo_mt_text____" >
							<div class="row">	
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Marca</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="marca" value="<?php echo $marca ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Modelo</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="modelo" value="<?php echo $modelo ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Serie</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="serie" value="<?php echo $serie ?>">
									</div>
								</div>
							</div>

							<div class="row" id="cont_ut">
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Tipo de Palpador</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="tipo_palpa" value="<?php echo $tipo_palpa ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Tamaño</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="size" value="<?php echo $size ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Frecuencia</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="frecuencia" value="<?php echo $frecuencia ?>">
									</div>
								</div>
							</div>
							<div class="row" id="cont_mt">
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Yugo Electromagnético:</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="yugo" value="<?php echo $yugo ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Partículas Magnéticas</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="part_magne" value="<?php echo $part_magne ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Tipo de partículas</label>
									</div>
									<div class="col-3">
										<input class="form-control" type="text" name="tipo_part" value="<?php echo $tipo_part ?>">
									</div>
									<div class="col-3 text-right align-self-center">
										<label>Color</label>
									</div>
									<div class="col-3">
										<input class="form-control" type="text" name="color" value="<?php echo $color ?>">
									</div>
								</div>
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Contraste</label>
									</div>
									<div class="col-3">
										<input class="form-control" type="text" name="contraste" value="<?php echo $contraste ?>">
									</div>
								</div>
							</div>
                        </div>
                        <div class="equipo_ut_text____">
							<div class="row">
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>No. Certificado</label>
									</div>
									<div class="col-9">
										<input class="form-control" type="text" name="no_certificado" value="<?php echo $no_certificado ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12 form-group d-flex">
									<div class="col-3 text-right align-self-center">
										<label>Fecha Cal</label>
									</div>
									<div class="col-3">
										<input class="form-control" type="date" name="fecha_cal" value="<?php echo $fecha_cal ?>">
									</div>
									<div class="col-3 text-right align-self-center">
										<label>Vigencia Cal</label>
									</div>
									<div class="col-3">
										<input class="form-control" type="date" name="vigencia_cal" value="<?php echo $vigencia_cal ?>">
									</div>
								</div>
							</div>
						</div>		
					</form>
					<hr>
					<div class="row">
                        <div class="col-lg-12">
                            <button type="button" onclick="add_form()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Equipo" class="btn btn-secondary mb-1">Regresar</a>
                        </div>
                    </div>  
				</div>

			</div>
		</div>
	</div>
</div>