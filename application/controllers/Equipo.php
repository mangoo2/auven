<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Equipo extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelEquipo');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid,5); // 2 es el id del submenu
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/equipos/index');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/equipos/indexjs');
  }

  public function registrar($id=0)
  {
    if($id==0){
      $data['tittle']='Nuevo';
      $data['id']=0;
      $data['tipo']=0;
      $data['marca']='';
      $data['modelo']='';
      $data['serie']='';
      $data['tipo_palpa']='';
      $data['size']='';
      $data['frecuencia']='';
      $data['yugo']='';
      $data['part_magne']='';
      $data['tipo_part']='';
      $data['color']='';
      $data['contraste']='';
      $data['no_certificado']='';
      $data['fecha_cal']='';
      $data['vigencia_cal']='';
    }else{
      $data['tittle']='Edición'; 
      $resul=$this->ModeloGeneral->getselectwhere('equipo','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['tipo']=$x->tipo;
        $data['marca']=$x->marca;
        $data['modelo']=$x->modelo;
        $data['serie']=$x->serie;
        $data['tipo_palpa']=$x->tipo_palpa;
        $data['size']=$x->size;
        $data['frecuencia']=$x->frecuencia;
        $data['yugo']=$x->yugo;
        $data['part_magne']=$x->part_magne;
        $data['tipo_part']=$x->tipo_part;
        $data['color']=$x->color;
        $data['contraste']=$x->contraste;
        $data['no_certificado']=$x->no_certificado;
        $data['fecha_cal']=$x->fecha_cal;
        $data['vigencia_cal']=$x->vigencia_cal;
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/equipos/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/equipos/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'equipo');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('equipo',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelEquipo->get_result($params);
    $totaldata= $this->ModelEquipo->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'equipo');
      echo $resul;
  }
}
