<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');

    $GLOBALS["fecha_inspeccion"]=$fecha_inspeccion;
    $GLOBALS["num_expediente"]=$num_expediente;
    $GLOBALS["foto"]=$foto;
    $GLOBALS["logo_cli"]=$logo_cli;
    $GLOBALS["nombre"]=$nombre;
    $GLOBALS["direccion"]=$direccion;
    $GLOBALS["color"]=$color;
    $GLOBALS["consultor"]=$consultor;
    if($consultor==1){
        $GLOBALS["direccionc"]=$direccionc;
    }
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        //log_message('error', "consultor: ".$GLOBALS["consultor"]);  
        if($GLOBALS["consultor"]==0){ //cliente directo
            $logo_head = FCPATH.'uploads/empresa/'.$GLOBALS["foto"];
            $GLOBALS["logo"]=$logo_head;
        }else{ //consultor
            $logo_headcon = FCPATH.'uploads/consultor/'.$GLOBALS["foto"];
            $logo_head = FCPATH.'uploads/empresa/'.$GLOBALS["logo_cli"];
            $GLOBALS["logo"]=$logo_headcon;
        }
        
        $html = '<style>
            .ft9{ font-size:10px; text-align:center; font-weight:bold; }
            .backg{background-color:rgb(217,217,217);}
            .pspaces{ font-size:1.8px;}
            .just{ text-align:justify; }
        </style>';
        $html.='
                <table border="1" cellpadding="2" align="center" class="ft9">
                    <tr>
                        <td height="95px" rowspan="6" width="25%"><p class="pspaces"></p><img src="'.$logo_head.'"></td>
                        <td rowspan="6" style="font-size:22px" width="50%"><p class="pspaces"></p>EXPEDIENTE</td>
                        <td width="25%"></td>
                    </tr>               
                    <tr class="backg">
                        <td>No. EXPEDIENTE</td>
                    </tr>
                    <tr>
                        <td>'.$GLOBALS["num_expediente"].' EXP</td>
                    </tr>
                    <tr class="backg">
                        <td>FECHA</td>
                    </tr>
                    <tr>
                        <!--<td>'.$GLOBALS["fecha_inspeccion"].'</td>-->
                        <td>'.date("d-m-Y").'</td>
                    </tr>
                    <tr>
                        <td> Página: '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                    </tr>
                </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        /*if($GLOBALS["consultor"]==0){ //cliente directo
            //$dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            //$color=$GLOBALS['color'];
            $dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            $color="green";
        }else{
            //$dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            //$color="green";
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            $color=$GLOBALS['color'];
        }*/
        if($GLOBALS["consultor"]==0){ //cliente directo
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
        }else{
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccionc"];
        }
        //$dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
        $color=$GLOBALS['color'];
        //log_message('error', "color: ".$color);  
      $html = '
        <style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center}
        </style> 
      <table class="table" width="100%" cellpadding="2">
        <tr><td colspan="2" class="back_foot"></td></tr>
        <tr><td colspan="2" class="footerpage" >'.$dir_foot.'</td></tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
      //$this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Expediente');
$pdf->SetTitle('Expediente ');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-020-STPS-2011');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(20,37,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(23);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 23);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);

// add a page
$pdf->AddPage('P', 'A4'); 

$htmlg='<style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center; font-size:14px;}
            .tab_info{ font-weight:bold;}
        </style> 
        <p></p>
        <table align="center" width="100%" cellpadding="2">
            <tr><td height="220px"><img width="220px" src="'.$GLOBALS["logo"].'"></td></tr>
        </table>
        <p></p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>EXPEDIENTE PARA RECIPIENTES</td></tr>
            <tr><td>SUJETOS A PRESIÓN CATEGORÍA '.$l6->categoria_rsp.'</td></tr>
            <tr><td>NOM-020-STPS-2011</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EMPRESA</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$GLOBALS["nombre"].'</td></tr>
            <tr><td>'.$GLOBALS["direccion"].'</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EQUIPO</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$l->nombre_equipo.'</td></tr>
            <tr><td>IDENTIFICACIÓN: '.$l->num_identifica.'</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 

if($l->estado_uso=="1") $estado_uso="NUEVO"; else $estado_uso="USADO";
$getF=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$l->id_fluido));
$fluido=$getF->nombre;
$foto_flu=$getF->foto;

$getM=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$l3->id_material));
$mat_cpo=$getM->spec_no;

if($l->orientacion=="1") { $orientacion="VERTICAL"; } else { $orientacion="HORIZONTAL"; }
if($l4->tipo_disp=="1") $tipo="Válvula de seguridad (gas)";
else if($l4->tipo_disp=="2") $tipo="Válvula de seguridad (vapor)";
else if($l4->tipo_disp=="3") $tipo="Válvula de alivio (liquido)";
else if($l4->tipo_disp=="4") $tipo="Protegido por el sistema";

if($l4->tipo_disp=="4")
    $num_tipo="N/A";
else
    $num_tipo="1";

if($l5->file_placa!=""){
    $img_placa='<img src="'.FCPATH."uploads/placas/".$l5->file_placa.'" width="220px" height="210px">';
}else{
    $img_placa='<p class="pspaces2"></p><p style="font-size:25px">SIN PLACA DE DATOS</p><p class="pspaces2"></p>';
}
$img_flu="";
if($foto_flu!=""){
    $img_flu='<img width="90px" src="'.FCPATH.'uploads/fluidos/'.$foto_flu.'">';
}
//log_message('error', "img_flu: ".$img_flu);  
if($l->codigo_norma=="1"){
    $codigo_norma="CÓDIGO ASME SECCIÓN VIII, DIVISIÓN 1";
}else{
    $codigo_norma="CÓDIGO ASME SECCIÓN I";
}

if($l6->categoria_rsp=="I" || $l6->categoria_rsp=="II"){
    $txt_stps="NO APLICA";
}else{
    $txt_stps=$l6->num_stps;
}

$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .pspaces{ font-size:0.1px;}
            .pspaces2{ font-size:30px;}
        </style> 
        <p class="tab_info">1. FICHA TÉCNICA DESCRIPTIVA</p>
        <table class="table" border="1" rules="none" align="center" width="100%" cellpadding="6">
            <tr>
                <th>Usuario:</th>
                <td colspan="3">'.$GLOBALS["nombre"].'</td>
            </tr>
            <tr>
                <th>Descripción del Recipiente:</th>
                <td colspan="3">'.$l->nombre_equipo.'</td>
            </tr>
            <tr>
                <th>Número de identificación:</th>
                <td>'.$l->num_identifica.'</td>
                <th>No. de STPS:</th>
                <td>'.$txt_stps.'</td>
            </tr>
            <tr>
                <th>Fabricante:</th>
                <td>'.$l->fabricante.'</td>
                <th>Lugar de Origen:</th>
                <td>'.$l->origen.'</td>
            </tr>
            <tr>
                <th>Año de Fabricación:</th>
                <td>'.$l->anio_fabrica.'</td>
                <th>Número de Serie:</th>
                <td>'.$l->num_serie.'</td>
            </tr>
            <tr>
                <th>Código o Norma de Fabricación:</th>
                <td>'.$codigo_norma.'</td>
                <td colspan="2" rowspan="3" height="90px">'.$img_flu.'</td>
            </tr>
            <tr>
                <th>Certificado de Fabricación:</th>
                <td>'.$l->certif_fabrica.'</td>
            </tr>
            <tr>
                <th>Ubicación del Equipo:</th>
                <td>'.$l->ubicacion.'</td>
            </tr>
            <tr>
                <th>Fluidos Manejados:</th>
                <td>'.$fluido.'</td>
                <th>Capacidad volumétrica:</th>
                <td>'.($l2->capacidad/1000).' m³</td>
            </tr>
            <tr>
                <th><p class="pspaces"></p>Presión de Diseño:</th>
                <td>'.$l3->presion_diseno.' kg/cm²<br>'.round(($l3->presion_diseno*98.0665),2).' kPa</td>
                <th><p class="pspaces"></p>Temperatura de Diseño:</th>
                <td>'.$l3->temp_diseno.' °C<br>'.($l3->temp_diseno+273.15).' K</td>
            </tr>
            <tr>
                <th><p class="pspaces"></p>Presión de Operación:</th>
                <td>'.$l3->presion.' kg/cm²<br>'.round(($l3->presion*98.0665),2).' kPa</td>
                <th><p class="pspaces"></p>Temperatura de Operación:</th>
                <td>'.$l3->temperatura_op.' °C<br>'.($l3->temperatura_op+273.15).' K</td>
            </tr>

            <tr>
                <th><p class="pspaces"></p>Presión Regulada:</th>
                <td>'.$l4->presion_regulada.' kg/cm²<br>'.round(($l4->presion_regulada*98.0665),2).' kPa</td>
                <th>Presión Interior Máxima de Trabajo:</th>
                <td>'.$l3->presion_diseno.'kg/cm²<br>'.round(($l3->presion_diseno*98.0665),2).' kPa</td>
            </tr>
            <tr>
                <th>Presión de Prueba Hidrostática:</th>
                <td>'.$l3->presion_hidro.' kg/cm²</td>
                <th>Orientación:</th>
                <td>'.$orientacion.'</td>
            </tr>
            <tr>
                <th>Estado:</th>
                <td>'.$estado_uso.'</td>
                <th>Rango del Manómetro:</th>
                <td>0 A '.$l4->rango.' kPa</td>
            </tr>
            <tr>
                <th>Tipo de Dispositivo de Relevo de Presión:</th>
                <td>'.$tipo.'</td>
                <th>Número de Dispositivos de Relevo de Presión:</th>
                <td>'.$num_tipo.'</td> 
            </tr>
            <tr>
                <th>Diámetro Exterior del Envolvente:</th>
                <td>'.$l2->d_ext.' mm</td>
                <th>Longitud Parte Recta:</th>
                <td>'.$l2->cuerpo.' mm</td>
            </tr>
            <tr>
                <th>No. de Dictamen:</th>
                <td>VER DOCUMENTO CORRESPONDIENTE</td>
                <th>Material:</th>
                <td>'.$mat_cpo.'</td>
            </tr> 
        </table>
        <p class="tab_info">2. FOTOGRAFÍA O CALCA DE LA PLACA DE DATOS</p>
        <table border="1" class="table" cellpadding="6">
            <tr>
                <th height="220px">'.$img_placa.'</th>
            </tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4');
$img_equipo="";

$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:20px;}
            .img_foto{ max-width:780px; }
        </style> 
        <p class="tab_info">3. FOTOGRAFÍA DEL EQUIPO</p>
        <!--<table border="1" class="table" cellpadding="6">
            <tr>
                <th><p class="pspaces2"></p>'.$img_equipo.' <p class="pspaces2"> </p></th>
            </tr>
        </table>-->';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$height = $pdf->getPageHeight() - 80;
$width = $pdf->getPageWidth() - 40;

if($l5->img_equipo!=""){
    //$img_equipo='<img class="img_foto" src="'.FCPATH.'uploads/equipos_cliente/'.$l5->img_equipo.'">';
    $pdf->Image(FCPATH.'uploads/equipos_cliente/'.$l5->img_equipo, 20, 45, $width, $height, '', '', 'T', false, 280, '', false, false, 1, false, false, false);
}

$backg1="";
$backg2="";
$backg3="";
if(mb_strtoupper($l6->categoria_rsp)=="I") $backg1="backg";
if(mb_strtoupper($l6->categoria_rsp)=="II") $backg2="backg";
if(mb_strtoupper($l6->categoria_rsp)=="III") $backg3="backg";
$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info p_just">4. DESCRIPCIÓN BREVE DE SU OPERACIÓN, RIESGOS RELACIONADOS CON SU OPERACIÓN Y ELEMENTOS DE SEGURIDAD.</p>
        <p class="p_center" style="font-size:12px">DESCRIPCIÓN BREVE DE LA OPERACIÓN DEL EQUIPO</p>   
        <table border="1" class="table" cellpadding="6">
            <tr>
                <th width="15%"><p class="pspaces"></p>Fluido:</th>
                <td width="30%"><p class="pspaces"></p>'.$fluido.'</td>';
                if($l4->tipo_disp!="4")
                    $htmlg.='<th width="55%" colspan="2"><p class="p_just">Tomando en cuenta que el fluido opera a <b>'.($l3->temperatura_op+273.15).' K</b>, cuenta con una <b>'.$tipo.'</b> calibrada a <b>'.round(($l4->presion_regulada*98.0665),2).' kPa</b>, la capacidad volumétrica es de <b>'.($l2->capacidad/1000).' m³,</b> de acuerdo a lo establecido en la tabla 1 de la NOM-020-STPS-2011, el RSP se clasifica en categoría <b>'.$l6->categoria_rsp.'</b>.</p></th>';
                else{
                   $htmlg.='<th width="55%" colspan="2"><p class="p_just">Tomando en cuenta que el fluido opera a <b>'.($l3->temperatura_op+273.15).' K</b>, y <b>'.round(($l4->presion_regulada*98.0665),2).' kPa</b>, la capacidad volumétrica es de <b>'.($l2->capacidad/1000).' m³</b> y el equipo se encuentra <b>protegido por el sistema</b>. De acuerdo a lo establecido en la tabla 1 de la NOM-020-STPS-2011, el RSP se clasifica en categoría <b>'.$l6->categoria_rsp.'</b>.</p></th>'; 
                }
            $htmlg.='</tr>
            <tr><th colspan="4"><p>TABLA 1</p><p>TIPOS DE CATEGORÍAS PARA RECIPIENTES SUJETOS A PRESIÓN</p></th></tr>
            <tr><th width="10%">CATEGORÍA</th><th width="25%">FLUIDO</th><th width="40%">PRESIÓN</th><th width="25%">VOLUMEN</th></tr>
            <tr class="'.$backg1.'"><th>I</th><th>Agua, Aire y/o fluido no peligroso.</th><th>Menor o igual a 490.33 kPa</th><th>Menor o igual a 0.5 m³</th></tr>
            <tr class="'.$backg2.'"><th><p class="pspaces"></p>II</th><th>Agua, aire y/o fluido no peligroso.<br>Agua, aire y/o fluido no peligroso.<br>Peligroso.</th><th>Menor o igual a 490.33 kPa<br>Mayor a 490.33 kPa y menor o igual a 784.53 kPa<br>Menor o igual a 686.47 kPa</th><th>Mayor a 0.5 m³<br>Menor o igual a 1 m³<br>Menor o igual a 1 m³</th></tr>
            <tr class="'.$backg3.'"><th><p class="pspaces"></p>III</th><th>Agua, aire y/o fluido no peligroso.<br>Agua, aire y/o fluido no peligroso.<br>Peligroso.<br>Peligroso.</th><th>Mayor a 490.33 kPa y menor o igual a 784.53 kPa<br>Mayor a 784.53 kPa<br>Menor o igual a 686.47 kPa<br>Mayor a 686.47 kPa</th><th>Mayor a 1 m³<br>Cualquier volumen.<br>Mayor a 1 m³<br>Cualquier volumen.</th></tr>
        </table>
        <span class="table">*Presión de calibración en su(s) dispositivo(s) de relevo de presión.</span>
        <p></p>
        <p class="p_center">DESCRIPCIÓN BREVE DE LA OPERACIÓN DEL EQUIPO</p>
        <table border="1" class="table" cellpadding="6">
            <!--<tr>
                <th class="p_just">El propósito del <b>'.$l->nombre_equipo.'</b> es filtrar el aire comprimido, enviarlo al tanque de almacenamiento de aire y que esté disponible cuando se requiera en el proceso.</th>
            </tr>
            <tr>
                <th class="p_just">El riesgo inherente que se puede presentar en el <b>'.$l->nombre_equipo.'</b> es un incremento de presión por descontrol operacional, para ello se tiene un manómetro con rango 0 a <b>'.$l4->rango.' kPa</b>, además de que el equipo está protegido directamente con una <b>'.$tipo.'</b> de '.$l4->diametro_valvula.' mm calibrada a <b>'.round(($l4->presion_regulada*98.0665),2).' kPa</b> instalada en el equipo.</th>
            </tr>-->
            <tr>
                <th class="p_just">'.$l->descrip_opera.'</th>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

if($GLOBALS["consultor"]==0){ //cliente directo
    $color=$GLOBALS['color'];
}else{
    //$color="green";
    $color=$GLOBALS['color'];
}
$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info p_just">5. RESUMEN CRONOLÓGICO DE LAS REVISIONES Y MANTENIMIENTOS EFECTUADOS, DE ACUERDO CON EL PROGRAMA ELABORADO PARA TAL EFECTO, DEBIDAMENTE REGISTRADOS Y DOCUMENTADOS.</p>  
        <table border="1" class="table" cellpadding="5">
            <thead>
                <tr style="background-color: '.$color.'">
                    <td width="10%">Fecha:</td>
                    <td width="40%">Descripción</td>
                    <td width="15%">Documento</td>
                    <td width="15%">Realizó</td>
                    <td width="20%">Responsable Empresa</td>
                </tr>
            </thead>';
            $cont_t1=0;
            foreach ($resu as $r) {
                if($r->tipo==1){
                    $cont_t1++;
                    $htmlg.='<tr><th width="10%">'.$r->fecha.'</th><th width="40%">'.$r->descrip.'</th><th width="15%">'.$r->documento.'</th><th width="15%">'.$r->realiza.'</th><th width="20%">'.$r->responsable.'</th></tr>';
                }
            }
            if($cont_t1<=10){
                $c2=25;
            }else if($cont_t1>20){
                $c2=15;
            }
            for($c=0; $c<$c2; $c++){
                $htmlg.='<tr><th width="10%"></th><th width="40%"></th><th width="15%"></th><th width="15%"></th><th width="20%"></th></tr>';   
            }
            
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info p_just">6. RESUMEN CRONOLÓGICO DE LAS PRUEBAS DE PRESIÓN O EXÁMENES NO DESTRUCTIVOS.</p>  
        <table border="1" class="table" cellpadding="5">
            <thead>
                <tr style="background-color: '.$color.'">
                    <td width="10%">Fecha:</td>
                    <td width="40%">Descripción</td>
                    <td width="15%">Documento</td>
                    <td width="15%">Realizó</td>
                    <td width="20%">Resumen Autorizado</td>
                </tr>
            </thead>';
            $cont_t2=0;
            foreach ($resu as $r) {
                if($r->tipo==2){
                    $cont_t2++;
                    $htmlg.='<tr><th width="10%">'.$r->fecha.'</th><th width="40%">'.$r->descrip.'</th><th width="15%">'.$r->documento.'</th><th width="15%">'.$r->realiza.'</th><th width="20%">'.$r->responsable.'</th></tr>';
                }
            }
            if($cont_t2<=10){
                $c3=25;
            }else if($cont_t2>20){
                $c3=15;
            }
            for($c=0; $c<$c3; $c++){
                $htmlg.='<tr><th width="10%"></th><th width="40%"></th><th width="15%"></th><th width="15%"></th><th width="20%"></th></tr>';   
            }
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info p_just">7. RESUMEN CRONOLÓGICO DE LAS MODIFICACIONES Y ALTERACIONES EFECTUADOS AL EQUIPO, DEBIDAMENTE REGISTRADAS Y DOCUMENTADAS.</p>  
        <table border="1" class="table" cellpadding="5">
            <thead>
                <tr style="background-color: '.$color.'">
                    <td width="10%">Fecha:</td>
                    <td width="40%">Descripción</td>
                    <td width="15%">Documento</td>
                    <td width="15%">Realizó</td>
                    <td width="20%">Responsable Empresa</td>
                </tr>
            </thead>';
            $cont_t3=0;
            foreach ($resu as $r) {
                if($r->tipo==3){
                    $cont_t3++;
                    $htmlg.='<tr><th width="10%">'.$r->fecha.'</th><th width="40%">'.$r->descrip.'</th><th width="15%">'.$r->documento.'</th><th width="15%">'.$r->realiza.'</th><th width="20%">'.$r->responsable.'</th></tr>';
                }
            }
            if($cont_t3<=10){
                $c4=25;
            }else if($cont_t3>20){
                $c4=15;
            }
            for($c=0; $c<$c4; $c++){
                $htmlg.='<tr><th width="10%"></th><th width="40%"></th><th width="15%"></th><th width="15%"></th><th width="20%"></th></tr>';   
            }
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_center{text-align:center}
        </style> 
        <p class="tab_info p_just">8. RESUMEN CRONOLÓGICO DE LAS REPARACIONES QUE IMPLICARON SOLDADURA, DEBIDAMENTE REGISTRADAS Y DOCUMENTADAS.</p>  
        <table border="1" class="table" cellpadding="5">
            <thead>
                <tr style="background-color: '.$color.'">
                    <td width="10%">Fecha:</td>
                    <td width="40%">Descripción</td>
                    <td width="15%">Documento</td>
                    <td width="15%">Realizó</td>
                    <td width="20%">Responsable Empresa</td>
                </tr>
            </thead>';
            $cont_t4=0;
            foreach ($resu as $r) {
                if($r->tipo==4){
                    $cont_t4++;
                    $htmlg.='<tr><th width="10%">'.$r->fecha.'</th><th width="40%">'.$r->descrip.'</th><th width="15%">'.$r->documento.'</th><th width="15%">'.$r->realiza.'</th><th width="20%">'.$r->responsable.'</th></tr>';
                }
            }
            if($cont_t4<=10){
                $c5=25;
            }else if($cont_t4>20){
                $c5=15;
            }
            for($c=0; $c<$c5; $c++){
                $htmlg.='<tr><th width="10%"></th><th width="40%"></th><th width="15%"></th><th width="15%"></th><th width="20%"></th></tr>';   
            }
        $htmlg.='</table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->Output('Expediente.pdf', 'I');

?>