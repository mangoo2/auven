<!-- Account Settings starts -->
<input type="hidden" id="personalId_tem" value="<?php echo $personalId;?>">
<div class="row">
    <div class="col-md-3 mt-3">
        <!-- Nav tabs -->
        <ul class="nav flex-column nav-pills" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">
                    <i class="ft-settings mr-1 align-middle"></i>
                    <span class="align-middle">General</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="change-password-tab" data-toggle="tab" href="#change-password" role="tab" aria-controls="change-password" aria-selected="false">
                    <i class="ft-lock mr-1 align-middle"></i>
                    <span class="align-middle">Accesos</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-9">
        <!-- Tab panes -->
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="tab-content">
                        <!-- General Tab -->
                        <div class="tab-pane active" id="general" role="tabpanel" aria-labelledby="general-tab">
                            <div class="media">
                                <span class="img_personal">
                                    <?php if($foto!=''){ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>uploads/personal/<?php echo $foto ?>" class="rounded mr-3" height="64" width="64">
                                    <?php }else{ ?>
                                        <img id="img_avatar" src="<?php echo base_url(); ?>public/img/avatar.png" class="rounded mr-3" height="64" width="64">
                                    <?php } ?>
                                </span>
                                <div class="media-body">
                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-start px-0 mb-sm-2">
                                        <label class="btn btn-sm bg-light-primary mb-sm-0" for="foto_avatar">Subir foto</label>
                                        <input type="file" id="foto_avatar" hidden>
                                        <a class="btn btn-sm bg-light-secondary ml-sm-2" onclick="reset_img()">Reiniciar</a>
                                    </div>
                                    <p class="text-muted mb-0 mt-1 mt-sm-0">
                                        <small>Permitido JPG, GIF or PNG. Tamaño máximo de 800kB</small>
                                    </p>
                                </div>
                            </div>
                            <hr class="mt-1 mt-sm-2">
                            <form method="post" id="form_data">
                                <input type="hidden" name="personalId" id="personalId" value="<?php echo $personalId ?>">
                                <div class="row">
                                    <div class="col-12 form-group">
                                        <label>Nombre completo</label>
                                        <div class="controls">
                                            <input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="name">Email</label>
                                        <div class="controls">
                                            <input type="email" name="correo" class="form-control" placeholder="ejemplo@ejemplo.com" value="<?php echo $correo ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="email">Celular</label>
                                        <div class="controls">
                                            <input type="text" name="celular" class="form-control" value="<?php echo $celular ?>">
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label for="email">Cédula Profesional</label>
                                        <div class="controls">
                                            <input type="text" name="cedula_prof" class="form-control" value="<?php echo $cedula_prof ?>">
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="cont_firma">
                                        <div id="aceptance">
                                            <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                                                <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                                                <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                                                    
                                                </canvas>
                                                <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                                    <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignature" 
                                                      data-signature="patientSignature">Limpiar</a>
                                                    <a class="btn btn-success waves-effect waves-green green btn-flat firma_empleado" style="display:none;">Aceptar</a>
                                                </div>
                                            </div>  
                                                  
                                        </div>
                                    </div>
                                    <div id="cont_firma_view" style="display:none;">
                                        <div class="firma_img"></div>
                                        <div>
                                            <a class="btn btn-outline-dark mb-1" onclick="editfirma()"><i class="icon-xl far fa-edit"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="file" name="cedula" id="cedula">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                    <button type="button" onclick="add_form()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Personal" class="btn btn-secondary mb-1">Regresar</a>
                                </div>
                            </div>
                        </div>

                        <!-- Change Password Tab -->
                        <div class="tab-pane" id="change-password" role="tabpanel" aria-labelledby="change-password-tab">
                            <form method="post" id="form_data_user">
                                <input type="hidden" name="UsuarioID" id="UsuarioID" value="<?php echo $UsuarioID ?>">
                                <div class="form-group">
                                    <label for="old-password">Perfil</label>
                                    <div class="controls">
                                        <select class="form-control vd_bd-red" name="perfilId" id="perfilId">
                                            <option disabled="" selected="">Seleccione opción</option>
                                            <?php foreach ($get_perfil as $x){ ?>
                                              <option value="<?php echo $x->perfilId ?>" <?php if($x->perfilId==$perfilId) echo 'selected' ?>><?php echo $x->nombre ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="new-password">Usuario</label>
                                    <div class="controls">
                                        <input type="text" name="Usuario" id="Usuario" class="form-control" value="<?php echo $User ?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="retype-new-password">Contraseña</label>
                                    <div class="controls">
                                        <input type="password" class="form-control vd_bd-red" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list" value="<?php echo $contrasena ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="retype-new-password">Verificar contraseña</label>
                                    <div class="controls">
                                        <input type="password" class="form-control vd_bd-red" id="contrasena2" name="contrasena2" value="<?php echo $contrasena ?>">
                                    </div>
                                </div>
                                <div class="d-flex flex-sm-row flex-column justify-content-end">
                                    <button type="button" onclick="add_user()" class="btn btn-sistema mr-sm-2 mb-1 btn_registro"><i class="fa fa-save"></i> Guardar</button>
                                    <a href="<?php echo base_url()?>Personal" class="btn btn-secondary mb-1">Regresar</a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Account Settings ends -->