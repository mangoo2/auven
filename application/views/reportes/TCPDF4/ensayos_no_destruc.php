<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    setlocale(LC_ALL, 'spanish-mexican');
    setlocale(LC_ALL, 'es_MX');

    $GLOBALS["fecha_inspeccion"]=$fecha_inspeccion;
    $GLOBALS["num_expediente"]=$num_expediente;
    $GLOBALS["foto"]=$foto;
    $GLOBALS["nombre"]=$nombre;
    $GLOBALS["direccion"]=$direccion;
    $GLOBALS["color"]=$color;
    $GLOBALS["consultor"]=$consultor;

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        //log_message('error', "consultor: ".$GLOBALS["consultor"]);  
        if($GLOBALS["consultor"]==0){ //cliente directo
            $logo_head=base_url().'uploads/empresa/'.$GLOBALS["foto"];
            $GLOBALS["logo"]=$logo_head;
        }else{ //consultor
            $logo_head=base_url().'public/img/logo_inicio.png';
            $GLOBALS["logo"]=$logo_head;
        }
        
        $html = '<style>
            .ft9{ font-size:10px; text-align:center; font-weight:bold; }
            .backg{background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.5px;}
            .just{ text-align:justify; }
        </style>';
        $html.='
                <table border="1" cellpadding="2" align="center" class="ft9">
                    <tr>
                        <td rowspan="6" width="25%"><img src="'.$logo_head.'"></td>
                        <td rowspan="6" style="font-size:22px" width="50%"><p class="pspaces"></p>ENSAYOS NO DESTRUCTIVOS</td>
                        <td width="25%"></td>
                    </tr>               
                    <tr class="backg">
                        <td>No. EXPEDIENTE</td>
                    </tr>
                    <tr>
                        <td>'.$GLOBALS["num_expediente"].' END</td>
                    </tr>
                    <tr class="backg">
                        <td>FECHA</td>
                    </tr>
                    <tr>
                        <td>'.$GLOBALS["fecha_inspeccion"].'</td>
                    </tr>
                    <tr>
                        <td> Página: '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                    </tr>
                </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        if($GLOBALS["consultor"]==0){ //cliente directo
            //$dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            //$color=$GLOBALS['color'];
            $dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            $color="green";
        }else{
            //$dir_foot="Soluciones Ambientales y en Seguridad e Higiene S.A. de C.V. 19 poniente 1508 Interior 3 Col. Barrio de Santiago C.P. 72410 Puebla, Pue. Tel. (222) 2310276- (222) 2322752 www.ecose.com.mx e-mail: ventas@ecose.com.mx";
            //$color="green";
            $dir_foot=$GLOBALS["nombre"]." ".$GLOBALS["direccion"];
            $color=$GLOBALS['color'];
        }
        //log_message('error', "color: ".$color);  
      $html = '
        <style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center}
        </style> 
      <table class="table" width="100%" cellpadding="2">
        <tr><td colspan="2" class="back_foot"></td></tr>
        <tr><td colspan="2" class="footerpage" >'.$dir_foot.'</td></tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
      //$this->writeHTMLCell(188, '', 12, 269, $html, 0, 0, 0, true, 'C', true);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Expediente');
$pdf->SetTitle('Expediente ');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('NOM-020-STPS-2011');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->setPrintHeader(true);
// set margins
$pdf->SetMargins(20,37,20);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(23);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 23);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('calibri', '', 12);

// add a page
$pdf->AddPage('P', 'A4'); 

$htmlg='<style type="text/css">
            .back_foot{font-size:9px; background-color: '.$color.'; font-weight: bold; margin-top:8px;}
            .footerpage{font-size:9px;font-style: italic;}
            .table{text-align:center; font-size:14px;}
            .tab_info{ font-weight:bold;}
        </style> 
        <p></p><p></p>
        <table align="center" width="100%" cellpadding="2">
            <tr><td><img width="250px" src="'.$GLOBALS["logo"].'"></td></tr>
        </table>
        <p></p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>INSPECCIÓN POR ENSAYOS NO DESTRUCTIVOS</td></tr>
            <tr><td>A RECIPIENTES SUJETOS A PRESIÓN </td></tr>
            <tr><td>CATEGORÍA '.$l6->categoria_rsp.' NOM-020-STPS-2011</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EMPRESA</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$GLOBALS["nombre"].'</td></tr>
            <tr><td>'.$GLOBALS["direccion"].'</td></tr>
        </table>
        <p></p><p></p>
        <p class="table">EQUIPO</p>
        <table class="tab_info" border="1" rules="none" align="center" width="100%" cellpadding="10">
            <tr><td>'.$l->nombre_equipo.'</td></tr>
            <tr><td>IDENTIFICACIÓN: '.$l->num_identifica.'</td></tr>
        </table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 

if($l->estado_uso=="1") $estado_uso="USADO"; else $estado_uso="NUEVO";
$getF=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$l->id_fluido));
$fluido=$getF->nombre;
$foto_flu=$getF->foto;

$getM=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$l3->id_material));
$mat_cpo=$getM->spec_no;

if($l->orientacion=="1") { $orientacion="VERTICAL"; } else { $orientacion="HORIZONTAL"; }
if($l4->tipo_disp=="1") $tipo="Válvula de segunda (gas)";
else if($l4->tipo_disp=="2") $tipo="Válvula de segunda (vapor)";
else if($l4->tipo_disp=="3") $tipo="Válvula de alivio (liquido)";
else if($l4->tipo_disp=="4") $tipo="Protegido por el sistema";

if($l5->file_placa!=""){
    $img_placa='<img src="'.base_url()."uploads/placas/".$l5->file_placa.'" width="140px">';
}else{
    $img_placa='<p class="pspaces2"></p><p style="font-size:25px">SIN PLACA DE DATOS</p><p class="pspaces2"></p>';
}

if($tap[0]->tipo_tapa==1){ $name_tapa="ELIPSOIDAL 2:1"; }
if($tap[0]->tipo_tapa==2){ $name_tapa="HEMISFERICA"; }
if($tap[0]->tipo_tapa==3){ $name_tapa="TORIESFÉRICA"; }
if($tap[0]->tipo_tapa==4){ $name_tapa="ABOMBADA"; }

$pdf->Bookmark('I. Descripción del Equipo -------------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

$htmlg='<style type="text/css">
            .table{text-align:left; font-size:11px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}

        </style> 
        <p class="tab_info">I.- DESCRIPCIÓN DEL EQUIPO</p>
        <table class="table" border="1" rules="none" align="left" width="100%" cellpadding="10">
            <tr>
                <th>Nombre del Equipo:</th>
                <td>'.$l->nombre_equipo.'</td>
            </tr>
            <tr>
                <th>Identificación del Equipo:</th>
                <td>'.$l->num_identifica.'</td>
            </tr>
            <tr>
                <th>Usuario:</th>
                <td>'.$GLOBALS["nombre"].'</td>
            </tr>
            <tr>
                <th>Dirección:</th>
                <td>'.$GLOBALS["direccion"].'</td>
            </tr>
            <tr>
                <th>Ubicación del equipo:</th>
                <td>'.$l->ubicacion.'</td>
            </tr>
            <tr>
                <th>Material del Envolvente:</th>
                <td>'.$mat_cpo.'</td>
            </tr>
            <tr>
                <th>Material del Tapas:</th>';  
                $mat_tap="";
                foreach ($tap as $t) {
                    $getMat=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$t->id_material));
                    $mat_tap.=$getMat->spec_no.", ";
                }  

            $htmlg.='<td>'.$mat_tap.'</td></tr>
            <tr>
                <th>Tipo de Tapas:</th>
                <td>'.$name_tapa.'</td>
            </tr>
            <tr>
                <th>Diámetro Exterior del Envolvente:</th>
                <td>'.$l2->d_ext.' mm</td>
            </tr>
            <tr>
                <th>Longitud Parte Recta:</th>
                <td>'.$l2->cuerpo.' mm</td>
            </tr>
            <tr>
                <th>Capacidad Volumétrica:</th>
                <td>'.($l2->capacidad/1000).' m³</td>
            </tr>
            <tr>
                <th>Fabricante:</th>
                <td>'.$l->fabricante.'</td>
            </tr>
            <tr>
                <th>Número de Serie:</th>
                <td>'.$l->num_serie.'</td>
            </tr>
            <tr>
                <th>Año de Fabricación:</th>
                <td>'.$l->anio_fabrica.'</td>
            </tr>
            <tr>
                <th>Fecha de Inspección:</th>
                <td>'.$fecha_inspeccion.'</td>
            </tr>';
            

        $htmlg.='</table>';
$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$pdf->Bookmark('II. Resumen de la Inspección --------------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));

$txt_resum=""; $txt_obj="";
if($l6->categoria_rsp=="III"){
    $txt_resum=" y para obtener el número de control otorgado por la Secretaría de Trabajo y Previsión Social";
    $txt_obj="con la finalidad de obtener el número de control otorgado por la Secretaría del Trabajo y Previsión Social,"; 
}
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .sangria{text-indent: 30px;}
        </style> 
        <p class="tab_info p_just">II.- RESUMEN DE LA INSPECCIÓN</p>
        <p class="p_just p_size">A solicitud de la empresa <b>'.$GLOBALS["nombre"].'</b>'.$txt_resum.' conforme a lo establecido en la NOM-020-STPS-2011 se llevaron a efecto las inspecciones por Ensayos No Destructivos en el equipo <b>'.$l->nombre_equipo.'</b> con No. de Identificación <b>'.$l->num_identifica.'</b> ubicado en <b>'.$l->ubicacion.'</b>.</p>
        <p class="tab_info p_center">OBJETIVO GENERAL DE LAS INSPECCIONES</p>
        <p class="p_center p_size">• Evaluar la seguridad del Recipiente Sujeto a Presión '.$txt_obj.' de acuerdo con la Norma Oficial Mexicana NOM-020 - STPS - 2011.</p>
        <p class="p_center p_size">• Aplicar dos métodos de inspección, uno superficial: Partículas Magnéticas así como un método de inspección volumétrica por medio de Ultrasonido Industrial (Medición de Espesores de Pared).</p>
        <p class="tab_info p_center">PARÁMETROS Y EXTENSIÓN DE PRUEBAS</p>
        <p class="p_just p_size">Los parámetros de pruebas así como los criterios de aceptación y rechazo aplicados se establecieron de conformidad con el Código ASME BPV Sección V y Sección VIII División 1, respectivamente, y en base a los procedimientos de inspección derivados de este documento, mismos que fueron debidamente revisados y aprobados por inspectores Nivel III en Ensayos No Destructivos certificados por The American Society for Nondestructive Testing (ASNT).</p>
        <p class="p_just p_size"><b><u>Medición de Espesores con Ultrasonido.-</u></b> Esta técnica se empleó para la determinación del espesor en el envolvente y tapas del equipo inspeccionado; la toma de lecturas se efectuó de la siguiente manera:</p>
        <p class="sangria p_just p_size"><b><u>a) Envolvente. -</u></b> Se obtuvieron '.$l6->niveles.' niveles en el envolvente, en cada uno se tomaron '.$l6->lecturas_nivel.' lecturas distribuidas equidistantemente a 0°, 90°, 180° y 270° respectivamente.</p>
        <p class="sangria p_just p_size"><b><u>b) Tapas. -</u></b> Se tomaron '.$get_cont1->num_rows().' lecturas en cada una de las tapas distribuidas equidistantemente.</p>
        <p class="p_just p_size">El objetivo de la medición de espesores es determinar si el espesor actual cumple con el espesor mínimo requerido en la memoria de cálculo del equipo para soportar la presión interna de diseño.</p>
        <p class="p_just p_size"><b><u>Partículas Magnéticas.-</u></b> Este método de inspección se aplica con la finalidad de detectar y evaluar la posible presencia de discontinuidades superficiales y sub superficiales tales como grietas por fatiga y en general, cualquier discontinuidad abierta a la superficie que pudiera poner en riesgo la integridad del equipo. La inspección se realiza en las zonas de soldaduras del envolvente y tapas de acuerdo al procedimiento PRC-DT-002 REV. 3.</p>
        <p class="tab_info p_center">PERSONAL TÉCNICO</p>
        <p class="p_just p_size"><b><u>Inspectores.- </u></b>El personal técnico que lleva a cabo la inspección por pruebas no destructivas así como la evaluación de los resultados obtenidos es Calificado como Nivel II de acuerdo a la Práctica Recomendada No. SNT-TC-1A en los métodos de:</p>
        <p class="p_just p_size">1) Partículas Magnéticas (MT).</p>
        <p class="p_just p_size">2) Ultrasonido Industrial (UT).</p>
        <p class="tab_info p_center">ACTIVIDADES DE APOYO</p>
        <p class="p_just p_size">Para la inspección de las soldaduras lineales y circunferenciales es necesaria la remoción de cualquier elemento no deseado que pueda interferir en la aplicación del método mediante pulidor eléctrico con carda y/o herramienta manual menor. Después de los trabajos de inspección las superficies intervenidas deben ser restauradas.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$pdf->Bookmark('III. Resultados de las Inspecciones -------------------------------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .sangria{text-indent: 30px;}
        </style> 
        <p class="tab_info p_just">III.- RESULTADOS DE LAS INSPECCIONES</p>
        <p class="p_just p_size">Los resultados obtenidos por cada método de prueba aplicado son los siguientes:</p>
        <p class="p_just p_size"><b><u>Medición de Espesores con Ultrasonido:</u></b></p>
        <p class="p_just p_size">Los resultados de la medición de espesores con ultrasonido efectuada al <b>'.$l->nombre_equipo.'</b> con identificación <b>'.$l->num_identifica.'</b> <b><u>SON ACEPTABLES</u></b> de acuerdo con los espesores mínimos requeridos en la memoria de cálculo. Ver reporte técnico No. <b>'.$num_expediente.'-END UT</b> de este documento.</p>
        <p class="p_just p_size"><b><u>Partículas Magnéticas:</u></b></p>
        <p class="p_just p_size"><b><u>NO</u></b> se obtuvieron indicaciones superficiales relevantes. Ver el reporte técnico No. <b>'.$num_expediente.'-END MT </b>de este documento.</p>
        <p class="pspaces2"></p>
        <p class="tab_info p_center"><u>CONCLUSIÓN DE LAS INSPECCIONES</u></p>
        <p class="pspaces2"></p>
        <p class="p_just p_size">En virtud de los resultados obtenidos durante las pruebas efectuadas: medición de espesores con ultrasonido y Partículas Magnéticas, se concluye que el <b>'.$l->num_identifica.'</b> con identificación <b>'.$l->num_identifica.'</b>, <b>CUMPLE</b> con los criterios de aceptación y rechazo establecidos en el Código ASME BPV Sección VIII División 1, Edición 2010.</p>
        <p class="pspaces2"></p>
        <p class="tab_info p_center"><u>RECOMENDACIONES</u></p>
        <p class="pspaces2"></p>
        <p class="p_just p_size">Se recomienda llevar a cabo las actividades establecidas en el programa de mantenimiento preventivo.</p>
        <p class="pspaces2"></p>
        <p class="tab_info p_center p_size">Responsable del análisis de resultados.</p>
        <p class="pspaces2"></p>
        <p class="tab_info p_center">___________________________________</p>
        <p class="tab_info p_center p_size"><b>'.$tecnico.'</b></p>
        <p class="tab_info p_center p_size">Técnico Nivel II E.N.D.</p>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

if($GLOBALS["consultor"]==0){ //cliente directo
    $color=$GLOBALS['color'];
}else{
    //$color="green";
    $color=$GLOBALS['color'];
}
$getF=$this->ModeloGeneral->getselectwhererow2("fluidos",array('id'=>$l->id_fluido));
$fluido=$getF->nombre;
$foto_flu=$getF->foto;

$getM=$this->ModeloGeneral->getselectwhererow2("material",array('id'=>$l3->id_material));
$mat_cpo=$getM->spec_no;

$getE=$this->ModeloGeneral->getselectwhererow2("equipo",array('id'=>$l5->id_equipo));
$getE2=$this->ModeloGeneral->getselectwhererow2("equipo",array('id'=>$l5->id_equipo2));

$pdf->AddPage('P', 'A4'); 
$pdf->Bookmark('IV. Reporte de medición de espesores con ultrasonido ----------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .sangria{text-indent: 30px;}
        </style> 
        <p class="tab_info p_just">IV.- REPORTE DE MEDICIÓN DE ESPESORES CON ULTRASONIDO</p>
        <table border="1" class="table" cellpadding="8">
            <tr><th class="tab_info" style="color:#ffffff ; background-color:'.$color.'">INFORMACIÓN GENERAL</th><th colspan="3"></th></tr>
            <tr>
                <th width="20%">Empresa:</th>
                <td width="40%">'.$GLOBALS["nombre"].'</td>
                <th colspan="13" width="15%">Informe No:</th>
                <td colspan="13" width="25%">'.$num_expediente.'-END UT</td>
            </tr>
            <tr>
                <th>Nombre del Equipo:</th>
                <td>'.$l->nombre_equipo.'</td>
            </tr>
            <tr>
                <th>Identificación:</th>
                <td>'.$l->num_identifica.'</td>
            </tr>
            <tr>
                <th>Categoría:</th>
                <td>'.$l6->categoria_rsp.'</td>
            </tr>
            <tr>
                <th>Fabricante:</th>
                <td>'.$l->fabricante.'</td>
            </tr>
            <tr>
                <th>No. de Serie:</th>
                <td>'.$l->num_serie.'</td>
            </tr>
            <tr>
                <th>Año de Fabricación:</th>
                <td>'.$l->anio_fabrica.'</td>
            </tr>
            <tr>
                <th>Fluido que contiene:</th>
                <td>'.$fluido.'</td>
            </tr>
            <tr>
                <th>Capacidad volumétrica:</th>
                <td>'.($l2->capacidad/1000).' m³</td>
            </tr>
            <tr>
                <th>Tipo de Material:</th>
                <td>'.$mat_cpo.'</td>
            </tr> 
            <tr>
                <th>Dimensiones:</th>
                <td>Longitud: '.$l2->cuerpo.' mm / Díametro: '.$l2->d_ext.' mm</td>
            </tr> 
            <tr>
                <th>Ubicación del Equipo:</th>
                <td>'.$l->ubicacion.'</td>
            </tr>
            <tr>
                <th>Presión de Diseño:</th>
                <td>'.round($l3->presion_diseno*98.0665,2).' kPa</td>
            </tr>
            <tr>
                <th>Presión de Operación:</th>
                <td>'.round($l3->presion*98.0665,2).' kPa</td>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="8">
            <tr><th colspan="2" class="tab_info" style="color:#ffffff; background-color:'.$color.'">EQUIPO UTILIZADO</th><th colspan="4"></th></tr>
            <tr>
                <th width="20%">Nombre del equipo:</th>
                <td width="80%" colspan="5">Equipo Ultrasónico Digital Medidor de Espesores</td>
            </tr>
            <tr>
                <th width="15%">Marca:</th>
                <td width="15%">'.$getE->marca.'</td>
                <th width="15%">Modelo:</th>
                <td width="15%">'.$getE->modelo.'</td>
                <th width="20%">No. de Serie:</th>
                <td width="20%">'.$getE->serie.'</td>
            </tr>
            <tr>
                <th width="15%">Tipo de Palpador:</th>
                <td width="15%">'.$getE->tipo_palpa.'</td>
                <th width="15%">Tamaño:</th>
                <td width="15%">'.$getE->size.'</td>
                <th width="20%">Frecuencia:</th>
                <td width="20%">'.$getE->frecuencia.'</td>
            </tr>
            <tr>
                <th colspan="2">Fecha de calibración del equipo:</th>
                <td width="15%">'.$getE->fecha_cal.'</td>
                <th colspan="2">Identificación o Informe:</th>
                <td width="20%">'.$getE->no_certificado.'</td>
            </tr>

        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$en_opera="--"; $fuera_opera="--"; $insp_env="--"; $insp_tap="--";
if($get_cont3->num_rows()>0)
    $insp_env='<p class="checked">✔</p>';
if($get_cont1->num_rows()>0 || $get_cont2->num_rows()>0)
    $insp_tap='<p class="checked">✔</p>';

if($l->en_opera=="1")
    $en_opera='<p class="checked">✔</p>';
if($l->en_opera=="2")
    $fuera_opera='<p class="checked">✔</p>';

// 1=vertical |
// 2=horizontal -
if($l->orientacion=="2") { $name_tap="Izquierda"; $name_tap2="Derecha"; } 
else { $name_tap="Superior"; $name_tap2="Inferior"; }

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .sangria{text-indent: 30px;}
            .checked{font-size:12px;font-family:dejavusans;}
        </style> 
        <table border="1" class="table" cellpadding="8">
            <tr><th colspan="2" class="tab_info" style="color:#ffffff; background-color:'.$color.'">CONDICIONES DEL EXAMEN</th><th colspan="3"></th></tr>
            <tr>
                <th width="20%">Objetivo de la prueba:</th>
                <td width="80%" colspan="4">Verificar el espesor real de placa mediante el equipo de ultrasonido</td>
            </tr>
            <tr>
                <th width="20%">Procedimiento:</th>
                <td width="15%">PRC-DT-004</td>
                <th width="10%">Revisión:</th>
                <td width="10%">3</td>
                <th width="15%">Código:</th>
                <td width="30%">ASME BPV Sección V, Articulo 23, SE 797</td>
            </tr>
            <tr>
                <th width="20%">Método de Calibración:</th>
                <td colspan="4" width="80%">Recorrido del Haz y Reflexión de Pared Posterior</td>
            </tr>
            <tr>
                <th width="20%">Bloque de Calibración:</th>
                <td width="25%" colspan="2">Bloque de Acero de 5 Pasos</td>
                <th width="15%">No. Serie:</th>
                <td width="40%" colspan="3">37369</td>
            </tr>
            <tr>
                <th width="20%">Condición Superficial:</th>
                <td width="80%" colspan="5">Con Recubrimiento Anticorrosivo Tipo Pintura</td>
            </tr>
            <tr>
                <th width="20%">Criterios de aceptación:</th>
                <td width="80%" colspan="5">Código ASME BPV, Sección VIII, División 1, Partes UG-27 y UG-32</td>
            </tr>
            <tr>
                <th width="20%">Etapa del examen:</th>
                <td width="25%">Equipo en operación:</td>
                <th width="15%">'.$en_opera.'</th>
                <td width="25%">Fuera de operación:</td>
                <th width="15%">'.$fuera_opera.'</th>
            </tr>
            <tr>
                <th width="20%">Partes a Inspeccionar:</th>
                <td width="25%">Envolvente:</td>
                <th width="15%">'.$insp_env.'</th>
                <td width="25%">Tapas:</td>
                <th width="15%">'.$insp_tap.'</th>
            </tr>
            <tr>
                <th width="20%">Observaciones:</th>
                <td width="80%" colspan="5">Lecturas obtenidas en el modo eco-eco, discriminando el espesor de la pintura.</td>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="10">
            <tr><th colspan="3" class="tab_info" style="color:#ffffff; background-color:'.$color.'">RESULTADOS<br>Espesores obtenidos en milímetros</th><th colspan="4"></th></tr>
            <tr class="tab_info" style="color:#ffffff; background-color:'.$color.'">
                <th >Elemento Inspeccionado</th>
                <th >Número De Puntos</th>
                <th >Espesor Máximo</th>
                <th >Espesor Actual A</th> 
                <th >Espesor Mínimo Requerido B</th>
                <th >Diferencia entre Espesores (A-B)</th>
                <th >Resultado de la Evaluación</th> 
            </tr>
            <tr >
                <th class="tab_info" style="color:#ffffff; background-color:'.$color.'">Tapa '.$name_tap.'</th>
                <td >'.$get_cont1->num_rows().'</td>
                <td >'.$get_esp1->max_espesor.'</td>
                <td >'.$tap[0]->espesor.'</td> 
                <td >'.round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).'</td>
                <td >'.($tap[0]->espesor-round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2)).'</td>';
                if(($tap[0]->espesor-round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2))>0)
                    $htmlg.='<td >ACEPTADO</td>';
                else
                    $htmlg.='<td >RECHAZADO</td>';
            $htmlg.='</tr>
            <tr >
                <th class="tab_info" style="color:#ffffff; background-color:'.$color.'">Envolvente</th>
                <td >'.$get_cont3->num_rows().'</td>
                <td >'.$get_cpo->max_espesor.'</td>
                <td >'.$l2->espesor_act.'</td> 
                <td >'.round(($l3->presion_diseno*($l2->d_int/2))/($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno),2).'</td>
                <td >'.($l2->espesor_act-round(($l3->presion_diseno*($l2->d_int/2))/($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno),2)).'</td>';
                if(($l2->espesor_act-round(($l3->presion_diseno*($l2->d_int/2))/($l3->esfuerzo_diseno*$l3->eficiencia_solda-0.6*$l3->presion_diseno),2))>0)
                    $htmlg.='<td >ACEPTADO</td>';
                else
                    $htmlg.='<td >RECHAZADO</td>';
            $htmlg.='</tr>
            <tr >
                <th class="tab_info" style="color:#ffffff; background-color:'.$color.'">Tapa '.$name_tap2.'</th>
                <td >'.$get_cont2->num_rows().'</td>
                <td >'.$get_esp2->max_espesor.'</td>
                <td >'.$tap[1]->espesor.'</td> 
                <td >'.round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2).'</td>
                <td >'.($tap[1]->espesor-round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2)).'</td>';
                if(($tap[1]->espesor-round(($l3->presion_diseno*$t->d_int)/(2*$l3->esfuerzo_diseno*$l3->eficiencia_solda-0.2*$l3->presion_diseno),2))>0){
                    $htmlg.='<td >ACEPTADO</td>';
                    $resulta='<p class="checked">✔</p>';
                    $resultc="--";
                    $txt_res="encima";
                    $txt_res2="DENTRO";
                }
                else{
                    $htmlg.='<td >RECHAZADO</td>';
                    $resultc='<p class="checked">✔</p>';
                    $resulta='--';
                    $txt_res="debajo";
                    $txt_res2="FUERA";
                }
            $htmlg.='</tr>
            <tr>
                <th colspan="7">
                <p class="p_just p_size">En base a la verificación realizada al <u><b>'.$GLOBALS["nombre"].'</b></u> con número de identificación <u><b>'.$l->num_identifica.'</b></u> </p>
                <p class="p_just p_size">se concluye que de acuerdo a las mediciones realizadas, <u><b>Se encuentra por '.$txt_res.' de los Espesores mínimos Requeridos Calculados</b></u></p>
                <p class="p_just p_size">de acuerdo al Código ASME Sección VIII, División 1, por lo que se encuentra <u><b>'.$txt_res2.' DE LOS PARÁMETROS DE SEGURIDAD.</b></u></p>
                </th>
            </tr>
            <tr><th colspan="3">Resultado de la Inspección:</th><td>ACEPTADO</td><th><p class="checked">✔</p></th><td>RECHAZADO</td><th></th></tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .sangria{text-indent: 30px;}
            .checked{font-size:12px;font-family:dejavusans;}
        </style>
        <p class="tab_info p_center">RESULTADOS</p>
        <p class="tab_info p_center">Espesores obtenidos en milímetros.</p>
        <table border="0" align="center" width="100%">
            <tr>
                <th width="30%"></th>
                <th width="40%">
                    <table border="1" class="table" cellpadding="2" align="center" width="100%">
                        <thead>
                            <tr style="color:#ffffff; background-color:'.$color.'">
                                <th width="10%">#</th>
                                <td width="30%">Tapa '.$name_tap.'</td>
                                <td width="30%">Envolvente</td>
                                <td width="30%">Tapa '.$name_tap2.'</td>
                            </tr>
                        </thead>';
                        $i=0;
                        foreach($espes as $e){
                            $i++;
                            $htmlg.='<tr><td width="10%" style="color:#ffffff; background-color:'.$color.'">'.$i.'</td>
                                    <th width="30%">'.$e->esp_tapa1.'</th>
                                    <th width="30%">'.$e->esp_cuerpo.'</th>
                                    <th width="30%">'.$e->esp_tapa2.'</th>
                                </tr>';
                        }
                    $htmlg.='</table>
                </th>
            </tr>
        </table><p></p>
        <table border="1" class="table" cellpadding="2" align="center" width="100%">
            <tr><th class="tab_info" style="color:#ffffff; background-color:'.$color.'">INFORMACIÓN COMPLEMENTARIA</th><th></th></tr>
            <tr>
                <td>Inspeccionó y Evaluó: <p>'.$tecnico.'</p><p><b>Técnico Nivel II END</b></p> <p class="pspaces2"></p>
                    <p>___________________________</p>
                    <p><b>Firma</b></p>
                </td>
                <td>Recibió:
                    <p>'.$l->responsable.'</p><p><b>'.$l->puesto.'</b></p> <p class="pspaces2"></p>
                    <p>___________________________</p>
                    <p><b>Firma</b></p>
                </td>
            </tr>
            <!--<tr>
                <th>'.$tecnico.'</th>
                <th>Agustin Cervantes Domínguez</th>
            </tr>
            <tr>
                <td>Técnico Nivel II END</td>
                <td>RESPONSABLE DE SISTEMA DE GESTIÓN DE SEGURIDAD Y AMBIENTE</td>
            </tr>-->
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .tab_info{ font-weight:bold;}
            .p_center{text-align:center}
        </style>
        <p class="tab_info p_center">CROQUIS UT</p>
        <table width="100%" align="center"><tr><td align="center"><img src="'.base_url().'uploads/croquis/'.$l5->img_croquis.'" width="800px" height="800px"></td></tr></table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$pdf->Bookmark('V. Reporte de inspección por partículas magnéticas -------------------------------------------------------', 0, 0, '', 'B', array(0,0,0));
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
            .pspaces{ font-size:0.8px;}
            .pspaces2{ font-size:30px;}
            .p_just{text-align:justify}
            .p_size{font-size:11px}
            .p_center{text-align:center}
            .checked{font-size:12px;font-family:dejavusans;}
        </style> 
        <p class="tab_info p_just">V.- REPORTE DE INSPECCIÓN POR PARTÍCULAS MAGNÉTICAS</p>

        <table border="1" class="table" cellpadding="8">
            <tr><th class="tab_info" style="color:#ffffff; background-color:'.$color.'">EQUIPO UTILIZADO</th><th colspan="3"></th></tr>
            <tr>
                <td colspan="2">INFORME NÚMERO:</td>
                <td colspan="2">'.$num_expediente.'-END MT</td>
            </tr>
            <tr>
                <td colspan="2">YUGO ELECTROMAGNÉTICO:</td>
                <td colspan="2" style="color:'.$color.'">'.$getE2->yugo.'</td>
            </tr>
            <tr>
                <td >MODELO:</td>
                <td style="color:'.$color.'">'.$getE2->modelo.'</td>
                <td >SERIE:</td>
                <td style="color:'.$color.'">'.$getE2->serie.'</td>
            </tr>
            <tr>
                <td colspan="2">PARTÍCULAS MAGNÉTICAS:</td>
                <td colspan="2" style="color:'.$color.'">'.$getE2->part_magne.'</td>
            </tr>
            <tr>
                <td >TIPO DE PARTÍCULAS:</td>
                <td style="color:'.$color.'">'.$getE2->tipo_part.'</td>
                <td >COLOR:</td>
                <td style="color:'.$color.'">'.$getE2->color.'</td>
            </tr>
            <tr>
                <td colspan="2">CONTRASTANTE:</td>
                <td colspan="2" style="color:'.$color.'">'.$getE2->contraste.'</td>
            </tr>
            <tr>
                <td colspan="2">MARCA:</td>
                <td colspan="2" style="color:'.$color.'">'.$getE2->marca.'</td>
            </tr>
        </table>
        <p></p>
        <table border="1" class="table" cellpadding="8">
            <tr><th colspan="3" class="tab_info" style="color:#ffffff; background-color:'.$color.'">CONDICIONES DE LA INSPECCIÓN</th><th colspan="3"></th></tr>
            <tr>
                <td colspan="2">OBJETIVO DE LA PRUEBA:</td>
                <td colspan="4" style="color:'.$color.'">DETECTAR DISCONTINUIDADES SUPERFICIALES O SUB SUPERFICIALES.</td>
            </tr>
            <tr>
                <td>PROCEDIMIENTO:</td>
                <td style="color:'.$color.'">PRC-DT-002</td>
                <td>REVISIÓN:</td>
                <td style="color:'.$color.'">3</td>
                <td>CÓDIGO:</td>
                <td style="color:'.$color.'">ASME, SECC. V, ART. 7 y 25</td>
            </tr>
            <tr>
                <td colspan="2">MÉTODO:</td>
                <td colspan="4" style="color:'.$color.'">MAGNETIZACIÓN INDIRECTA.</td>
            </tr>
            <tr>
                <td colspan="2">OBSERVACIONES:</td>
                <td colspan="4" style="color:'.$color.'">LAS SOLDADURAS INSPECCIONADAS SE ENCUENTRAN LIBRES DE INDICACIONES.</td>
            </tr>
            <tr>
                <td colspan="2">CRITERIO DE EVALUACIÓN:</td>
                <td colspan="4" style="color:'.$color.'">CÓDIGO ASME SECCIÓN VIII, DIV. 1, APÉNDICE 6</td>
            </tr>
            <tr>
                <td colspan="2">Resultado de la Inspección:</td>
                <th>ACEPTADO</th>
                <th><p class="checked">✔</p></th>
                <th>RECHAZADO</th>
                <th></th>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .tab_info{ font-weight:bold;}
            .p_center{text-align:center}
        </style>
        <p class="tab_info p_center">CROQUIS MT</p>
        <table width="100%" align="center"><tr><td align="center"><img src="'.base_url().'uploads/croquis/'.$l5->img_croquis_mt.'" width="800px" height="800px"></td></tr></table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

if($GLOBALS["consultor"]==0){ //cliente directo
    $logo_head=base_url().'uploads/empresa/'.$GLOBALS["foto"];
}else{ //consultor
    $logo_head=base_url().'public/img/logo_inicio.png';
}
$pdf->AddPage('P', 'A4'); 
$htmlg='<style type="text/css">
            .table{text-align:center; font-size:10px}
            .tab_info{ font-weight:bold;}
            table td{ font-weight:bold;}
            .backg{ background-color:rgb(217,217,217);}
        </style>

        <table border="1" class="table" cellpadding="2" align="center" width="100%">
            <thead>
                <tr style="color:#ffffff; background-color:'.$color.'">
                    <th colspan="2">EVIDENCIA FOTOGRÁFICA</th>
                </tr>
            </thead>';
            $htmlg.=$html_tab;
        $htmlg.='</table>
        <p></p>
        <table border="1" class="table" cellpadding="2" align="center" width="100%">
            <tr>
                <td width="20%">Elaborado por: <br><img width="30px" src="'.$logo_head.'"></td>
                <td width="30%">ELABORÓ:<br>'.$tecnico.'</td>
                <td width="50%">El presente documento tiene una vigencia de 5 años a partir de la fecha de realización de las pruebas siempre y cuando no cambien las condiciones en el mismo</td>
            </tr>
        </table>';

$pdf->writeHTML($htmlg, true, false, true, false, '');

// add a new page for TOC
$pdf->addTOCPage();
$pdf->SetTextColor(0,0,0);
// write the TOC title
$pdf->SetFont('Arial', 'B', 18);
$pdf->MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('Arial', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
//$pdf->addTOC(2, 'Arial', '-', 'INDEX', '', array(128,0,0));
$bookmark_templates[0]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[1]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$bookmark_templates[2]='<table border="0" cellpadding="4"><tr><td width="90%" style="font-size:14px;font-family: Calibri;text-indent: 30px; text-align: justify;">#TOC_DESCRIPTION#</td><td width="10%" style="font-size:14px;font-family: Calibri; text-align:right;">#TOC_PAGE_NUMBER#</td></tr></table>';
$pdf->addHTMLTOC(2, 'INDEX', $bookmark_templates, true, '', array(128,0,0));

// end of TOC page
$pdf->endTOCPage();

$pdf->Output('ensayos_no_destructivos.pdf', 'I');

?>