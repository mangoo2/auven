<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Empresa extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelEmpresa');
    if (!$this->session->userdata('logeado')) {
      redirect('/Login');
    } else {
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
      $permiso = $this->ModeloCatalogos->getviewpermiso($this->perfilid,3); // 2 es el id del submenu
      if ($permiso == 0) {
        redirect('/Sistema');
      }
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/empresa/index');
    $this->load->view('templates/footer');
    $this->load->view('catalogos/empresa/indexjs');
  }

  public function registrar($id=0)
  {
    if($id==0){
      $data['tittle']='Nueva';
      $data['id']='';
      $data['foto']='';
      $data['tipo']='';
      $data['alias']='';
      $data['abreviacion']='';
      $data['direccion']='';
      $data['color']='';
      $data['check_datosfiscales']='';
      $data['rfc']='';
      $data['razon_social']='';
      $data['direccion_df']='';
      $data['alias_df']='';
      $data['codigo_postal']='';
      $data['calle_numero']='';
      $data['idcolonia']='';
      $data['colonia']='';
      $data['ciudad']='';
      $data['estado']='';
      $data['municipio']='';
      $data['UsoCFDI']='';
      $data['metodopago']='';
      $data['condiciones_pago']='';
      $data['forma_pago']='';
      $data['regimenfiscalreceptor']='';
      $data['correo']='';
      $data['nombre']='';
    }else{
      $data['tittle']='Edición'; 
      $resul=$this->ModeloGeneral->getselectwhere('empresa','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['foto']=$x->foto;
        $data['tipo']=$x->tipo;
        $data['alias']=$x->alias;
        $data['abreviacion']=$x->abreviacion;
        $data['direccion']=$x->direccion;
        $data['color']=$x->color;
        $data['check_datosfiscales']=$x->check_datosfiscales;
        $data['rfc']=$x->rfc;
        $data['razon_social']=$x->razon_social;
        $data['direccion_df']=$x->direccion_df;
        $data['alias_df']=$x->alias_df;
        $data['codigo_postal']=$x->codigo_postal;
        $data['calle_numero']=$x->calle_numero;
        $data['colonia']=$x->colonia;
        $data['ciudad']=$x->ciudad;
        $data['estado']=$x->estado;
        $data['municipio']=$x->municipio;
        $data['UsoCFDI']=$x->UsoCFDI;
        $data['metodopago']=$x->metodopago;
        $data['condiciones_pago']=$x->condiciones_pago;
        $data['forma_pago']=$x->forma_pago;
        $data['regimenfiscalreceptor']=$x->regimenfiscalreceptor;
        $data['correo']=$x->correo;
        $data['nombre']=$x->nombre;
      }
    }
    $data['get_cfdi']=$this->ModeloGeneral->getselectwhere('uso_cfdi','activo',1);
    $data['get_metodopago']=$this->ModeloGeneral->getselectwhere('metodopago','activo',1);
    $data['get_formapago']=$this->ModeloGeneral->getselectwhere('formapago','activo',1);
    $data['get_regimenfiscal']=$this->ModeloGeneral->getselectwhere('regimenfiscal','activo',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('catalogos/empresa/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('catalogos/empresa/formjs');
  }


  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    if(isset($datos['check_datosfiscales'])){
      $datos['check_datosfiscales']=1;
    }else{
      $datos['check_datosfiscales']=0;
    }
    $datos['tipo']=0;
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'empresa');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('empresa',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelEmpresa->get_result($params);
    $totaldata= $this->ModelEmpresa->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function getDatosCP(){
    $cp =$this->input->post('cp');
    $col =$this->input->post('col');
    $results=$this->ModelEmpresa->getDatosCPEstado_gruop($cp,$col);
    echo json_encode($results);
  }

  public function getDatosCP_colonia(){
    $cp =$this->input->post('cp');
    $col =$this->input->post('col');
    $results=$this->ModelEmpresa->getDatosCPEstado($cp,$col);
    echo json_encode($results);
  }

  public function getDatosCPSelect(){
    $cp =$this->input->get('cp');
    $col =$this->input->get('col');
    $results=$this->ModelEmpresa->getDatosCPEstado($cp,$col);
    echo json_encode($results);
  }

  function cargafiles(){
    $id=$this->input->post('id');
    $folder="empresa";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;        
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      log_message('error', "ocurrió un error al subir el archivo. No pudo guardarse");
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'empresa');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'empresa');
      echo $resul;
  }

}
