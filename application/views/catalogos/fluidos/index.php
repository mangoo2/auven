<div class="row">
    <div class="col-12">
        <div class="content-header">Listado de fluidos</div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body table-responsive">
                    <a href="<?php echo base_url(); ?>Fluidos/registrar/" class="btn btn-sistema mr-sm-2 mb-3"><i class="fa fa-tint"></i> &nbsp; Nuevo fluido</a>
                    <table class="table text-start m-0" id="table_data">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Logo</th>
                                <th>Nombre del fluido</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>