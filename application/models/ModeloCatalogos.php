<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM perfiles_detalles WHERE perfilId=$perfil AND MenusubId=$modulo";
        //log_message('error', $strq);
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    } 

}